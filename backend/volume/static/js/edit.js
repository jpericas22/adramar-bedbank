const deleteButtonHandler = (button) => {
    return curried_func = e => {
        let confirmedValue = button.getAttribute('data-confirmed');
        if (confirmedValue !== 'true') {
            e.preventDefault();
            button.disabled = true;
            button.value = 'Confirm deletion'
            button.classList.add('pending');
            button.setAttribute('data-confirmed', 'true');
            setTimeout(() => {
                button.classList.remove('pending');
                button.classList.add('confirmed');
                button.disabled = false;
            }, 1000)
        }
    }
}

const labelLinkSelectorHandler = (labelLink, baseLink, selector) => {
    return curried_func = e => {
        if (selector.value) {
            labelLink.setAttribute('href', baseLink + '/' + selector.value)
            labelLink.setAttribute('target', '_blank')
        } else {
            labelLink.setAttribute('href', '#')
            labelLink.removeAttribute('target')
        }
    }
}

const expandPanelAnimationHandler = (expandPanelButtonIcon, expandPanel, expandPanelHeight) => {
    return curried_func = e => {
        expandPanelButtonIcon.innerText = expandPanelButtonIcon.innerText === '+' ? '–' : '+';
        let hide = expandPanel.getAttribute('data-collapsed') === 'false';
        expandPanel.setAttribute('data-collapsed', hide);
        expandPanel.animate([
            { height: '0px' },
            { height: expandPanelHeight }
        ], {
            easing: 'ease-in',
            duration: 200,
            direction: hide ? 'reverse' : 'normal',
            fill: 'both'
        });
        let parentExpandContainer = expandPanel.parentNode.closest('.edit-panel-expand-container');
        if (parentExpandContainer) {
            let parentPanelHeight = parentExpandContainer.getBoundingClientRect().height;
            let parentPanelItemsHeight = [...parentExpandContainer.querySelectorAll('.edit-panel-expand-container[data-collapsed="false"] > .edit-panel-item')].reduce((prev, curr) => {
                return prev + parseFloat(curr.getBoundingClientRect().height);
            }, 0.0);
            if (hide) {
                parentPanelHeight -= parseFloat(expandPanelHeight);
                parentPanelItemsHeight += parseFloat(expandPanelHeight);
            }
            parentExpandContainer.animate([
                { height: parentPanelHeight + 'px' },
                { height: parentPanelItemsHeight + 'px' }
            ], {
                easing: 'ease-in',
                duration: 200,
                direction: hide ? 'reverse' : 'normal',
                fill: 'both'
            });
        }
        if (hide) {
            let childrenExpandContainer = expandPanel.querySelectorAll('.edit-panel-expand-container');
            childrenExpandContainer.forEach(el => {
                el.setAttribute('data-collapsed', true);
                let elementHeight = el.getBoundingClientRect().height;
                el.animate([
                    { height: elementHeight + 'px' },
                    { height: '0px' }
                ], {
                    easing: 'ease-in',
                    duration: 200,
                    direction: 'normal',
                    fill: 'both'
                });
                let elementDataGroup = el.getAttribute('data-group');
                let dataGroupButton = expandPanel.querySelector('.edit-panel-expand-button[data-group="' + elementDataGroup + '"] .expand-icon');
                dataGroupButton.innerText = '+'
            })
        }

    }
}

const injectExpandPanelAnimation = () => {
    let buttons = document.querySelectorAll('.edit-panel-expand-button');
    buttons.forEach(expandPanelButton => {
        let group = expandPanelButton.getAttribute('data-group');
        let expandPanel = document.querySelector('.edit-panel-expand-container[data-group="' + group + '"]');
        let offsetChildrenHeight = [...expandPanel.querySelectorAll('.edit-panel-expand-container')].reduce((prev, curr) => {
            return prev + curr.getBoundingClientRect().height
        }, 0.0);
        let expandPanelHeight = (expandPanel.getBoundingClientRect().height - offsetChildrenHeight) + 'px';
        let expandPanelButtonIcon = expandPanelButton.querySelector('.expand-icon');
        let expandSelected = [...expandPanel.children].some(el => {
            return el.classList.contains('selected');
        })
        if (!expandSelected) {
            expandPanel.style.height = '0px';
        }
        if (!expandPanelButton.classList.contains('disabled')) {
            expandPanelButtonIcon.innerText = expandSelected ? '–' : '+';
            expandPanel.setAttribute('data-collapsed', expandSelected ? 'false' : 'true')
            expandPanelButton.addEventListener('click', expandPanelAnimationHandler(expandPanelButtonIcon, expandPanel, expandPanelHeight))
        } else {
            expandPanelButtonIcon.innerText = ''
        }
    })

}

const injectEditLabelLink = () => {
    let items = document.querySelectorAll('.edit-parameter');
    if (!items) {
        return;
    }
    items.forEach(item => {
        let labelLink = item.querySelector('.edit-label-container .edit-label .edit-label-link');
        if (labelLink) {
            let selector = item.querySelector('select');
            let baseLink = labelLink.getAttribute('data-base-link');
            if (selector.value) {
                labelLink.setAttribute('href', baseLink + '/' + selector.value);
                labelLink.setAttribute('target', '_blank')
            } else {
                labelLink.setAttribute('href', '#');
                labelLink.removeAttribute('target')
            }
            selector.addEventListener('change', labelLinkSelectorHandler(labelLink, baseLink, selector))
        }
    })
}

const injectDeleteButton = () => {
    let button = document.querySelector('.delete-button')
    if (!button) {
        return;
    }
    button.setAttribute('data-confirmed', 'false');
    button.addEventListener('click', deleteButtonHandler(button))
}

onloadFunctionArray.push(injectExpandPanelAnimation, injectDeleteButton, injectEditLabelLink);
const xhrCall = async (path, data) => {
    return new Promise((resolve, reject) => {
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = () => {
            if (xhttp.readyState === 4 && xhttp.status === 200) {
                try {
                    let parsed = JSON.parse(xhttp.responseText)
                    resolve(parsed)
                    return;
                } catch (e) {
                    reject('Error decoding JSON');
                    return;
                }
            }
            if (xhttp.readyState === 4 && xhttp.status !== 200) {
                reject('received status ' + xhttp.status)
                return;
            }
        };
        xhttp.open('POST', path, true);
        xhttp.setRequestHeader('Content-type', 'application/json');
        xhttp.send(JSON.stringify(data));
    })
}
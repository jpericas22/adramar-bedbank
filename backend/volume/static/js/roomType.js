
const addPaxRangeHandler = e => {
    let addPaxRangeButton = e.target;
    let parentPaxRangeContainer = addPaxRangeButton.closest('.multiple-range-container')
    let paxRangeHTMLArray = [...document.querySelectorAll('.pax-range-container')];
    let paxRangeHTMLCount = paxRangeHTMLArray.length;
    let paxRangeHTMLTemplate = paxRangeHTMLArray.pop().cloneNode(true);
    paxRangeHTMLTemplate.querySelectorAll('.pax-range-input-container input').forEach(el => {
        el.value = '';
        el.name = el.name.replace(/[0-9]+/g, paxRangeHTMLCount);
    })
    let deletePaxRangeButton = paxRangeHTMLTemplate.querySelector('.pax-range-delete-button-container input');
    deletePaxRangeButton.addEventListener('click', deletePaxRangeHandler);
    parentPaxRangeContainer.insertBefore(paxRangeHTMLTemplate, addPaxRangeButton.parentNode)
    updatePaxRangeHandler();
}

const updatePaxRangeHandler = () => {
    let paxRangeHTMLArray = [...document.querySelectorAll('.pax-range-container')];
    paxRangeHTMLArray.forEach((el, i) => {
        el.querySelectorAll('input').forEach(input => {
            input.name = input.name.replace(/[0-9]+/g, i);
        })
        let deletePaxRangeButton = el.querySelector('.pax-range-delete-button-container input');
        deletePaxRangeButton.disabled = (i === 0 && paxRangeHTMLArray.length === 1);
    })
}

const deletePaxRangeHandler = e => {
    let deletePaxRangeButton = e.target;
    let deletePaxRangeTarget = deletePaxRangeButton.closest('.pax-range-container');
    deletePaxRangeTarget.remove();
    updatePaxRangeHandler();
}

const addPaxRangeButtonHandler = () => {
    let addPaxRangeButton = document.querySelector('input.add-pax-range');
    if (!addPaxRangeButton) {
        return;
    }
    addPaxRangeButton.addEventListener('click', addPaxRangeHandler)
}

const deletePaxRangeButtonHandler = () => {
    let deletePaxRangeButtonArray = document.querySelectorAll('input.delete-pax-range');
    deletePaxRangeButtonArray.forEach(el => {
        el.addEventListener('click', deletePaxRangeHandler);
    })
}

onloadFunctionArray.push(addPaxRangeButtonHandler, deletePaxRangeButtonHandler);
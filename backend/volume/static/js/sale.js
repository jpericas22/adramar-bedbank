const contractSelectorHandler = async e => {
    let target = e.target;
    let contractId = target.value;
    let xhrRes = await xhrCall('/xhr/contract', {
        id: contractId
    })

    let changeEvent = new Event('change');
    let regimeCheckboxes = document.querySelectorAll('input[name="regime[]"]');
    regimeCheckboxes.forEach(checkbox => {
        if (xhrRes.regime) {
            checkbox.checked = xhrRes.regime.some(regime => {
                return regime.id === parseInt(checkbox.value);
            })
        } else {
            checkbox.checked = false;
        }
        checkbox.dispatchEvent(changeEvent);
    })

    let roomCheckboxes = document.querySelectorAll('input[name="room[]"]');
    roomCheckboxes.forEach(checkbox => {
        if (xhrRes.room) {
            checkbox.checked = xhrRes.room.some(room => {
                return room.id === parseInt(checkbox.value);
            });
        } else {
            checkbox.checked = false;
        }
        checkbox.dispatchEvent(changeEvent);
    })

    let priceInputs = document.querySelectorAll('input.price-input.cost-sale-view');
    priceInputs.forEach(input => {
        let inputRoomId = parseInt(input.getAttribute('data-room'));
        let inputRegimeId = parseInt(input.getAttribute('data-regime'));
        let cost;
        if (xhrRes.cost) {
            cost = xhrRes.cost.find(cost => {
                return cost.rel_room === inputRoomId && cost.rel_regime === inputRegimeId;
            });
        }
        input.value = cost ? cost.value : '';
    })
}


const markupButtonHandler = e => {
    let target = e.target;
    let markupInput = target.parentNode.querySelector('.markup-input');
    let markupValue = parseFloat(markupInput.value);
    if (!markupValue || markupValue < 0 || markupValue > 99.99) {
        return;
    }
    let priceInputs = document.querySelectorAll('input.price-sale-view');
    priceInputs.forEach(el => {
        let costInput = el.closest('.price-group-container').querySelector('input.cost-sale-view');
        if (costInput.value) {
            el.value = customCeil((costInput.value) / ((100 - markupValue) * 0.01));
        }
    })
}

const injectContractSelectorHandler = () => {
    let contractSelector = document.querySelector('select[name="contract"]');
    contractSelector.addEventListener('change', contractSelectorHandler);
}

const injectMarkupButtonHandler = () => {
    let markupButton = document.querySelector('input.markup-button');
    markupButton.addEventListener('click', markupButtonHandler);
}

onloadFunctionArray.push(injectContractSelectorHandler, injectMarkupButtonHandler);

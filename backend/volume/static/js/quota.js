

const quotaModifierHandler = (e, el = null) => {
    let quotaModifierSelector = el ? el : e.target;
    let quotaContainer = quotaModifierSelector.closest('.room-quota-container')
    let roomQuotaInput = quotaContainer.querySelector('input[name*="[value]"]')
    let roomSharedQuotaSelector = quotaContainer.querySelector('select[name*="[hotel_id_shared_quota]"]')
    switch (quotaModifierSelector.value){
        case 'free_sale':
            roomSharedQuotaSelector.value = '';
            roomSharedQuotaSelector.disabled = true;
            roomQuotaInput.value = '';
            roomQuotaInput.disabled = true;
            roomQuotaInput.required = false;
            break;
        case 'on_request':
            roomSharedQuotaSelector.disabled = false;
            roomQuotaInput.value = '';
            roomQuotaInput.disabled = true;
            roomQuotaInput.required = false;
            break;
        default:
            roomSharedQuotaSelector.disabled = false;
            roomQuotaInput.disabled = false;
            roomQuotaInput.required = true;
            break;
    }
}

const quotaModifierSelectorHandler = () => {
    let quotaModifierSelectors = document.querySelectorAll('.room-quota-container select[name*="[quota_modifier]"]')
    quotaModifierSelectors.forEach(el => {
        el.addEventListener('change', quotaModifierHandler);
        quotaModifierHandler(null, el);
    })
}

onloadFunctionArray.push(quotaModifierSelectorHandler);
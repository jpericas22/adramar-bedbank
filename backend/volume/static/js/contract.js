const getRandomString = () => {
    return Math.random().toString(36).substring(2, 15);
}

const regimeStatusHandler = (e, initTarget) => {
    let target = initTarget ? initTarget : e.target;
    let regimeId = target.value;
    let regimeActive = target.checked; // && !target.disabled; ???

    let regimeHeader = [...document.querySelectorAll('span.price-regime-header')].find(header => {
        return header.getAttribute('data-regime') === regimeId;
    });
    if (regimeActive) {
        regimeHeader.classList.remove('disabled');
    } else {
        regimeHeader.classList.add('disabled');
    }
    let regimeContainer = target.closest('.regime-container');
    let baseRadio = regimeContainer.querySelector('.regime-base-radio')
    let modifierInput = regimeContainer.querySelector('.regime-modifier-input');

    if (baseRadio.checked && !regimeActive) {
        baseRadio.checked = false;
    }
    if (regimeActive) {
        baseRadio.classList.remove('disabled');
        baseRadio.removeAttribute('disabled');
        modifierInput.classList.remove('disabled');
        modifierInput.removeAttribute('disabled');
        modifierInput.setAttribute('placeholder', '0.00');
    } else {
        baseRadio.classList.add('disabled');
        baseRadio.setAttribute('disabled', '');
        modifierInput.classList.add('disabled');
        modifierInput.setAttribute('disabled', '');
        modifierInput.setAttribute('placeholder', '');
    }

    let priceInputs = document.querySelectorAll('input.price-input:not(.cost-sale-view):not(.price-sale-view)');
    let roomCheckboxes = document.querySelectorAll('input[name="room[]"]');
    priceInputs.forEach(input => {
        if (input.getAttribute('data-regime') === regimeId) {
            let roomActive = [...roomCheckboxes].find(room => {
                return room.value === input.getAttribute('data-room');
            }).checked;
            let inputLabel = input.parentNode.querySelector('span');
            if (regimeActive && roomActive) {
                inputLabel.classList.remove('disabled');
                if (!input.classList.contains('cost-sale-view')) {
                    input.removeAttribute('disabled');
                    input.setAttribute('placeholder', '0.00');
                }
            } else {
                inputLabel.classList.add('disabled');
                if (!input.classList.contains('cost-sale-view')) {
                    input.setAttribute('disabled', '');
                    input.setAttribute('placeholder', '');
                }
            }
        }
    })
};

const regimeModifierButtonHandler = e => {
    let regimeRadios = document.querySelectorAll('input.regime-base-radio');
    let selectedRadio = [...regimeRadios].find(el => {
        return el.checked;
    })
    if (!selectedRadio) {
        alert('Please select a base regime');
        return;
    }
    let baseRegimeId = selectedRadio.value;

    let inputsToModify = [...document.querySelectorAll('.cost-table .price-input')].filter(el => {
        return !el.disabled && el.getAttribute('data-regime') !== baseRegimeId;
    });

    inputsToModify.forEach(el => {
        let regimeId = el.getAttribute('data-regime');
        let roomId = el.getAttribute('data-room');
        let modifierInput = document.querySelector('input.regime-modifier-input[data-regime="' + regimeId + '"]');
        let modifierValue = parseFloat(modifierInput.value) || 0;
        let baseInput = document.querySelector('.cost-table .price-input[data-room="' + roomId + '"][data-regime="' + baseRegimeId + '"]');
        let baseValue = parseFloat(baseInput.value) || 0;
        if (baseValue === 0) {
            baseInput.value = 0;
        }
        el.value = customCeil(baseValue + modifierValue);
    })
}

const injectRegimeStatusHandler = () => {
    let regimes = document.querySelectorAll('.regime-container input[name="regime[]"]');
    regimes.forEach(el => {
        regimeStatusHandler(null, el);
        el.addEventListener('change', regimeStatusHandler)
    })
}


const injectRegimeRadioSelectHandler = () => {
    let radios = document.querySelectorAll('.regime-container input[type="radio"]');
    radios.forEach(el => {
        regimeRadioSelectMouseDownHandler(null, el);
        el.addEventListener('click', e => {
            e.preventDefault();
        });
        el.addEventListener('mousedown', regimeRadioSelectMouseDownHandler);
    })
}

const injectRegimeModifierButtonHandler = () => {
    let button = document.querySelector('input.regime-modifier-button');
    if (button) {
        button.addEventListener('click', regimeModifierButtonHandler);
    }
}


const roomStatusHandler = (e, initTarget) => {
    let target = initTarget ? initTarget : e.target;
    let roomId = target.value;
    let roomActive = target.checked; // && !target.disabled; ???

    let roomHeader = [...document.querySelectorAll('span.price-room-header')].find(header => {
        return header.getAttribute('data-room') === roomId;
    });
    if (roomActive) {
        roomHeader.classList.remove('disabled');
    } else {
        roomHeader.classList.add('disabled');
    }
    let roomContainer = target.closest('.room-container');
    let baseRadio = roomContainer.querySelector('.room-base-radio')
    let modifierInput = roomContainer.querySelector('.room-modifier-input');

    if (baseRadio.checked && !roomActive) {
        baseRadio.checked = false;
    }
    if (roomActive) {
        baseRadio.classList.remove('disabled');
        baseRadio.removeAttribute('disabled');
        baseRadioLabel.classList.remove('disabled');
        modifierInput.classList.remove('disabled');
        modifierInput.removeAttribute('disabled');
        modifierInput.setAttribute('placeholder', '0.00');
        modifierInputLabel.classList.remove('disabled');
    } else {
        baseRadio.classList.add('disabled');
        baseRadio.setAttribute('disabled', '');
        baseRadioLabel.classList.add('disabled');
        modifierInput.classList.add('disabled');
        modifierInput.setAttribute('disabled', '');
        modifierInput.setAttribute('placeholder', '');
        modifierInputLabel.classList.add('disabled');
    }

    let priceInputs = document.querySelectorAll('input.price-input:not(.cost-sale-view):not(.price-sale-view)');
    let regimeCheckboxes = document.querySelectorAll('input[name="regime[]"]');
    priceInputs.forEach(input => {
        if (input.getAttribute('data-room') === roomId) {
            let regimeActive = [...regimeCheckboxes].find(regime => {
                return regime.value === input.getAttribute('data-regime');
            }).checked;
            let inputLabel = input.parentNode.querySelector('span');
            if (regimeActive && roomActive) {
                inputLabel.classList.remove('disabled');
                if (!input.classList.contains('cost-sale-view')) {
                    input.removeAttribute('disabled');
                    input.setAttribute('placeholder', '0.00');
                }
            } else {
                inputLabel.classList.add('disabled');
                if (!input.classList.contains('cost-sale-view')) {
                    input.setAttribute('disabled', '');
                    input.setAttribute('placeholder', '');
                }
            }
        }
    })


};

const roomModifierButtonHandler = e => {
    let roomRadios = document.querySelectorAll('input.room-base-radio');
    let selectedRadio = [...roomRadios].find(el => {
        return el.checked;
    })
    if (!selectedRadio) {
        alert('Please select a base room');
        return;
    }
    let baseRoomId = selectedRadio.value;

    let inputsToModify = [...document.querySelectorAll('.cost-table .price-input')].filter(el => {
        return !el.disabled && el.getAttribute('data-room') !== baseRoomId;
    });

    inputsToModify.forEach(el => {
        let regimeId = el.getAttribute('data-regime');
        let roomId = el.getAttribute('data-room');
        let modifierInput = document.querySelector('input.room-modifier-input[data-room="' + roomId + '"]');
        let modifierValue = parseFloat(modifierInput.value) || 0;
        let baseInput = document.querySelector('.cost-table .price-input[data-room="' + baseRoomId + '"][data-regime="' + regimeId + '"]');
        let baseValue = parseFloat(baseInput.value) || 0;
        if (baseValue === 0) {
            baseInput.value = 0;
        }
        el.value = customCeil(baseValue + modifierValue);
    })
}




const injectRoomStatusHandler = () => {
    let rooms = document.querySelectorAll('.room-container input[name="room[]"]');
    rooms.forEach(el => {
        roomStatusHandler(null, el);
        el.addEventListener('change', roomStatusHandler)
    })
}

const injectRoomRadioSelectHandler = () => {
    let radios = document.querySelectorAll('.room-container input[type="radio"]');
    radios.forEach(el => {
        roomRadioSelectMouseDownHandler(null, el);
        el.addEventListener('click', e => {
            e.preventDefault();
        });
        el.addEventListener('mousedown', roomRadioSelectMouseDownHandler);
    })
}

const injectRoomModifierButtonHandler = () => {
    let button = document.querySelector('input.room-modifier-button');
    if (button) {
        button.addEventListener('click', roomModifierButtonHandler);
    }
}



onloadFunctionArray.push(injectRoomStatusHandler, injectRoomRadioSelectHandler, injectRoomModifierButtonHandler, injectRegimeStatusHandler, injectRegimeRadioSelectHandler, injectRegimeModifierButtonHandler);
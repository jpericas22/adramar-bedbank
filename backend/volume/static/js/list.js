const setupPaginator = () => {
    let paginatorButtons = document.querySelectorAll('.paginator-button');
    paginatorButtons.forEach(el => {
        el.addEventListener('click', evt => {
            evt.preventDefault();
            let url = new URL(window.location);
            let currentPage = url.searchParams.get('page');
            if (currentPage) {
                currentPage = parseInt(currentPage) + parseInt(el.getAttribute('data-offset'));
            } else {
                currentPage = 1 + parseInt(el.getAttribute('data-offset'));
            }
            url.searchParams.set('page', currentPage);
            window.location = url.toString();
        })
    });
}

const setupSearch = () => {
    searchObjects = document.querySelectorAll('.table-filter-search');
    searchObjects.forEach(async el => {
        let input = el.querySelector('input')
        let searchList = el.querySelector('datalist');
        let searchButton = el.querySelector('.filter-button')
        const parseInput = async e => {
            if (e && e.inputType !== 'insertText') {
                return;
            }
            if (input.getAttribute('selected')) {
                e.preventDefault();
                input.setAttribute('selected', '')
                return;
            }
            input.setAttribute('data-id', '');
            while (searchList.firstChild) {
                searchList.firstChild.remove();
            }
            let text = input.value;
            let xhrRes = await xhrCall('/xhr/search', {
                text: text
            })
            xhrRes.forEach(result => {
                let node = document.createElement('option');
                node.value = result.name;
                node.setAttribute('data-id', result.id)
                searchList.appendChild(node);
            })
        }
        const searchDroplistChange = e => {
            input.setAttribute('data-id', '');
            [...searchList.children].forEach(option => {
                if (input.value === option.value) {
                    let id = option.getAttribute('data-id')
                    input.setAttribute('data-id', id);
                    return;
                }
            })
        }
        await parseInput();
        searchDroplistChange();
        searchButton.addEventListener('click', e => {
            e.preventDefault();
            let id = input.getAttribute('data-id');
            let path = input.getAttribute('data-path');
            if (id) {
                window.location = path + '/' + id;
            }
        })
        input.addEventListener('change', searchDroplistChange);
        input.addEventListener('input', parseInput)
    })
}

onloadFunctionArray.push(setupPaginator, setupSearch);

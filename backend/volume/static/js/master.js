const listSelectorChangeHandler = () => {
    let listSelect = document.querySelectorAll('select.list-select:not(.no-base-link)');
    listSelect.forEach( selector => {
        selector.addEventListener('change', e => {
            let target = e.target;
            let path = target.getAttribute('data-base-link');
            let optionId = target.value;
            if (optionId) {
                path += '/' + optionId;
            }
            window.location.href = path;
        })
    })
}

onloadFunctionArray.push(listSelectorChangeHandler);

let redis = require('redis');

const getConnection = () => {
    return new Promise((resolve, reject) => {
        const client = redis.createClient({
            port: process.env.REDIS_PORT,
            host: process.env.REDIS_HOST,
            password: process.env.REDIS_PASSWORD,
        });
        client.on('connect', () => {
            resolve(client)
        });
        client.on('error', e => {
            reject(e);
        });
    })
}

module.exports = {
    getConnection: getConnection
}
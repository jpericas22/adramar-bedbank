const { Client } = require('pg')

const DB_HOST = process.env.DB_HOST;
const DB_PORT = parseInt(process.env.DB_PORT);
const DB_USER = process.env.DB_USER;
const DB_PASSWORD = process.env.DB_PASSWORD;
const DB_NAME = process.env.DB_NAME;

const getConnection = async () => {
    const client = new Client({
        host: DB_HOST,
        port: DB_PORT,
        user: DB_USER,
        password: DB_PASSWORD,
        database: DB_NAME,
    })
    await client.connect()
    return client;
}

const query = (command, params, client) => {
    return new Promise((resolve, reject) => {
        client.query(command, params, (err, res) => {
            if (err) {
                reject(err)
                return;
            }
            resolve(res)
        })
    })
}

module.exports = {
    getConnection: getConnection
}
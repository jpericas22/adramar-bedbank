'use strict';
const env = process.env.NODE_ENV || 'DEV';
const express = require('express')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser');
const nunjucks = require('nunjucks');
const app = express()
const port = process.env.BACKEND_PORT
const config = require('./config.json');
const auth = require('./services/auth');
const hotel = require('./services/hotel');
const extra = require('./services/extra');
const room = require('./services/room');
const contract = require('./services/contract');
const sale = require('./services/sale');
const contractRange = require('./services/contractRange');
const quota = require('./services/quota');
const quotaRange = require('./services/quotaRange');
const roomType = require('./services/roomType');
const provider = require('./services/provider');
const regime = require('./services/regime');
const chain = require('./services/chain');
const extras = require('./services/extras'); //TODO CHANGE NAME!!!!!!!!!
const booking = require('./services/booking');
const crossReference = require('./services/crossReference');

let nunjucksConfig = nunjucks.configure('views', {
    autoescape: true,
    express: app
})

nunjucksConfig.addFilter('dateSimple', date => {
    let dayNumber = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
    let monthNumber = (date.getMonth() + 1) < 10 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1);
    let yearNumber = date.getFullYear();
    return dayNumber + '/' + monthNumber + '/' + yearNumber;
})

nunjucksConfig.addFilter('dateSimpleTime', date => {
    let dayNumber = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
    let monthNumber = (date.getMonth() + 1) < 10 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1);
    let yearNumber = date.getFullYear();
    let hourNumber = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
    let minuteNumber = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
    return dayNumber + '/' + monthNumber + '/' + yearNumber + ' ' + hourNumber + ':' + minuteNumber;
});

nunjucksConfig.addFilter('dateStringSimple', date => {
    return date.split('-').reduce((prev, curr, i) => {
        if (i > 0) {
            prev = curr + '/' + prev
        } else {
            prev = curr;
        }
        return prev
    })
})

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(cookieParser())

app.use(async (req, res, next) => {
    /*
    if (config.NO_LOGIN_ROUTE_WHITELIST.includes(req.path)) {
        next();
        return;
    }
    let sessionId = req.cookies.sid;
    console.log(sessionId);
    if (!sessionId) {
        res.redirect('/auth');
        return;
    }
    let redisRes = await auth.checkSession(sessionId);
    console.log(redisRes);
    if (!redisRes) {
        res.redirect('/auth');
        return;
    }
    req.sessionData = redisRes;
    */
    next();
})



app.get('/', (req, res) => {
    res.redirect('/hotel')
});

app.all('/hotel', async (req, res, next) => {
    let hotelRes = await hotel.getHotelReduced();
    res.render('hotel/list.html', {
        hotelList: hotelRes.data,
        page: 'hotel'
    })
})

app.get('/hotel/create', async (req, res, next) => {
    let hotelRes = await extra.handleHotelData();
    res.render('hotel/edit.html', {
        hotel: hotelRes,
        option: 'details',
        create: true,
    })
});

app.post('/hotel/create', async (req, res, next) => {
    let hotelId = await hotel.insertHotel(req.body)
    res.redirect('/hotel/' + hotelId + '/edit/details');
});

app.get([
    '/hotel/:hotelId/edit',
    '/hotel/:hotelId'
], (req, res, next) => {
    let { hotelId } = req.params;
    res.redirect('/hotel/' + hotelId + '/edit/details');
})

app.post('/hotel/:hotelId/edit/:option/:optionId?/:sub?/:subId?', async (req, res, next) => {
    let { hotelId, option, optionId, sub, subId } = req.params;
    switch (option) {
        case 'details':
            await hotel.editDetails(hotelId, req.body);
            break;
        case 'room':
            if (optionId) {
                if ('delete' in req.body) {
                    await room.deleteRoom(optionId);
                    res.redirect('/hotel/' + hotelId + '/edit/' + option);
                    return;
                }
                await room.updateRoom(optionId, req.body);
            } else {
                let roomId = await room.insertRoom(hotelId, req.body);
                res.redirect('/hotel/' + hotelId + '/edit/' + option + '/' + roomId);
                return;
            }
            break;
        case 'contract':
            if (!optionId) {
                let contractId = await contract.insertContract(hotelId, req.body);
                res.redirect('/hotel/' + hotelId + '/edit/' + option + '/' + contractId);
                return;
            } else {
                let contractId = optionId;
                if (sub === 'range') {
                    if (subId) {
                        if ('delete' in req.body) {
                            await contractRange.deleteRangeHandler(subId);
                            res.redirect('/hotel/' + hotelId + '/edit/' + option + '/' + contractId);
                            return;
                        }
                        if ('duplicate' in req.body) {
                            console.log('HEY')
                            let rangeId = await contractRange.duplicateRangeHandler(subId);
                            console.log(rangeId)
                            res.redirect('/hotel/' + hotelId + '/edit/' + option + '/' + contractId + '/' + sub + '/' + rangeId);
                            return;
                        }
                        await contractRange.updateRangeHandler(subId, req.body);
                    } else {
                        let rangeId = await contractRange.insertRangeHandler(contractId, req.body);
                        res.redirect('/hotel/' + hotelId + '/edit/' + option + '/' + contractId + '/' + sub + '/' + rangeId);
                        return;
                    }
                }
                if (!sub) {
                    if ('delete' in req.body) {
                        await contract.deleteContractHandler(contractId);
                        res.redirect('/hotel/' + hotelId + '/edit/' + option);
                        return;
                    }
                    await contract.updateContract(contractId, req.body);
                }
            }
            break;
        case 'sale':
            if (!optionId) {
                let saleId = await sale.insertSale(hotelId, req.body);
                res.redirect('/hotel/' + hotelId + '/edit/' + option + '/' + saleId);
                return;
            } else {
                let saleId = optionId;
                if (sub === 'range' && subId) {
                    await contractRange.updateRangeHandler(subId, req.body);
                }
                if (!sub) {
                    if ('delete' in req.body) {
                        await sale.deleteSale(saleId);
                        res.redirect('/hotel/' + hotelId + '/edit/' + option);
                        return;
                    }
                    await sale.updateSale(saleId, req.body);
                }
            }
            break;
        case 'quota':
            if (!optionId) {
                let quotaId = await quota.insertQuota(hotelId, req.body);
                res.redirect('/hotel/' + hotelId + '/edit/' + option + '/' + quotaId);
                return;
            } else {
                let quotaId = optionId;
                if (sub === 'range') {
                    if (subId) {
                        if ('delete' in req.body) {
                            await quotaRange.deleteRangeHandler(subId);
                            res.redirect('/hotel/' + hotelId + '/edit/' + option + '/' + quotaId);
                            return;
                        }
                        await quotaRange.updateRangeHandler(subId, req.body);
                    } else {
                        let rangeId = await quotaRange.insertRangeHandler(quotaId, req.body);
                        res.redirect('/hotel/' + hotelId + '/edit/' + option + '/' + quotaId + '/' + sub + '/' + rangeId);
                        return;
                    }
                }
                if (!sub) {
                    if ('delete' in req.body) {
                        await quota.deleteQuotaHandler(optionId);
                        res.redirect('/hotel/' + hotelId + '/edit/' + option);
                        return;
                    }
                    await quota.updateQuota(optionId, req.body);
                }
            }
            break;
    }
    let redirectString = '/hotel/' + hotelId + '/edit/' + option + '/' + optionId;
    if (sub) redirectString += '/' + sub;
    if (subId) redirectString += '/' + subId;
    res.redirect(redirectString);
})

app.get('/hotel/:hotelId/edit/:option/:optionId?/:sub?/:subId?', async (req, res, next) => {
    let { hotelId, option, optionId, sub, subId } = req.params;
    if (!hotelId) {
        res.redirect('/hotel/create');
        return;
    }
    let filters;
    let hotelRes = (await hotel.getHotel(filters = {
        'h.id': hotelId
    })).data[0];
    if (!hotelRes) {
        res.redirect('/hotel/create');
        return;
    }
    hotelRes = {
        ...hotelRes,
        ...(await extra.handleHotelData(hotelRes))
    }

    let roomRes = {};
    let contractRes = {};
    let saleRes = {};
    let quotaRes = {};

    switch (option) {
        case 'room':
            if (optionId) {
                roomRes = (await room.getRoom(filters = {
                    'r.id': optionId
                })).data[0];
            }
            roomRes = {
                ...roomRes,
                ...(await extra.handleRoomData(roomRes))
            }
            break;
        case 'contract':
            contractRes['hotel_id'] = hotelId;
            if (optionId) {
                contractRes = (await contract.getContract(filters = {
                    'con.id': optionId
                })).data[0];
            }
            contractRes = {
                ...contractRes,
                ...(await extra.handleContractData(contractRes))
            }
            break;
        case 'sale':
            saleRes['hotel_id'] = hotelId;
            if (optionId) {
                saleRes = (await sale.getSale(filters = {
                    's.id': optionId
                })).data[0];
            }
            saleRes = {
                ...saleRes,
                ...(await extra.handleSaleData(saleRes))
            }
            break;
        case 'quota':
            quotaRes['hotel_id'] = hotelId;
            if (optionId) {
                quotaRes = (await quota.getQuota(filters = {
                    'q.id': optionId
                })).data[0];
            }
            quotaRes = {
                ...quotaRes,
                ...(await extra.handleQuotaData(quotaRes))
            }
            break;
    }
    res.render('hotel/edit.html', {
        hotel: hotelRes,
        room: roomRes,
        contract: contractRes,
        sale: saleRes,
        quota: quotaRes,
        option: option,
        optionId: optionId,
        sub: sub,
        subId: subId
    })
})

app.get('/booking', async (req, res, next) => {
    let bookingRes = await booking.getBooking();
    console.log(bookingRes.data)
    res.render('booking/list.html', {
        bookingList: bookingRes.data
    })
})

app.get('/booking/:bookingId/edit/:option/:optionId?', async (req, res, next) => {
    let { bookingId, option, optionId } = req.params;
    if (!bookingId) {
        res.redirect('/booking/create');
        return;
    }
    let filters;
    let bookingRes = (await booking.getBooking(filters = {
        'b.id': bookingId
    })).data[0];
    if (!bookingRes) {
        res.redirect('/booking/create');
        return;
    }
    /*
    hotelRes = {
        ...hotelRes,
        ...(await extra.handleHotelData(hotelRes))
    }
    */

    res.render('booking/edit.html', {
        booking: bookingRes,
        option: option,
        optionId: optionId,
    })
})

app.get('/master', async (req, res, next) => {
    res.redirect('/master/room');
})

app.post('/master/:option/:optionId?/:sub?/:subId?', async (req, res, next) => {
    let { option, optionId, sub, subId } = req.params;
    switch (option) {
        case 'room':
            if (optionId) {
                if ('delete' in req.body) {
                    await roomType.deleteRoomTypeHandler(optionId);
                    res.redirect('/master/' + option);
                    return;
                }
                optionId = await roomType.updateRoomTypeHandler(optionId, req.body)
            } else {
                optionId = await roomType.insertRoomTypeHandler(req.body);
            }
            break;
        case 'regime':
            if (optionId) {
                if ('delete' in req.body) {
                    await regime.deleteRegime(optionId)
                    res.redirect('/master/' + option);
                    return;
                }
                optionId = await regime.updateRegime(optionId, req.body)
            } else {
                optionId = await regime.insertRegime(req.body);
            }
            break;
        case 'extra':
            if (optionId) {
                if ('delete' in req.body) {
                    await extras.deleteExtras(optionId)
                    res.redirect('/master/' + option);
                    return;
                }
                optionId = await extras.updateExtras(optionId, req.body)
            } else {
                optionId = await extras.insertExtras(req.body);
            }
            break;
        case 'provider':
            if (optionId) {
                if ('delete' in req.body) {
                    await provider.deletePovider(optionId);
                    res.redirect('/master/' + option);
                    return;
                }
                optionId = await provider.updateProvider(optionId, req.body)
            } else {
                optionId = await provider.insertProvider(req.body);
            }
            break;
        case 'chain':
            if (optionId) {
                if ('delete' in req.body) {
                    await chain.deleteChain(optionId);
                    res.redirect('/master/' + option);
                    return;
                }
                optionId = await chain.updateChain(optionId, req.body)
            } else {
                optionId = await chain.insertChain(req.body);
            }
            break;
        case 'mapping':
            let clientItemId = req.body.client_item;
            console.log(clientItemId)
            switch (sub) {
                case 'hotel':
                    await crossReference.setHotelHandler(subId, clientItemId, optionId)
                    break;
                case 'room':
                    console.log(subId, clientItemId, optionId)
                    await crossReference.setRoomHandler(subId, clientItemId, optionId)
                    break;
            }
            break;
    }
    let redirectString = '/master/' + option
    if (optionId) redirectString += '/' + optionId;
    if (sub) redirectString += '/' + sub;
    if (subId) redirectString += '/' + subId;
    res.redirect(redirectString);
});

app.get('/master/:option/:optionId?/:sub?/:subId?', async (req, res, next) => {
    let { option, optionId, sub, subId } = req.params;
    let masterRes = [];
    let extraRes = {}
    switch (option) {
        case 'room':
            masterRes = await roomType.getRoomType();
            if (optionId) {
                let roomFound = false;
                masterRes = masterRes.map(el => {
                    if (el.id === parseInt(optionId)) {
                        el.selected = true;
                        roomFound = true;
                    };
                    return el;
                })
                if (!roomFound) {
                    res.redirect('/master/' + option)
                    return;
                }
            }
            break;
        case 'regime':
            masterRes = await regime.getRegime()
            if (optionId) {
                let regimeFound = false;
                masterRes = masterRes.map(el => {
                    if (el.id === parseInt(optionId)) {
                        el.selected = true;
                        regimeFound = true;
                    };
                    return el;
                })
                if (!regimeFound) {
                    res.redirect('/master/' + option)
                    return;
                }
            }
            extraRes = await extra.handleRegimeData();
            break;
        case 'extra':
            masterRes = await extras.getExtras();
            if (optionId) {
                let extraFound = false;
                masterRes = masterRes.map(el => {
                    if (el.id === parseInt(optionId)) {
                        el.selected = true;
                        extraFound = true;
                    };
                    return el;
                })
                if (!extraFound) {
                    res.redirect('/master/' + option)
                    return;
                }
            }
            break;
        case 'provider':
            masterRes = (await provider.getProvider()).data;
            if (optionId) {
                let providerFound = false;
                masterRes = masterRes.map(el => {
                    if (el.id === parseInt(optionId)) {
                        el.selected = true;
                        providerFound = true;
                    };
                    return el;
                })
                if (!providerFound) {
                    res.redirect('/master/' + option)
                    return;
                }
            }
            extraRes = await extra.handleProviderData();
            break;
        case 'chain':
            masterRes = await chain.getChain();
            if (optionId) {
                let chainFound = false;
                masterRes = masterRes.map(el => {
                    if (el.id === parseInt(optionId)) {
                        el.selected = true;
                        chainFound = true;
                    };
                    return el;
                })
                if (!chainFound) {
                    res.redirect('/master/' + option)
                    return;
                }
            }
            break;
        case 'mapping':
            masterRes = await crossReference.get(optionId)
            extraRes = await crossReference.getClientData(optionId);
            if (subId) {
                let itemFound = false;
                masterRes = masterRes.map(el => {
                    if (sub === 'room') {
                        el.room.forEach(rEl => {
                            if (rEl.id === parseInt(subId)) {
                                rEl.selected = true;
                                el.selected = true;
                                itemFound = el;
                            }
                        })
                    } else {
                        if (el.id === parseInt(subId)) {
                            el.selected = true;
                            itemFound = el;
                        };
                    }
                    return el;
                })
                if (!itemFound) {
                    res.redirect('/master/' + option + '/' + optionId + '/hotel')
                    return;
                } else {
                    extraRes = extraRes.map(el => {
                        if (el.client_id === itemFound.client_id) {
                            el.selected = true;
                        }
                        if (sub === 'room') {
                            el.room.forEach(rEl => {
                                itemFound.client_room.forEach( i => {
                                    if(i.rel_room === rEl.rel_room){
                                        rEl.selected = true;
                                    }
                                })
                            })
                        }
                        return el;
                    })
                }
            }
            break;
    }
    res.render('master/edit.html', {
        master: masterRes,
        extra: extraRes,
        option: option,
        optionId: optionId
    })
});


app.post('/xhr/search', async (req, res) => {
    if (req.body.text) {
        let searchRes = await hotel.searchHotel(req.body.text)
        res.json(searchRes);
        return;
    }
    res.json([])
});

app.post('/xhr/contract', async (req, res) => {
    if (req.body.id) {
        let filters = {
            'con.id': req.body.id
        }
        let contractRes = (await contract.getContract(filters = filters)).data[0]
        res.json(contractRes);
        return;
    }
    res.json({})
});

app.post('/auth', async (req, res, next) => {
    let { username, password } = req.body;
    try {
        let authRes = await auth.checkLogin(username, password);
        res.cookie('sid', authRes, { saveUninitialized: true, maxAge: 900000, secure: env !== 'DEV' });
        res.json(authRes);
    } catch (e) {
        res.status(403).send(e.message);
    }
})

app.get('/auth', async (req, res, next) => {
    res.render('login.html', {
    })
})


app.listen(port, () => {
    console.log(`adramar backend listening on port ${port}`)
    console.log(process.env.DB_HOST);
    console.log(process.env.DB_PORT);
    console.log(process.env.DB_USER);
    console.log(process.env.DB_PASSWORD);
    console.log(process.env.DB_NAME);
})
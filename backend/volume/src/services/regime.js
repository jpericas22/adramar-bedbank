'use strict';
const db = require('../lib/db');

const cleanForm = (formData) => {
    let checkboxList = ['active'];
    for (let key in formData) {
        if (formData.hasOwnProperty(key)) {
            if (checkboxList.includes(key)) {
                formData[key] = formData[key][0] === 'on';
            }
            if (formData[key] === '') {
                formData[key] = null;
            }
        }
    }
}


const getRegime = async (con = null, json = false) => {
    let closeCon = false;
    let queryString = `SELECT re.id, 
                              re.name,
                              re.description,
                              re.rel_regime_type AS regime_type_id,
                              re.code,
                              res_first.id AS regime_service_first_id,
							  res_first.name AS regime_service_first_name,
                              res_last.id AS regime_service_last_id,
							  res_last.name AS regime_service_last_name,
                              re.active
                         FROM data.regime as re
					LEFT JOIN data.regime_type as ret ON re.rel_regime_type = ret.id
                    LEFT JOIN data.regime_service as res_first ON re.rel_regime_service_first = res_first.id
					LEFT JOIN data.regime_service as res_last ON re.rel_regime_service_last = res_last.id
                    `;
    if (!con) {
        closeCon = true;
        con = await db.getConnection();
    }
    let res = await con.query(queryString, []);
    if (closeCon) {
        await con.end();
    }
    if (json) {
        return {
            regime: res.rows
        }
    }
    return res.rows;
}

const insertRegime = async (data, con = null) => {
    cleanForm(data)
    let closeCon = false;
    let queryString = `INSERT INTO data.regime(name, 
                                               description, 
                                               rel_regime_type, 
                                               code, 
                                               rel_regime_service_first, 
                                               rel_regime_service_last, 
                                               active)
                                        VALUES ($1, $2, $3, $4, $5, $6, $7)
                                     RETURNING id`
    let parameters = [
        data.name,
        data.description,
        data.regime_type,
        data.code,
        data.regime_service_first,
        data.regime_service_lats,
        data.active
    ]
    if (!con) {
        closeCon = true;
        con = await db.getConnection();
    }
    let res = await con.query(queryString, parameters);
    if (closeCon) {
        await con.end();
    }
    return res.rows[0].id;
}

const updateRegime = async (regimeId, data, con = null) => {
    cleanForm(data)
    let closeCon = false;
    let queryString = `UPDATE data.regime
                          SET name=$2, 
                              description=$3, 
                              rel_regime_type=$4, 
                              code=$5, 
                              rel_regime_service_first=$6, 
                              rel_regime_service_last=$7, 
                              active=$8
                        WHERE id=$1
                    RETURNING id`;
    let parameters = [
        regimeId,
        data.name,
        data.description,
        data.regime_type,
        data.code,
        data.regime_service_first,
        data.regime_service_last,
        data.active
    ]
    if (!con) {
        closeCon = true;
        con = await db.getConnection();
    }
    let res = await con.query(queryString, parameters);
    if (closeCon) {
        await con.end();
    }
    return res.rows[0].id;
}

const deleteRegime = async (regimeId, con = null) => {
    let closeCon = false;
    let queryString = `DELETE FROM data.regime
                             WHERE id=$1
                         RETURNING id`;
    if (!con) {
        closeCon = true;
        con = await db.getConnection();
    }
    let res = await con.query(queryString, [regimeId]);
    if (closeCon) {
        await con.end();
    }
    return res.rows[0].id;
}



module.exports = {
    getRegime: getRegime,
    insertRegime: insertRegime,
    updateRegime: updateRegime,
    deleteRegime, deleteRegime,
}
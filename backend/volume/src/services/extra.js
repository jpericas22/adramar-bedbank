'use strict';
const db = require('../lib/db');
const roomType = require('./roomType');
const regime = require('./regime');
const chain = require('./chain');
const extras = require('./extras');

const test = async (con = null, json = false) => {
    let closeCon = false;
    let queryString = ``;
    if (!con) {
        con = await db.getConnection();
    }
    let res = await con.query(queryString, []);
    if (closeCon) {
        await con.end();
    }
    if (json) {
        return {
            roomTypeRes: res.rows
        }
    }
    return res.rows;
}

const getAccomodationCategory = async (con = null, json = false) => {
    let closeCon = false;
    let queryString = `SELECT id, 
                              name
	                     FROM data.accomodation_category`;
    if (!con) {
        closeCon = true;
        con = await db.getConnection();
    }
    let res = await con.query(queryString, []);
    if (closeCon) {
        await con.end();
    }
    if (json) {
        return {
            accomodationCategory: res.rows
        }
    }
    return res.rows;
}

const getAccomodationType = async (con = null, json = false) => {
    let closeCon = false;
    let queryString = `SELECT id, 
                              name
	                     FROM data.accomodation_type`;
    if (!con) {
        closeCon = true;
        con = await db.getConnection();
    }
    let res = await con.query(queryString, []);
    if (closeCon) {
        await con.end();
    }
    if (json) {
        return {
            accomodationType: res.rows
        }
    }
    return res.rows;
}




const getRegimeType = async (con = null, json = false) => {
    let closeCon = false;
    let queryString = `SELECT id, 
                              name
	                     FROM data.regime_type`;
    if (!con) {
        closeCon = true;
        con = await db.getConnection();
    }
    let res = await con.query(queryString, []);
    if (closeCon) {
        await con.end();
    }
    if (json) {
        return {
            regimeType: res.rows
        }
    }
    return res.rows;
}


const getRegimeService = async (con = null, json = false) => {
    let closeCon = false;
    let queryString = `SELECT id, 
                              name
	                     FROM data.regime_service`;
    if (!con) {
        closeCon = true;
        con = await db.getConnection();
    }
    let res = await con.query(queryString, []);
    if (closeCon) {
        await con.end();
    }
    if (json) {
        return {
            regimeService: res.rows
        }
    }
    return res.rows;
}




const getCity = async (con = null, json = false) => {
    let closeCon = false;
    let queryString = `SELECT id, 
                              name, 
                              rel_region
	                     FROM data.city`;
    if (!con) {
        closeCon = true;
        con = await db.getConnection();
    }
    let res = await con.query(queryString, []);
    if (closeCon) {
        await con.end();
    }
    if (json) {
        return {
            city: res.rows
        }
    }
    return res.rows;
}

const getRegion = async (con = null, json = false) => {
    let closeCon = false;
    let queryString = `SELECT id, 
                              name, 
                              rel_country
	                     FROM data.region`;
    if (!con) {
        closeCon = true;
        con = await db.getConnection();
    }
    let res = await con.query(queryString, []);
    if (closeCon) {
        await con.end();
    }
    if (json) {
        return {
            region: res.rows
        }
    }
    return res.rows;
}

const getCountry = async (con = null, json = false) => {
    let closeCon = false;
    let queryString = `SELECT id, 
                              name
	                     FROM data.country`;
    if (!con) {
        closeCon = true;
        con = await db.getConnection();
    }
    let res = await con.query(queryString, []);
    if (closeCon) {
        await con.end();
    }
    if (json) {
        return {
            country: res.rows
        }
    }
    return res.rows;
}

const getProviderReduced = async (con = null, json = false) => {
    let closeCon = false;
    let queryString = `SELECT id, 
                              fiscal_name
	                     FROM data.provider`;
    if (!con) {
        closeCon = true;
        con = await db.getConnection();
    }
    let res = await con.query(queryString, []);
    if (closeCon) {
        await con.end();
    }
    if (json) {
        return {
            provider: res.rows
        }
    }
    return res.rows;
}

const getRelatedSale = async (contractId, con = null, json = false) => {
    let closeCon = false;
    let queryString = `SELECT id, 
                              name, 
                              rel_contract AS contract_id, 
                              active
                         FROM data.sale
                        WHERE rel_contract = $1`;
    if (!con) {
        closeCon = true;
        con = await db.getConnection();
    }
    let res = await con.query(queryString, [contractId]);
    if (closeCon) {
        await con.end();
    }
    if (json) {
        return {
            sale: res.rows
        }
    }
    return res.rows;
}

const getRelatedContracts = async (hotelID, con = null, json = false) => {
    let closeCon = false;
    let queryString = `SELECT id, 
                              name
                         FROM data.contract
                        WHERE rel_hotel = $1`;
    if (!con) {
        closeCon = true;
        con = await db.getConnection();
    }
    let res = await con.query(queryString, [hotelID]);
    if (closeCon) {
        await con.end();
    }
    if (json) {
        return {
            contract: res.rows
        }
    }
    return res.rows;
}

const getRelatedRoom = async (hotelId, con = null, json = false) => {
    let closeCon = false;
    let queryString = `SELECT id, 
                              name
                         FROM data.room
                        WHERE rel_hotel = $1`;
    if (!con) {
        closeCon = true;
        con = await db.getConnection();
    }
    let res = await con.query(queryString, [hotelId]);
    if (closeCon) {
        await con.end();
    }
    if (json) {
        return {
            room: res.rows
        }
    }
    return res.rows;
}


const relHandler = async (baseData, pairs, functions) => {
    functions = await Promise.all(functions).then(values => {
        return values.reduce((prev, current, { }) => {
            return {
                ...prev,
                ...current
            }
        })
    });
    for (let value in functions) {
        if (pairs[value]) {
            functions[value].forEach(el => {
                if (!Array.isArray(pairs[value])){
                    pairs[value] = [pairs[value]];
                }
                pairs[value].forEach( (pairsEl, i) => {
                    let dest = baseData[pairsEl];
                    let destIsArray = Array.isArray(dest);
                    if (destIsArray) {
                        if (dest.some(destEl => {
                            return el.id === destEl.id;
                        })) {
                            el.selected = true;
                            if (!el.selectedIndex){
                                el.selectedIndex = [i];
                            } else {
                                el.selectedIndex.push(i);
                            }
                        }
                    } else {
                        if (el.id === dest) {
                            el.selected = true;
                            if (!el.selectedIndex) {
                                el.selectedIndex = [i];
                            } else {
                                el.selectedIndex.push(i);
                            }
                        }
                    }
                })
                
            })
        }
    }
    return {
        ...functions
    }
}

const handleHotelData = async (hotelData={}) => {
    let con = await db.getConnection();
    let pairs = {
        'accomodationCategory': 'accomodation_category_id',
        'accomodationType': 'accomodation_type_id',
        'city': 'city_id',
        'region': 'region_id',
        'country': 'country_id',
        'chain': 'chain_id',
        'provider': 'provider_id',
        'extras': 'extras',
    }
    let functions = [
        getAccomodationCategory(con, true),
        getAccomodationType(con, true),
        getCity(con, true),
        getRegion(con, true),
        getCountry(con, true),
        chain.getChain(con, true),
        getProviderReduced(con, true),
        extras.getExtras(con, true)
    ];
    let values = await relHandler(hotelData, pairs, functions);
    await con.end();
    return {
        ...values
    }
}

const handleRoomData = async (roomData={}) => {
    let con = await db.getConnection();
    let pairs = {
        'roomType': 'room_type_id'
    }
    let functions = [roomType.getRoomType(con, true)];
    let values = await relHandler(roomData, pairs, functions);
    await con.end();
    return {
        ...values
    }
}

const handleContractData = async (contractData) => {
    let con = await db.getConnection();
    let pairs = {
        'regime': 'regime',
        'room': 'room'
    }
    let functions = [
        regime.getRegime(con, true),
        getRelatedSale(contractData.id, con, true),
        getRelatedRoom(contractData.hotel_id, con, true)
    ];
    let values = await relHandler(contractData, pairs, functions);
    await con.end();
    return {
        ...values
    }
}

const handleSaleData = async (saleData) => {
    let con = await db.getConnection();
    let pairs = {
        'regime': 'regime',
        'room': 'room'
    }
    let functions = [
        regime.getRegime(con, true),
        getRelatedRoom(saleData.hotel_id, con, true),
        getRelatedContracts(saleData.hotel_id, con, true)
    ];
    let values = await relHandler(saleData, pairs, functions);
    await con.end();
    return {
        ...values
    }
}

const handleQuotaData = async (quotaData) => {
    let con = await db.getConnection();
    let pairs = {
        'regime': 'regime',
        'room': 'room'
    }
    let functions = [
        regime.getRegime(con, true),
        getRelatedRoom(quotaData.hotel_id, con, true),
    ];
    let values = await relHandler(quotaData, pairs, functions);
    await con.end();
    return {
        ...values
    }
}

const handleRegimeData = async (regimeData={}) => {
    let con = await db.getConnection();
    let pairs = {
        'regimeType': 'regime_type_id',
        'regimeService': ['regime_service_first_id', 'regime_service_last_id']
    }
    let functions = [
        getRegimeType(con, true),
        getRegimeService(con, true),
    ];
    let values = await relHandler(regimeData, pairs, functions);
    await con.end();
    return {
        ...values
    }
}

const handleProviderData = async (providerData = {}) => {
    let con = await db.getConnection();
    let pairs = {
        'city': 'city_id',
        'region': 'region_id',
        'country': 'country_id',
    }
    let functions = [
        getCity(con, true),
        getRegion(con, true),
        getCountry(con, true)
    ];
    let values = await relHandler(providerData, pairs, functions);
    await con.end();
    return {
        ...values
    }
}




module.exports = {
    getAccomodationCategory: getAccomodationCategory,
    getAccomodationType: getAccomodationType,
    getExtras: extras.getExtras,
    getRegime: regime.getRegime,
    getRegimeType: getRegimeType,
    getRoomType: roomType.getRoomType,
    getChain: chain.getChain,
    handleHotelData: handleHotelData,
    handleRoomData: handleRoomData,
    handleContractData: handleContractData,
    handleSaleData: handleSaleData,
    handleQuotaData: handleQuotaData,
    handleRegimeData: handleRegimeData,
    handleProviderData: handleProviderData
}

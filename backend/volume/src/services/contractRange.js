
'use strict';
const db = require('../lib/db')

const cleanForm = (formData) => {
    let checkboxList = ['active'];
    let emptyDefaultList = ['regime'];
    for (let key in formData) {
        if (formData.hasOwnProperty(key)) {
            if (checkboxList.includes(key)) {
                formData[key] = formData[key][0] === 'on';
            }
            if (formData[key] === '') {
                formData[key] = null;
            }
        }
    }
    emptyDefaultList.forEach(el => {
        if (!(el in formData)) {
            formData[el] = [];
        }
    })
    if (!('contract_value' in formData)) {
        formData['contract_value'] = [];
    } else {
        let newCost = []
        for (let room in formData['contract_value']) {
            if (formData['contract_value'].hasOwnProperty(room)) {
                for (let regime in formData['contract_value'][room]) {
                    if (formData['contract_value'][room].hasOwnProperty(regime)) {
                        newCost.push({
                            'roomId': parseInt(room.replace('room_id_', '')),
                            'regimeId': parseInt(regime.replace('regime_id_', '')),
                            'value': parseFloat(formData['contract_value'][room][regime]['value']),
                            'sale_value': parseFloat(formData['contract_value'][room][regime]['sale_value'])
                        })
                    }
                }
            }
        }
        formData['contract_value'] = newCost;
    }
}

const deleteRangeRelatedValue = async rangeId => {
    if (!Array.isArray(rangeId)) {
        rangeId = [rangeId];
    }
    let queryString = `DELETE FROM data.contract_value
                             WHERE rel_contract_range = ANY($1)`
    let conn = await db.getConnection();
    await conn.query(queryString, [rangeId]);
    await conn.end();
}

const insertContractValue = async (rangeId, contractValueArray) => {
    let queryString = `INSERT INTO data.contract_value(rel_contract_range,
                                                       rel_room, 
                                                       rel_regime, 
                                                       value,
                                                       sale_value)
                                                VALUES `;
    let parameters = [];
    contractValueArray.forEach(el => {
        queryString += '($' + (parameters.length + 1) + ', $' + (parameters.length + 2) + ', $' + (parameters.length + 3) + ', $' + (parameters.length + 4) + ', $' + (parameters.length + 5) + '),\n'
        parameters.push(rangeId, el.roomId, el.regimeId, el.value, el.sale_value);
    })
    queryString = queryString.slice(0, -2);
    let conn = await db.getConnection();
    let res = await conn.query(queryString, parameters);
    await conn.end();
    return res.rows;
}


const insertRange = async (contractId, data) => {
    let queryString = `INSERT INTO data.contract_range(rel_contract, 
                                                       rel_regime, 
                                                       date_start, 
                                                       date_end,
                                                       release,
                                                       min_nights,
                                                       extra_pax_discount,
                                                       children_discount)
                                                VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
                                             RETURNING id`
    let parameters = [
        contractId,
        data.regime,
        data.start,
        data.end,
        data.release,
        data.min_nights,
        data.extra_pax_discount,
        data.children_discount
    ]
    let conn = await db.getConnection();
    let res = await conn.query(queryString, parameters);
    await conn.end();
    return res.rows[0].id
}

const deleteRange = async rangeId => {
    let queryString = `DELETE FROM data.contract_range
                             WHERE id = $1`
    let conn = await db.getConnection();
    await conn.query(queryString, [rangeId]);
    await conn.end();
}

const deleteRangeHandler = async rangeId => {
    await deleteRange(rangeId);
    await deleteRangeRelatedValue(rangeId);
}

const insertRangeHandler = async (contractId, data) => {
    cleanForm(data);
    let rangeId = await insertRange(contractId, data);
    if (data.contract_value.length > 0) {
        await insertContractValue(rangeId, data.contract_value);
    }
    return rangeId
}

const updateRangeHandler = async (rangeId, data) => {
    cleanForm(data);
    await updateRange(rangeId, data);
    await deleteRangeRelatedValue(rangeId);
    if (data.contract_value.length > 0) {
        await insertContractValue(rangeId, data.contract_value);
    }
    return rangeId;
}


const updateRange = async (rangeId, data) => {
    let queryString = `UPDATE data.contract_range
                          SET rel_regime=$2, 
                              date_start=$3, 
                              date_end=$4,
                              release=$5,
                              min_nights=$6,
                              extra_pax_discount=$7,
                              children_discount=$8
                        WHERE id=$1
                    RETURNING id`
    let parameters = [
        rangeId,
        data.regime,
        data.start,
        data.end,
        data.release,
        data.min_nights,
        data.extra_pax_discount,
        data.children_discount
    ]
    let conn = await db.getConnection();
    let res = await conn.query(queryString, parameters);
    await conn.end();
    return res.rows[0].id
}

const duplicateRangeHandler = async (rangeId) => {
    let conn = await db.getConnection();
    let duplicateRangeId = await duplicateRange(rangeId, conn, false);
    await duplicateAssociatedRangeValues(rangeId, duplicateRangeId, conn, true);
    return duplicateRangeId
}

const duplicateRange = async (rangeId, conn = null, close = true) => {
    let queryString = `INSERT INTO data.contract_range(rel_contract, 
                                                       rel_regime, 
                                                       date_start, 
                                                       date_end,
                                                       release,
                                                       min_nights,
                                                       extra_pax_discount,
                                                       children_discount)
                                                SELECT rel_contract, 
                                                       rel_regime, 
                                                       date_start, 
                                                       date_end,
                                                       release,
                                                       min_nights,
                                                       extra_pax_discount,
                                                       children_discount
                                                  FROM data.contract_range 
                                                 WHERE id=$1
                                             RETURNING id`
    if (!conn) {
        conn = await db.getConnection();
    }
    let res = await conn.query(queryString, [rangeId]);
    if (close) {
        await conn.end();
    }
    return res.rows[0].id
}

const duplicateAssociatedRangeValues = async (rangeId, duplicateRangeId, conn = null, close = true) => {
    let queryString = `INSERT INTO data.contract_value(rel_contract_range,
                                                       rel_room, 
                                                       rel_regime, 
                                                       value,
                                                       sale_value)
                                                SELECT $2 AS rel_contract_range,
                                                       rel_room, 
                                                       rel_regime, 
                                                       value,
                                                       sale_value
                                                  FROM data.contract_value 
                                                       WHERE rel_contract_range=$1
                                                   RETURNING id`
    if (!conn) {
        conn = await db.getConnection();
    }
    let res = await conn.query(queryString, [rangeId, duplicateRangeId]);
    if (close) {
        await conn.end();
    }
    return res.rows;
}

module.exports = {
    insertRangeHandler: insertRangeHandler,
    updateRangeHandler: updateRangeHandler,
    deleteRangeHandler: deleteRangeHandler,
    deleteRangeRelatedValue: deleteRangeRelatedValue,
    duplicateRangeHandler: duplicateRangeHandler
}
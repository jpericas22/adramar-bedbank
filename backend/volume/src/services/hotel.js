'use strict';
const db = require('../lib/db')

const cleanForm = (formData) => {
    let removeEmptyStringList = ['children_max_age']
    let sortNumberList = ['children_max_age']
    for (let key in formData) {
        if (formData.hasOwnProperty(key)) {
            if (key === 'active') {
                formData[key] = formData[key][0] === 'on';
            }
            if (formData[key] === '') {
                formData[key] = null;
            }
            removeEmptyStringList.forEach(el => {
                if (el in formData){
                    formData[el] = formData[el].filter( item => {
                        return item !== ''
                    })
                }
            })
            sortNumberList.forEach(el => {
                if (el in formData){
                    formData[el] = formData[el].sort((a, b) => a - b);
                }
            })
        }
    }
    if (formData.geolocation && formData.geolocation.some(el => {
        return el === '';
    })) {
        formData.geolocation = null;
    }
    console.log(formData)
}

const getHotel = async (filters = {}, limit = 0, page = 0) => {
    let paginatorQueryString = `SELECT count(id) FROM data.hotel`
    let queryString = `SELECT h.id, 
                              h.name, 
                              h.description, 
                              h.address, 
                              h.postal_code, 
                              h.geolocation, 
                              h.email, 
                              h.email_cc, 
                              h.email_booking, 
                              h.phone, 
                              h.fax, 
                              h.children_max_age, 
                              h.infant_max_age, 
                              p.id AS provider_id, 
                              p.fiscal_name AS provider_name, 
                              c.id AS city_id, 
                              c.name AS city_name, 
                              re.id AS region_id, 
                              re.name AS region_name, 
                              co.id AS country_id, 
                              co.name AS country_name, 
                              ac.id AS accomodation_category_id, 
                              ac.name AS accomodation_category_name, 
                              at.id AS accomodation_type_id, 
                              at.name AS accomodation_type_name, 
                              ch.id AS chain_id, 
                              ch.name AS chain_name, 
							  COALESCE(jsonb_agg(DISTINCT ex.* ORDER BY ex.*) FILTER (WHERE ex.id IS NOT NULL), '[]') AS extras,
							  COALESCE(jsonb_agg(DISTINCT r.* ORDER BY r.*) FILTER (WHERE r.id IS NOT NULL), '[]') AS room,
							  COALESCE(jsonb_agg(DISTINCT con.* ORDER BY con.*) FILTER (WHERE con.id IS NOT NULL), '[]') AS contract,
							  COALESCE(jsonb_agg(DISTINCT conr.* ORDER BY conr.*) FILTER (WHERE conr.id IS NOT NULL), '[]') AS contract_range,
                              COALESCE(jsonb_agg(DISTINCT s.* ORDER BY s.*) FILTER (WHERE s.id IS NOT NULL), '[]') AS sale,
                              COALESCE(jsonb_agg(DISTINCT conr.* ORDER BY conr.*) FILTER (WHERE conr.id IS NOT NULL), '[]') AS sale_range,
                              COALESCE(jsonb_agg(DISTINCT q.* ORDER BY q.*) FILTER (WHERE q.id IS NOT NULL), '[]') AS quota,
                              COALESCE(jsonb_agg(DISTINCT qr.* ORDER BY qr.*) FILTER (WHERE qr.id IS NOT NULL), '[]') AS quota_range,
                              h.active 
                         FROM data.hotel AS h
	                LEFT JOIN data.provider AS p ON p.id = h.rel_provider
                    LEFT JOIN data.city AS c ON c.id = h.rel_city
                    LEFT JOIN data.region AS re ON re.id = c.rel_region
                    LEFT JOIN data.country AS co ON co.id = re.rel_country
                    LEFT JOIN data.accomodation_category AS ac ON ac.id = h.rel_accomodation_category
                    LEFT JOIN data.accomodation_type AS at ON at.id = h.rel_accomodation_type
                    LEFT JOIN data.chain AS ch ON ch.id = h.rel_chain
                    LEFT JOIN data.extras AS ex ON ex.id = ANY(h.rel_extras)
					LEFT JOIN data.room AS r ON r.rel_hotel = h.id
					LEFT JOIN data.contract AS con ON con.rel_hotel = h.id
					LEFT JOIN data.contract_range AS conr ON conr.rel_contract = con.id
					LEFT JOIN data.sale AS s ON s.rel_contract = con.id
					LEFT JOIN data.quota AS q ON q.rel_hotel = h.id
                    LEFT JOIN data.quota_range AS qr ON qr.rel_quota = q.id
                        WHERE TRUE\n`;
    let params = [];


    for (const filter in filters) {
        params.push(filters[filter]);
        queryString += 'AND ' + filter + '= $' + params.length + '\n';
    }

    queryString += 'GROUP BY h.id, p.id, c.id, re.id, co.id, ac.id, at.id, ch.id\n'
    if (limit) {
        params.push(limit, page * limit)
        queryString += 'LIMIT $' + (params.length - 1) + 'OFFSET $' + params.length + '\n';
    }

    let conn = await db.getConnection();
    let paginatorRes = await conn.query(paginatorQueryString, []);
    let totalItems = paginatorRes.rows[0].count;
    let totalPages = Math.ceil(totalItems / limit);


    let res = await conn.query(queryString, params);
    await conn.end();
    return {
        data: res.rows,
        paginator: {
            pageNumber: page + 1,
            totalPages: totalPages,
            totalItems: totalItems,
            currentItems: res.rows.length
        }
    };

}

const getHotelReduced = async (filters = {}, limit = 0, page = 0) => {
    let paginatorQueryString = `SELECT count(id) FROM data.hotel`
    let queryString = `SELECT h.id, 
                              h.name, 
                              h.postal_code, 
                              c.id AS city_id, 
                              c.name AS city_name, 
                              re.id AS region_id, 
                              re.name AS region_name, 
                              co.id AS country_id, 
                              co.name AS country_name, 
                              ac.id AS accomodation_category_id, 
                              ac.name AS accomodation_category_name, 
                              at.id AS accomodation_type_id, 
                              at.name AS accomodation_type_name, 
                              ch.id AS chain_id, 
                              ch.name AS chain_name, 
                              h.active 
                         FROM data.hotel AS h
                    LEFT JOIN data.city AS c ON c.id = h.rel_city
                    LEFT JOIN data.region AS re ON re.id = c.rel_region
                    LEFT JOIN data.country AS co ON co.id = re.rel_country
                    LEFT JOIN data.accomodation_category AS ac ON ac.id = h.rel_accomodation_category
                    LEFT JOIN data.accomodation_type AS at ON at.id = h.rel_accomodation_type
                    LEFT JOIN data.chain AS ch ON ch.id = h.rel_chain
                        WHERE TRUE\n`;
    let params = [];

    for (const filter in filters) {
        params.push(filters[filter]);
        queryString += 'AND ' + filter + '= $' + params.length + '\n';
    }

    if (limit) {
        params.push(limit, page * limit)
        queryString += 'LIMIT $' + (params.length - 1) + 'OFFSET $' + params.length + '\n';
    }

    let conn = await db.getConnection();
    let paginatorRes = await conn.query(paginatorQueryString, []);
    let totalItems = paginatorRes.rows[0].count;
    let totalPages = Math.ceil(totalItems / limit);


    let res = await conn.query(queryString, params);
    await conn.end();
    return {
        data: res.rows,
        paginator: {
            pageNumber: page + 1,
            totalPages: totalPages,
            totalItems: totalItems,
            currentItems: res.rows.length
        }
    };

}

const insertHotel = async data => {
    cleanForm(data);
    let queryString = `INSERT INTO data.hotel(name, 
                                              description, 
                                              active,
                                              email, 
                                              email_cc,
                                              email_booking,
                                              phone, 
                                              fax,
                                              rel_city,
                                              address, 
                                              postal_code, 
                                              geolocation, 
                                              rel_accomodation_type,
                                              rel_accomodation_category,
                                              rel_chain,
                                              rel_provider,
                                              rel_extras,
                                              infant_max_age,
                                              children_max_age)
                                       VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19)
                                    RETURNING id`
                                    //infant_age_limit, age_range ??
    let params = [
        data.name,
        data.description,
        data.active,
        data.email,
        data.email_cc,
        data.email_booking,
        data.phone,
        data.fax,
        data.city,
        data.address,
        data.postal_code,
        data.geolocation,
        data.accomodation_type,
        data.accomodation_category,
        data.chain,
        data.provider,
        data.extras || [],
        data.infant_max_age || 0,
        data.children_max_age || []
    ]
    let conn = await db.getConnection();
    let res = await conn.query(queryString, params);
    await conn.end();
    return res.rows[0].id
}

const editDetails = async (id, data) => {
    cleanForm(data);
    let queryString = `UPDATE data.hotel
                          SET name=$2,
                              description=$3,
                              active=$4,
                              email=$5, 
                              email_cc=$6,
                              email_booking=$7,
                              phone=$8, 
                              fax=$9,
                              rel_city=$10,
                              address=$11, 
                              postal_code=$12, 
                              geolocation=$13, 
                              rel_accomodation_type=$14,
                              rel_accomodation_category=$15,
                              rel_chain=$16,
                              rel_provider=$17,
                              rel_extras=$18,
                              infant_max_age=$19,
                              children_max_age=$20
                        WHERE id=$1
                    RETURNING id`
    let params = [
        id,
        data.name,
        data.description,
        data.active,
        data.email,
        data.email_cc,
        data.email_booking,
        data.phone,
        data.fax,
        data.city,
        data.address,
        data.postal_code,
        data.geolocation,
        data.accomodation_type,
        data.accomodation_category,
        data.chain,
        data.provider,
        data.extras || [],
        data.infant_max_age || 0,
        data.children_max_age || []
    ]
    let conn = await db.getConnection();
    let res = await conn.query(queryString, params);
    await conn.end();
    return res.rows[0]
}


const searchHotel = async (text) => {
    let queryString = `SELECT id, 
                            name 
                       FROM data.hotel 
                      WHERE name ILIKE $1 LIMIT 5`
    let params = [text + '%'];

    let conn = await db.getConnection();
    let res = await conn.query(queryString, params);
    await conn.end();
    return res.rows;
}

module.exports = {
    getHotel: getHotel,
    getHotelReduced: getHotelReduced,
    insertHotel: insertHotel,
    searchHotel: searchHotel,
    editDetails: editDetails,
}
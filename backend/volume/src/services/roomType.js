'use strict';
const db = require('../lib/db')

const cleanForm = formData => {
    let checkboxList = ['active'];
    let emptyDefaultList = ['pax_range'];
    for (let key in formData) {
        if (formData.hasOwnProperty(key)) {
            if (checkboxList.includes(key)) {
                formData[key] = formData[key][0] === 'on';
            }
            if (formData[key] === '') {
                formData[key] = null;
            }
            emptyDefaultList.forEach(el => {
                if (!(el in formData)) {
                    formData[el] = [];
                }
            })
        }
    }
}

const getRoomType = async (conn = null, json = false) => {
    let closeConn = false;
    let queryString = `SELECT rt.id, 
                              rt.name, 
                              rt.description,
                              COALESCE(jsonb_agg(DISTINCT pr.* ORDER BY pr.*) FILTER (WHERE pr.id IS NOT NULL), '[]') AS pax_range,
                              rt.min_pax, 
                              rt.max_pax, 
                              rt.active
                         FROM data.room_type AS rt
                    LEFT JOIN data.pax_range AS pr ON rt.id = pr.rel_room_type
                     GROUP BY rt.id`;

    if (!conn) {
        closeConn = true;
        conn = await db.getConnection();
    }
    let res = await conn.query(queryString, []);
    if (closeConn) {
        await conn.end();
    }
    if (json) {
        return {
            roomType: res.rows
        }
    }
    return res.rows;
}

const clearRelatedRoomPaxRange = async roomTypeId => {
    let queryString = `DELETE FROM data.pax_range
                             WHERE rel_room_type = $1`
    let conn = await db.getConnection();
    await conn.query(queryString, [roomTypeId])
}

const insertRoomPaxRange = async (roomTypeId, paxRangeArray) => {
    let queryString = `INSERT INTO data.pax_range(rel_room_type, 
                                                 adult_min_pax, 
                                                 adult_max_pax, 
                                                 children_min_pax, 
                                                 children_max_pax)
                                          VALUES `
    let parameters = [];
    paxRangeArray.forEach(el => {
        queryString += '($' + (parameters.length + 1) + ', $' + (parameters.length + 2) + ', $' + (parameters.length + 3) + ', $' + (parameters.length + 4) + ', $' + (parameters.length + 5) + '),\n'
        parameters.push(roomTypeId, el.adult.min, el.adult.max, el.children.min, el.children.max);
    })
    queryString = queryString.slice(0, -2);
    let conn = await db.getConnection();
    let res = await conn.query(queryString, parameters);
    await conn.end();
    return res.rows;
}

const insertRoomType = async data => {
    let queryString = `INSERT INTO data.room_type(name, 
                                                  description, 
                                                  min_pax, 
                                                  max_pax, 
                                                  active)
                                           VALUES ($1, $2, $3, $4, $5)
                                        RETURNING id`
    let parameters = [
        data.name,
        data.description,
        data.min_pax,
        data.max_pax,
        data.active
    ]
    let conn = await db.getConnection();
    let res = await conn.query(queryString, parameters);
    return res.rows[0].id
}

const updateRoomType = async (roomTypeId, data) => {
    let queryString = `UPDATE data.room_type
                          SET name=$2, 
                              description=$3, 
                              min_pax=$4, 
                              max_pax=$5, 
                              active=$6
                        WHERE id = $1
                    RETURNING id`
    let parameters = [
        roomTypeId,
        data.name,
        data.description,
        data.min_pax,
        data.max_pax,
        data.active
    ]
    let conn = await db.getConnection();
    let res = await conn.query(queryString, parameters);
    return res.rows[0].id
}

const deleteRoomType = async roomTypeId => {
    let queryString = `DELETE FROM data.room_type
	                         WHERE id=$1`
    let parameters = [
        roomTypeId
    ]
    let conn = await db.getConnection();
    await conn.query(queryString, parameters);
}

const insertRoomTypeHandler = async data => {
    cleanForm(data);
    let roomTypeId = await insertRoomType(data);
    if (data.pax_range.length > 0){
        await insertRoomPaxRange(roomTypeId, data.pax_range);
    }
    return roomTypeId;
}

const updateRoomTypeHandler = async (roomTypeId, data) => {
    cleanForm(data);
    await updateRoomType(roomTypeId, data);
    await clearRelatedRoomPaxRange(roomTypeId);
    if (data.pax_range.length > 0) {
        await insertRoomPaxRange(roomTypeId, data.pax_range);
    }
    return roomTypeId;
}

const deleteRoomTypeHandler = async roomTypeId => {
    await clearRelatedRoomPaxRange(roomTypeId);
    await deleteRoomType(roomTypeId);
}

module.exports = {
    getRoomType: getRoomType,
    insertRoomTypeHandler: insertRoomTypeHandler,
    updateRoomTypeHandler: updateRoomTypeHandler,
    deleteRoomTypeHandler: deleteRoomTypeHandler
}
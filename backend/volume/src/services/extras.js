
'use strict';
const db = require('../lib/db');
//TODO CHANGE NAME TO "SUPLEMENTS"
const getExtras = async (con = null, json = false) => {
    let closeCon = false;
    let queryString = `SELECT ex.id, 
                              ex.name,
                              ex.description
                         FROM data.extras AS ex`;
    if (!con) {
        closeCon = true;
        con = await db.getConnection();
    }
    let res = await con.query(queryString, []);
    if (closeCon) {
        await con.end();
    }
    if (json) {
        return {
            extras: res.rows
        }
    }
    return res.rows;
}

//TODO CHANGE NAME TO "SUPLEMENTS"
const insertExtras = async (data, con = null) => {
    let closeCon = false;
    let queryString = `INSERT INTO data.extras(name, 
                                               description)
                            VALUES ($1, $2)
                         RETURNING id`;
    let parameters = [
        data.name,
        data.description
    ]
    if (!con) {
        closeCon = true;
        con = await db.getConnection();
    }
    let res = await con.query(queryString, parameters);
    if (closeCon) {
        await con.end();
    }
    return res.rows[0].id;
}

const updateExtras = async (extrasId, data, con = null) => {
    let closeCon = false;
    let queryString = `UPDATE data.extras
                          SET name=$2, 
                              description=$3
                        WHERE id=$1
                    RETURNING id`;
    let parameters = [
        extrasId,
        data.name,
        data.description
    ]
    if (!con) {
        closeCon = true;
        con = await db.getConnection();
    }
    let res = await con.query(queryString, parameters);
    if (closeCon) {
        await con.end();
    }
    return res.rows[0].id;
}

const deleteExtras = async (extrasId, con = null) => {
    let closeCon = false;
    let queryString = `DELETE FROM data.extras
                             WHERE id=$1
                         RETURNING id`;
    let parameters = [
        extrasId
    ]
    if (!con) {
        closeCon = true;
        con = await db.getConnection();
    }
    let res = await con.query(queryString, parameters);
    if (closeCon) {
        await con.end();
    }
    return res.rows[0].id;
}

module.exports = {
    getExtras: getExtras,
    insertExtras: insertExtras,
    updateExtras: updateExtras,
    deleteExtras: deleteExtras
}
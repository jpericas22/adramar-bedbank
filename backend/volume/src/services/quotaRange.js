
'use strict';
const db = require('../lib/db')

const cleanForm = formData => {
    
    let emptyDefaultList = ['quota_value'];
    for (let key in formData) {
        if (formData.hasOwnProperty(key)) {
            if (formData[key] === '') {
                formData[key] = null;
            }
            emptyDefaultList.forEach(el => {
                if (!(el in formData)) {
                    formData[el] = [];
                }
            })
        }
    }
    formData['quota_value'].forEach( el => {
        switch (el['quota_modifier']) {
            case 'free_sale':
                el.free_sale = true;
                el.shared_quota_room_id = null
                break;
            case 'on_request':
                el.on_request = true;
                el.shared_quota_room_id = el['hotel_id_shared_quota']
                break;
            default:
                el.shared_quota_room_id = el['hotel_id_shared_quota']
                break;
        }
    })
}

const deleteRangeRelatedValue = async rangeId => {
    if (!Array.isArray(rangeId)){
        rangeId = [rangeId];
    }
    let queryString = `DELETE FROM data.quota_value
                             WHERE rel_quota_range = ANY($1)`
    let conn = await db.getConnection();
    await conn.query(queryString, [rangeId]);
    await conn.end();
}

const insertQuotaValue = async (rangeId, quotaValueArray) => {
    let queryString = `INSERT INTO data.quota_value(rel_quota_range,
                                             rel_room,
                                             value,
                                             rel_room_shared_quota,
                                             free_sale,
                                             on_request)
                                             VALUES `;
    let parameters = [];
    quotaValueArray.forEach(el => {
        console.log(el)
        queryString += '($' + (parameters.length + 1) + ', $' + (parameters.length + 2) + ', $' + (parameters.length + 3) + ', $' + (parameters.length + 4) + ', $' + (parameters.length + 5) + ', $' + (parameters.length + 6) + '),\n'
        parameters.push(rangeId, el.room_id, el.value || null, el.shared_quota_room_id || null, !!el.free_sale, !!el.on_request);
    })
    queryString = queryString.slice(0, -2);
    let conn = await db.getConnection();
    let res = await conn.query(queryString, parameters);
    await conn.end();
    return res.rows;
}


const insertRange = async (quotaId, data) => {
    console.log(data)
    let queryString = `INSERT INTO data.quota_range(rel_quota,
                                                    date_start, 
                                                    date_end,
                                                    priority)
                                             VALUES ($1, $2, $3, $4)
                                          RETURNING id`
    let parameters = [
        quotaId,
        data.start,
        data.end,
        data.priority
    ]
    let conn = await db.getConnection();
    let res = await conn.query(queryString, parameters);
    await conn.end();
    return res.rows[0].id
}

const deleteRange = async rangeId => {
    let queryString = `DELETE FROM data.quota_range
                             WHERE id = $1`
    let conn = await db.getConnection();
    await conn.query(queryString, [rangeId]);
    await conn.end();
}

const deleteRangeHandler = async rangeId => {
    await deleteRange(rangeId);
    await deleteRangeRelatedValue(rangeId);
}

const insertRangeHandler = async (quotaId, data) => {
    cleanForm(data);
    let rangeId = await insertRange(quotaId, data);
    if (data.quota_value.length > 0) {
        await insertQuotaValue(rangeId, data.quota_value);
    }
    return rangeId
}

const updateRangeHandler = async (rangeId, data) => {
    cleanForm(data);
    await updateRange(rangeId, data);
    await deleteRangeRelatedValue(rangeId);
    if (data.quota_value.length > 0) {
        await insertQuotaValue(rangeId, data.quota_value);
    }
    return rangeId;
}


const updateRange = async (rangeId, data) => {
    console.log(data)

    let queryString = `UPDATE data.quota_range
                          SET date_start=$2, 
                              date_end=$3,
                              priority=$4
                        WHERE id=$1
                    RETURNING id`
    let parameters = [
        rangeId,
        data.start,
        data.end,
        data.priority
    ]
    let conn = await db.getConnection();
    let res = await conn.query(queryString, parameters);
    await conn.end();
    return res.rows[0].id
}

module.exports = {
    insertRangeHandler: insertRangeHandler,
    updateRangeHandler: updateRangeHandler,
    deleteRangeHandler: deleteRangeHandler,
    deleteRangeRelatedValue: deleteRangeRelatedValue
}
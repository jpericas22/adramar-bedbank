'use strict'
const db = require('../lib/db');

const cleanForm = (formData) => {
    let checkboxList = ['active'];
    for (let key in formData) {
        if (formData.hasOwnProperty(key)) {
            if (checkboxList.includes(key)) {
                formData[key] = formData[key][0] === 'on';
            }
            if (formData[key] === '') {
                formData[key] = null;
            }
        }
    }
}

const getProvider = async (conn = null, filter = {}, limit = 0, page = 0) => {
    let paginatorQueryString = `SELECT count(id) FROM data.provider`
    let queryString = `SELECT p.id, 
                              p.fiscal_name, 
                              p.fiscal_code, 
                              p.contact_name, 
                              p.phone, 
                              p.fax, 
                              p.accounting_id, 
                              p.address, 
                              c.id AS city_id, 
                              c.name AS city_name, 
                              re.id AS region_id, 
                              re.name AS region_name, 
                              co.id AS country_id, 
                              co.name AS country_name,
                              p.active
	                     FROM data.provider AS p
                    LEFT JOIN data.city AS c ON c.id = p.rel_city
                    LEFT JOIN data.region AS re ON re.id = c.rel_region
                    LEFT JOIN data.country AS co ON co.id = re.rel_country
                      GROUP BY p.id, c.id, re.id, co.id\n`;
    let params = [];

    if (limit) {
        queryString += `LIMIT $1 OFFSET $2`;
        params.push(limit, page * limit)
    }

    if (!conn) {
        conn = await db.getConnection();
    }

    let paginatorRes = await conn.query(paginatorQueryString, []);
    let totalItems = paginatorRes.rows[0].count;
    let totalPages = Math.ceil(totalItems / limit);
    let res = await conn.query(queryString, params);
    await conn.end();

    return {
        data: res.rows,
        paginator: {
            pageNumber: page + 1,
            totalPages: totalPages,
            totalItems: totalItems,
            currentItems: res.rows.length
        }
    };
}

const insertProvider = async data => {
    cleanForm(data);
    let queryString = `INSERT INTO data.provider(fiscal_name, 
                                                 fiscal_code, 
                                                 contact_name, 
                                                 phone, 
                                                 fax, 
                                                 accounting_id, 
                                                 address, 
                                                 rel_city, 
                                                 active)
                                         VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)
                                      RETURNING id`
    let parameters = [
        data.fiscal_name,
        data.fiscal_code,
        data.contact_name,
        data.phone,
        data.fax,
        data.accounting_id,
        data.address,
        data.city,
        data.active,
    ]

    let conn = await db.getConnection();
    let res = await conn.query(queryString, parameters);
    return res.rows[0].id;
}

const updateProvider = async (providerId, data) => {
    cleanForm(data);
    let queryString = `UPDATE data.provider
                          SET fiscal_name=$2, 
                              fiscal_code=$3, 
                              contact_name=$4, 
                              phone=$5, 
                              fax=$6,
                              accounting_id=$7, 
                              address=$8, 
                              rel_city=$9, 
                              active=$10
                        WHERE id=$1
                    RETURNING id`
    let parameters = [
        providerId,
        data.fiscal_name,
        data.fiscal_code,
        data.contact_name,
        data.phone,
        data.fax,
        data.accounting_id,
        data.address,
        data.city,
        data.active,
    ]

    let conn = await db.getConnection();
    let res = await conn.query(queryString, parameters);
    return res.rows[0].id;
}

const deletePovider = async providerId => {
    let queryString = `DELETE FROM data.provider
                             WHERE id = $1
                         RETURNING id`
    let conn = await db.getConnection();
    let res = await conn.query(queryString, [providerId]);
    return res.rows[0].id;
}

module.exports = {
    getProvider: getProvider,
    insertProvider: insertProvider,
    updateProvider: updateProvider,
    deletePovider: deletePovider
}
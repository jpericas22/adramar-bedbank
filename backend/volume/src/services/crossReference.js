'use strict';
const db = require('../lib/db')

const CROSSREFERENCE_WHITELIST = ['webbeds']
const getClientData = async crossreference => {
    if (!CROSSREFERENCE_WHITELIST.includes(crossreference)) {
        return;
    }
    let queryString = `SELECT cr_h.client_id AS client_id,
                              cr_h.client_name AS client_name,
                              COALESCE(jsonb_agg(cr_r.* ORDER BY cr_r.*), '[]') AS room
                         FROM crossreference.`+ crossreference + `_hotel AS cr_h
                    LEFT JOIN crossreference.`+ crossreference + `_room AS cr_r ON cr_r.client_hotel_id = cr_h.client_id
                    GROUP BY cr_h.client_id`
    let conn = await db.getConnection();
    let res = await conn.query(queryString, []);
    await conn.end();
    return res.rows;
}

const get = async (crossreference, filters = {}) => {
    if (!CROSSREFERENCE_WHITELIST.includes(crossreference)) {
        return;
    }
    let queryString = `SELECT h.id AS id,
                              h.name AS name,
                              cr_h.client_id AS client_id,
                              cr_h.client_name AS client_name,
							  COALESCE(jsonb_agg(DISTINCT r.* ORDER BY r.*) FILTER (WHERE r.id IS NOT NULL), '[]') AS room,
							  COALESCE(jsonb_agg(DISTINCT cr_r.* ORDER BY cr_r.*) FILTER (WHERE cr_r.client_id IS NOT NULL), '[]') AS client_room
                         FROM data.hotel AS h
                    LEFT JOIN data.room AS r ON r.rel_hotel = h.id
                    LEFT JOIN crossreference.`+ crossreference + `_hotel AS cr_h ON cr_h.rel_hotel = h.id
                    LEFT JOIN crossreference.`+ crossreference + `_room AS cr_r ON cr_r.rel_room = r.id
                    WHERE TRUE\n`

    let params = []

    for (const filter in filters) {
        params.push(filters[filter]);
        queryString += 'AND ' + filter + '=$' + params.length + '\n';
    }

    let conn = await db.getConnection();
    queryString += 'GROUP BY h.id, cr_h.client_id, r.id'
    let res = await conn.query(queryString, params);
    await conn.end();
    return res.rows;
}

const setHotel = async (hotelId, clientHotelId, crossreference, conn = null, close = true) => {
    if (!conn) {
        conn = await db.getConnection();
    }
    let queryString = `UPDATE crossreference.` + crossreference + `_hotel
	                      SET rel_hotel=$1,
                              "timestamp"=NOW()
	                    WHERE client_id=$2
                    RETURNING client_id
                    `
    let res = await conn.query(queryString, [hotelId, clientHotelId]);
    if (close) {
        await conn.end();
    }
    return res.rows[0];
}

const cleanRelatedHotel = async (hotelId, crossreference, conn = null, close = true) => {
    if (!conn) {
        conn = await db.getConnection();
    }
    let queryString = `UPDATE crossreference.` + crossreference + `_hotel
	                      SET rel_hotel=NULL,
                              "timestamp"=NOW()
	                    WHERE rel_hotel=$1`
    let res = await conn.query(queryString, [hotelId]);
    if (close) {
        await conn.end();
    }
    return res.rows[0];
}

const cleanRelatedRoom = async (roomId, crossreference, conn = null, close = true) => {
    if (!conn) {
        conn = await db.getConnection();
    }
    console.log('ayo')
    console.log(roomId)
    let queryString = `UPDATE crossreference.` + crossreference + `_room
	                      SET rel_room=NULL,
                              "timestamp"=NOW()
	                    WHERE rel_room=$1`
    let res = await conn.query(queryString, [roomId]);
    if (close) {
        await conn.end();
    }
    return res.rows[0];
}

const setRoom = async (roomId, clientRoomId, crossreference, conn = null, close = true) => {
    if (!conn) {
        conn = await db.getConnection();
    }
    let queryString = `UPDATE crossreference.` + crossreference + `_room
	                      SET rel_room=$1,
                              "timestamp"=NOW()
	                    WHERE client_id=$2
                    RETURNING client_id`
    let res = await conn.query(queryString, [roomId, clientRoomId]);
    if (close) {
        await conn.end();
    }
    console.log(res.rows);
    return res.rows[0];
}

const setHotelHandler = async (hotelId, clientHotelId, crossreference) => {
    let conn = await db.getConnection();
    await cleanRelatedHotel(hotelId, crossreference, conn, false);
    await setHotel(hotelId, clientHotelId, crossreference, conn, false);
    let roomsToUpdate = (await get(crossreference, {'h.id': hotelId}))[0].client_room;
    if(roomsToUpdate.find(el => {
        return el.client_hotel_id !== clientHotelId
    })){
        await Promise.all(roomsToUpdate.map( el => {
            return cleanRelatedRoom(el.rel_room, crossreference, conn, false);
        }))
    }
    await conn.end();
}

const setRoomHandler = async (roomId, clientRoomId=[], crossreference) => {
    let conn = await db.getConnection();
    await cleanRelatedRoom(roomId, crossreference, conn, false);
    await Promise.all(clientRoomId.map( el => {
        return setRoom(roomId, el, crossreference, conn, false)
    }))
    await conn.end();
}


module.exports = {
    getClientData: getClientData,
    get: get,
    setHotel: setHotel,
    setRoom: setRoom,
    setHotelHandler: setHotelHandler,
    setRoomHandler: setRoomHandler
}
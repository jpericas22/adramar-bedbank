'use strict';
const db = require('../lib/db')

const cleanForm = (formData) => {
    let checkboxList = ['kids_as_adult', 'infants_as_adult'];
    for (let key in formData) {
        if (formData.hasOwnProperty(key)) {
            if (checkboxList.includes(key)) {
                formData[key] = formData[key][0] === 'on';
            }
            if (formData[key] === '') {
                formData[key] = null;
            }
        }

    }
}

const getRoom = async (filters = {}, limit = 0, page = 0) => {
    let paginatorQueryString = `SELECT count(id) FROM data.room`
    let queryString = `SELECT r.id, 
                              h.id AS hotel_id, 
                              h.name AS hotel_name, 
                              r.name, 
                              r.description, 
                              rt.id AS room_type_id, 
                              rt.name AS room_type_name, 
                              r.kids_as_adult, 
                              r.infants_as_adult
                         FROM data.room AS r
                         LEFT JOIN data.hotel AS h ON h.id = r.rel_hotel
                         LEFT JOIN data.room_type AS rt ON rt.id = r.rel_room_type
                        WHERE TRUE\n`
    //TODO mirar si podemos quitar los LEFT JOIN y cambiarlos por JOIN
    let params = [];

    for (const filter in filters) {
        params.push(filters[filter]);
        queryString += 'AND ' + filter + '= $' + params.length + '\n';
    }
    if (limit) {
        params.push(limit, page * limit)
        queryString += 'LIMIT $' + (params.length - 1) + 'OFFSET $' + params.length;
    }

    let conn = await db.getConnection();
    let paginatorRes = await conn.query(paginatorQueryString, []);
    let totalItems = paginatorRes.rows[0].count;
    let totalPages = Math.ceil(totalItems / limit);
    let res = await conn.query(queryString, params);
    await conn.end();
    return {
        data: res.rows,
        paginator: {
            pageNumber: page + 1,
            totalPages: totalPages,
            totalItems: totalItems,
            currentItems: res.rows.length
        }
    };
};

const insertRoom = async (hotelId, data) => {
    cleanForm(data);
    let queryString = `INSERT INTO data.room(
                                   rel_hotel, 
                                   name, 
                                   description, 
                                   rel_room_type, 
                                   kids_as_adult, 
                                   infants_as_adult)
                            VALUES ($1, $2, $3, $4, $5, $6)
                         RETURNING id`;
    let parameters = [
        hotelId,
        data.name,
        data.description,
        data.room_type,
        data.kids_as_adult,
        data.infants_as_adult
    ]

    let conn = await db.getConnection();
    let res = await conn.query(queryString, parameters);
    return res.rows[0].id;
}

const updateRoom = async (roomId, data) => {
    cleanForm(data);
    let queryString = `UPDATE data.room
                          SET name=$2, 
                          description=$3, 
                          rel_room_type=$4, 
                          kids_as_adult=$5, 
                          infants_as_adult=$6
                    WHERE id=$1
                RETURNING id`;
    let parameters = [
        roomId,
        data.name,
        data.description,
        data.room_type,
        data.kids_as_adult,
        data.infants_as_adult
    ]

    let conn = await db.getConnection();
    let res = await conn.query(queryString, parameters);
    return res.rows[0].id;
}

const deleteRoom = async (roomId) => {
    let queryString = `DELETE FROM data.room
                             WHERE id=$1`;
    let parameters = [roomId];
    let conn = await db.getConnection();
    await conn.query(queryString, parameters);
}


module.exports = {
    getRoom: getRoom,
    insertRoom: insertRoom,
    updateRoom: updateRoom,
    deleteRoom: deleteRoom
}
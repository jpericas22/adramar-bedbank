'use strict'
const db = require('../lib/db');
const redis = require('../lib/redis');
const crypto = require('crypto');

const EXPIRE_MINS = 10;
const EXPIRE_VAL = EXPIRE_MINS * 60;
const SESSION_ID_LENGHT = 32

const genRandomString = length => {
    return crypto.randomBytes(Math.ceil(length / 2))
        .toString('hex')
        .slice(0, length);
};

const checkLogin = async (username, password) => {
    let con = await db.getConnection();
    let queryString = `SELECT id,
                              username,
                              password,
                              salt,
                              email,
                              active
                         FROM access.user
                        WHERE username=$1 AND active IS TRUE`;
    let res = await con.query(queryString, [username]);
    if (res.rows.length === 0) {
        throw new Error('user not found');
    }
    let userData = res.rows[0];
    let hash = crypto.createHmac('sha256', userData.salt);
    let inputSalted = hash.update(password).digest('hex');
    let dbSalted = userData.password;
    if (inputSalted !== dbSalted) {
        throw new Error('wrong password');
    }
    return (await createSession(userData.id));
}

const createSession = async userId => {
    let con = await redis.getConnection();
    let sessionId = genRandomString(SESSION_ID_LENGHT);
    let redisKey = 'session_' + sessionId;
    let res = await new Promise((resolve, reject) => {
        con.hmset(redisKey, {
            userId: userId
        }, (err, res) => {
            if (err) {
                reject(err);
                return;
            }
            con.expire(redisKey, EXPIRE_VAL, (expErr, expRes) => {
                if (expErr) {
                    reject(expErr);
                    return;
                }
                resolve(sessionId);
            })
        })
    })
    return res;
}

const checkSession = async sessionId => {
    let con = await redis.getConnection();
    let redisKey = 'session_' + sessionId;
    let res = await new Promise((resolve, reject) => {
        con.HGETALL(redisKey, (redisErr, redisRes) => {
            if (redisErr) {
                reject(redisErr);
                return;
            }
            resolve(redisRes);
        })
    })
    return res;
}
module.exports = {
    checkLogin: checkLogin,
    checkSession: checkSession
}
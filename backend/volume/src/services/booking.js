'use strict';
const db = require('../lib/db')

const getBooking = async (filters = {}, limit = 0, page = 0) => { //TODO CAMBIAR LEFT JOIN A JOIN
    let paginatorQueryString = `SELECT count(id) FROM data.booking`;
    let queryString = `SELECT b.id, 
                              c.id AS client_id,
                              c.name AS client_name,
                              h.id AS hotel_id,
                              h.name AS hotel_name,
                              ro.id AS room_id,
                              ro.name AS room_name, 
                              re.id AS regime_id,
                              re.name AS regime_name,
                              b.adults, 
                              b.children, 
                              b.infants, 
                              b.checkin_date, 
                              b.checkout_date, 
                              b.contact_name, 
                              b.contact_phone, 
                              b.contact_email, 
                              b.observations, 
                              b.status, 
                              b.booking_date, 
                              b.cancellation_date, 
                              b.no_show
                         FROM data.booking AS b
                         LEFT JOIN data.client AS c ON c.id = b.rel_client
                         LEFT JOIN data.hotel AS h ON h.id = b.rel_hotel
                         LEFT JOIN data.room AS ro ON ro.id = b.rel_room
                         LEFT JOIN data.regime AS re ON re.id = b.rel_regime
                         WHERE TRUE\n`

    let parameters = [];

    for (const filter in filters) {
        parameters.push(filters[filter]);
        queryString += 'AND ' + filter + '= $' + parameters.length + '\n';
    }
    if (limit) {
        parameters.push(limit, page * limit)
        queryString += 'LIMIT $' + (parameters.length - 1) + 'OFFSET $' + parameters.length + '\n';
    }
    let conn = await db.getConnection();
    let paginatorRes = await conn.query(paginatorQueryString, []);
    let totalItems = paginatorRes.rows[0].count;
    let totalPages = Math.ceil(totalItems / limit);
    let res = await conn.query(queryString, parameters);
    await conn.end();
    return {
        data: res.rows,
        paginator: {
            pageNumber: page + 1,
            totalPages: totalPages,
            totalItems: totalItems,
            currentItems: res.rows.length
        }
    };
}

module.exports = {
    getBooking: getBooking
}
'use strict';
const db = require('../lib/db')
const contractRange = require('./contractRange');

const cleanForm = (formData) => {
    let checkboxList = ['active'];
    let emptyDefaultList = ['room'];
    let removeEmptyStringList = ['children_max_age']
    for (let key in formData) {
        if (formData.hasOwnProperty(key)) {
            if (checkboxList.includes(key)) {
                formData[key] = formData[key][0] === 'on';
            }
            if (formData[key] === '') {
                formData[key] = null;
            }
            emptyDefaultList.forEach(el => {
                if (!(el in formData)) {
                    formData[el] = [];
                }
            })
        }
    }
}

const getContract = async (filters = {}, limit = 0, page = 0) => {
    let paginatorQueryString = `SELECT count(id) FROM data.contract`
    let queryString = `SELECT con.id,
                              con.name,
                              con.rel_room,
                              con.rel_regime,
                              h.id AS hotel_id, 
                              h.name AS hotel_name,
                              q.id AS quota_id,
                              q.name AS quota_name,
                              to_char(q.date_start, 'YYYY-MM-DD') AS quota_date_start,
                              to_char(q.date_end, 'YYYY-MM-DD') AS quota_date_end,
                              COALESCE(jsonb_agg(DISTINCT r.* ORDER BY r.*) FILTER (WHERE r.id IS NOT NULL), '[]') AS regime,
                              COALESCE(jsonb_agg(DISTINCT ro.* ORDER BY ro.*) FILTER (WHERE ro.id IS NOT NULL), '[]') AS room,
                              COALESCE(jsonb_agg(DISTINCT cv.* ORDER BY cv.*) FILTER (WHERE cv.id IS NOT NULL), '[]') AS contract_value,
                              COALESCE(jsonb_agg(DISTINCT cr.* ORDER BY cr.*) FILTER (WHERE cr.id IS NOT NULL), '[]') AS contract_range,
                              con.active
                         FROM data.contract AS con
                         LEFT JOIN data.hotel AS h ON h.id = con.rel_hotel
						 LEFT JOIN data.room AS ro ON ro.id = ANY(con.rel_room)
                         LEFT JOIN data.regime AS r ON r.id = ANY(con.rel_regime)
						 LEFT JOIN data.quota AS q ON q.id = con.rel_quota
                         LEFT JOIN data.contract_range AS cr ON cr.rel_contract = con.id
                         LEFT JOIN data.contract_value AS cv ON cv.rel_contract_range = cr.id
                        WHERE TRUE\n`
    let parameters = [];

    for (const filter in filters) {
        parameters.push(filters[filter]);
        queryString += 'AND ' + filter + '= $' + parameters.length + '\n';
    }
    if (limit) {
        parameters.push(limit, page * limit)
        queryString += 'LIMIT $' + (parameters.length - 1) + 'OFFSET $' + parameters.length + '\n';
    }
    let conn = await db.getConnection();
    let paginatorRes = await conn.query(paginatorQueryString, []);
    let totalItems = paginatorRes.rows[0].count;
    let totalPages = Math.ceil(totalItems / limit);
    queryString += 'GROUP BY con.id, h.id, q.id';
    let res = await conn.query(queryString, parameters);
    await conn.end();
    return {
        data: res.rows,
        paginator: {
            pageNumber: page + 1,
            totalPages: totalPages,
            totalItems: totalItems,
            currentItems: res.rows.length
        }
    };
};

const insertContract = async (hotelId, data) => {
    cleanForm(data);
    let queryString = `INSERT INTO data.contract(name, 
                                                rel_hotel,
                                                rel_room,
                                                rel_quota,
                                                rel_regime,
                                                active)
                                         VALUES ($1, $2, $3, $4, $5, $6)
                                      RETURNING id`;
    let parameters = [
        data.name,
        hotelId,
        data.room,
        data.quota,
        data.regime,
        data.active
    ]
    let conn = await db.getConnection();
    let res = await conn.query(queryString, parameters);
    await conn.end();
    return res.rows[0].id
}

const updateContract = async (contractId, data) => {
    cleanForm(data);
    let queryString = `UPDATE data.contract
                          SET name=$2,
                              rel_room=$3,
                              rel_regime=$4,
                              active=$5
                        WHERE id = $1
                    RETURNING id`
    let parameters = [
        contractId,
        data.name,
        data.room,
        data.regime,
        data.active
    ]
    let conn = await db.getConnection();
    let res = await conn.query(queryString, parameters);
    await conn.end();
    return res.rows[0].id
}

const deleteContract = async contractId => {
    let queryString = `DELETE FROM data.contract
                             WHERE id=$1`;
    let conn = await db.getConnection();
    await conn.query(queryString, [contractId]);
    await conn.end();
}

const deleteContractRelatedRange = async contractId => {
    let queryString = `DELETE FROM data.contract_range
                             WHERE rel_contract = $1
                         RETURNING id`
    let conn = await db.getConnection();
    let res = await conn.query(queryString, [contractId]);
    await conn.end();
    return res.rows.map(el => {
        return el.id;
    })
}


const deleteContractHandler = async contractId => {
    await deleteContract(contractId);
    let rangeIdArray = await deleteContractRelatedRange(contractId);
    await contractRange.deleteRangeRelatedValue(rangeIdArray);
}



module.exports = {
    getContract: getContract,
    insertContract: insertContract,
    updateContract, updateContract,
    deleteContractHandler: deleteContractHandler
}
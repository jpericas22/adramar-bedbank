'use strict';
const db = require('../lib/db')
const quotaRange = require('./quotaRange');

const cleanForm = (formData) => {
    let checkboxList = ['active'];
    for (let key in formData) {
        if (formData.hasOwnProperty(key)) {
            if (checkboxList.includes(key)) {
                formData[key] = formData[key][0] === 'on';
            }
            if (formData[key] === '') {
                formData[key] = null;
            }
        }
    }
}

const getQuota = async (filters = {}, limit = 0, page = 0) => {
    let paginatorQueryString = `SELECT count(id) FROM data.quota`
    let queryString = `SELECT q.id, 
                              q.name, 
                              h.id as hotel_id, 
                              h.name as hotel_name, 
                              q.active, 
                              to_char(q.date_start, 'YYYY-MM-DD') AS date_start, 
                              to_char(q.date_end, 'YYYY-MM-DD') AS date_end,
                              COALESCE(jsonb_agg(DISTINCT qr.* ORDER BY qr.*) FILTER (WHERE qr.id IS NOT NULL), '[]') AS quota_range,
                              COALESCE(jsonb_agg(DISTINCT qv.* ORDER BY qv.*) FILTER (WHERE qv.id IS NOT NULL), '[]') AS quota_value
                         FROM data.quota AS q
                    LEFT JOIN data.hotel AS h ON h.id = q.rel_hotel
                    LEFT JOIN data.quota_range AS qr ON qr.rel_quota = q.id
                    LEFT JOIN data.quota_value AS qv ON qv.rel_quota_range = qr.id
                        WHERE TRUE\n`

    let parameters = [];
    for (const filter in filters) {
        parameters.push(filters[filter]);
        queryString += 'AND ' + filter + '= $' + parameters.length + '\n';
    }
    if (limit) {
        parameters.push(limit, page * limit)
        queryString += 'LIMIT $' + (parameters.length - 1) + 'OFFSET $' + parameters.length + '\n';
    }
    let conn = await db.getConnection();
    let paginatorRes = await conn.query(paginatorQueryString, []);
    let totalItems = paginatorRes.rows[0].count;
    let totalPages = Math.ceil(totalItems / limit);
    queryString += 'GROUP BY q.id, h.id'
    let res = await conn.query(queryString, parameters);
    await conn.end();
    return {
        data: res.rows,
        paginator: {
            pageNumber: page + 1,
            totalPages: totalPages,
            totalItems: totalItems,
            currentItems: res.rows.length
        }
    };
};

const insertQuota = async (hotelId, data) => {
    cleanForm(data);
    let queryString = `INSERT INTO data.quota(name, 
                                              rel_hotel, 
                                              active, 
                                              date_start, 
                                              date_end)
	                                   VALUES ($1, $2, $3, $4, $5)
                                    RETURNING id`;
    let parameters = [
        data.name,
        hotelId,
        data.active,
        data.start,
        data.end
    ]
    let conn = await db.getConnection();
    let res = await conn.query(queryString, parameters);
    await conn.end();
    return res.rows[0].id
}

const deleteQuota = async quotaId => {
    let queryString = `DELETE FROM data.quota
                             WHERE id=$1`;
    let conn = await db.getConnection();
    await conn.query(queryString, [quotaId]);
    await conn.end();
}

const deleteQuotaRelatedRange = async quotaId => {
    let queryString = `DELETE FROM data.quota_range
                             WHERE rel_quota = $1
                         RETURNING id`
    let conn = await db.getConnection();
    let res = await conn.query(queryString, [quotaId]);
    await conn.end();
    return res.rows.map(el => {
        return el.id;
    })
}

const deleteQuotaHandler = async quotaId => {
    await deleteQuota(quotaId);
    let rangeIdArray = await deleteQuotaRelatedRange(quotaId);
    await quotaRange.deleteRangeRelatedValue(rangeIdArray);
}

const updateQuota = async (quotaId, data) => {
    cleanForm(data);
    let queryString = `UPDATE data.quota
                          SET name=$2,
                              active=$3,
                              date_start=$4,
                              date_end=$5
                        WHERE id = $1
                    RETURNING id`
    let parameters = [
        quotaId,
        data.name,
        data.active,
        data.start,
        data.end
    ]
    let conn = await db.getConnection();
    let res = await conn.query(queryString, parameters);
    await conn.end();
    return res.rows[0].id
}

module.exports = {
    getQuota: getQuota,
    insertQuota: insertQuota,
    updateQuota: updateQuota,
    deleteQuotaHandler: deleteQuotaHandler
}
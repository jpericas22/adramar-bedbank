'use strict';
const db = require('../lib/db');


const getChain = async (con = null, json = false) => {
    let closeCon = false;
    let queryString = `SELECT id, 
                              name
	                     FROM data.chain`;
    if (!con) {
        closeCon = true;
        con = await db.getConnection();
    }
    let res = await con.query(queryString, []);
    if (closeCon) {
        await con.end();
    }
    if (json) {
        return {
            chain: res.rows
        }
    }
    return res.rows;
}

const insertChain = async (data, con = null) => {
    let closeCon = false;
    let queryString = `INSERT INTO data.chain(name)
                            VALUES ($1)
                         RETURNING id`;
    let parameters = [
        data.name
    ]
    if (!con) {
        closeCon = true;
        con = await db.getConnection();
    }
    let res = await con.query(queryString, parameters);
    if (closeCon) {
        await con.end();
    }
    return res.rows[0].id;
}

const updateChain = async (chainId, data, con = null) => {
    let closeCon = false;
    let queryString = `UPDATE data.chain
                          SET name=$2
                        WHERE id=$1
                    RETURNING id`;
    let parameters = [
        chainId,
        data.name
    ]
    if (!con) {
        closeCon = true;
        con = await db.getConnection();
    }
    let res = await con.query(queryString, parameters);
    if (closeCon) {
        await con.end();
    }
    return res.rows[0].id;
}

const deleteChain = async (chainId, con = null) => {
    let closeCon = false;
    let queryString = `DELETE FROM data.chain
                             WHERE id=$1
                         RETURNING id`;
    let parameters = [
        chainId
    ]
    if (!con) {
        closeCon = true;
        con = await db.getConnection();
    }
    let res = await con.query(queryString, parameters);
    if (closeCon) {
        await con.end();
    }
    return res.rows[0].id;
}

module.exports = {
    getChain: getChain,
    insertChain: insertChain,
    updateChain: updateChain,
    deleteChain: deleteChain
}
'use strict';
const db = require('../lib/db')

const cleanForm = (formData) => {
    let checkboxList = ['active'];
    for (let key in formData) {
        if (formData.hasOwnProperty(key)) {
            if (checkboxList.includes(key)) {
                formData[key] = formData[key][0] === 'on';
            }
            if (formData[key] === '') {
                formData[key] = null;
            }
        }
    }
}

const getSale = async (filters = {}, limit = 0, page = 0) => {
    let paginatorQueryString = `SELECT count(id) FROM data.sale`
    let queryString = `SELECT s.id,
                              s.name,
							  con.id AS contract_id,
							  con.name AS contract_name,
                              h.id AS hotel_id, 
                              h.name AS hotel_name,
                              COALESCE(jsonb_agg(DISTINCT r.* ORDER BY r.*) FILTER (WHERE r.id IS NOT NULL), '[]') AS regime,
                              COALESCE(jsonb_agg(DISTINCT ro.* ORDER BY ro.*) FILTER (WHERE ro.id IS NOT NULL), '[]') AS room,
                              COALESCE(jsonb_agg(DISTINCT cv.* ORDER BY cv.*) FILTER (WHERE cv.id IS NOT NULL), '[]') AS contract_value,
                              COALESCE(jsonb_agg(DISTINCT cr.* ORDER BY cr.*) FILTER (WHERE cr.id IS NOT NULL), '[]') AS sale_range,
                              s.base_markup,
                              s.active
                         FROM data.sale AS s
                         LEFT JOIN data.hotel AS h ON h.id = s.rel_hotel
						 LEFT JOIN data.contract AS con ON con.id = s.rel_contract
						 LEFT JOIN data.room AS ro ON ro.id = ANY(con.rel_room)
						 LEFT JOIN data.contract_range AS cr ON cr.rel_contract = con.id
                         LEFT JOIN data.contract_value AS cv ON cv.rel_contract_range = cr.id
                         LEFT JOIN data.regime AS r ON r.id = ANY(cr.rel_regime)
                        WHERE TRUE\n`

    let parameters = [];

    for (const filter in filters) {
        parameters.push(filters[filter]);
        queryString += 'AND ' + filter + '= $' + parameters.length + '\n';
    }
    if (limit) {
        parameters.push(limit, page * limit)
        queryString += 'LIMIT $' + (parameters.length - 1) + 'OFFSET $' + parameters.length + '\n';
    }
    let conn = await db.getConnection();
    let paginatorRes = await conn.query(paginatorQueryString, []);
    let totalItems = paginatorRes.rows[0].count;
    let totalPages = Math.ceil(totalItems / limit);
    queryString += 'GROUP BY s.id, con.id, h.id'
    let res = await conn.query(queryString, parameters);
    await conn.end();
    return {
        data: res.rows,
        paginator: {
            pageNumber: page + 1,
            totalPages: totalPages,
            totalItems: totalItems,
            currentItems: res.rows.length
        }
    };
};

const insertSale = async (hotelId, data) => {
    cleanForm(data);
    let queryString = `INSERT INTO data.sale(rel_hotel, 
                                             rel_contract,
                                             name,
                                             base_markup,
                                             active)
                                      VALUES ($1, $2, $3, $4, $5)
                                   RETURNING id`;
    let parameters = [
        hotelId,
        data.contract,
        data.name,
        data.base_markup,
        data.active
    ]
    let conn = await db.getConnection();
    let res = await conn.query(queryString, parameters);
    await insertSaleBaseMarkupHandler(data.contract, data.base_markup, conn);
    await conn.end();
    return res.rows[0].id
}

const insertSaleBaseMarkupHandler = async (contractId, baseMarkup, conn=null) => {
    if(!conn){
        conn = await db.getConnection();
    }

    let getCostQueryString = `SELECT cr.id AS contract_range_id,
                                     cv.id AS contract_range_value_id,
                                     cv.value AS contract_range_value_cost
                                FROM data.contract_range AS cr
                                JOIN data.contract_value AS cv ON cv.rel_contract_range = cr.id
                               WHERE rel_contract=$1`
    let res = await conn.query(getCostQueryString, [contractId]);
    let updateSaleQueryValues = res.rows.reduce((prev, curr, i) => {
        if (i > 0) {
            prev += ', '
        }
        prev += '(' + curr.contract_range_id + ', ' + (curr.contract_range_value_cost + 1000) + ')'
        return prev;
    }, '')
    let updateSaleQueryString = `UPDATE data.contract_value AS cv SET
                                        id = c.id,
                                        sale_value = c.sale_value
                                   FROM (VALUES
                                        ` + updateSaleQueryValues + `
                                        ) as c(id, sale_value) 
                                  WHERE c.id = cv.id`
    await conn.query(updateSaleQueryString, []);
}

const updateSale = async (saleId, data) => {
    cleanForm(data);
    let queryString = `UPDATE data.sale
                          SET name=$2,
                              active=$3
                        WHERE id=$1
                    RETURNING id`
    let parameters = [
        saleId,
        data.name,
        data.active
    ]
    let conn = await db.getConnection();
    let res = await conn.query(queryString, parameters);
    await conn.end();
    return res.rows[0].id
}

const deleteSale = async saleId => {
    let queryString = `DELETE FROM data.sale
                             WHERE id=$1`;
    let conn = await db.getConnection();
    await conn.query(queryString, [saleId]);
    await conn.end();
}

const insertPrice = async (saleId, priceArray) => {
    let queryString = `INSERT INTO data.price(rel_sale, 
                                              rel_room, 
                                              rel_regime, 
                                              value)
                            VALUES `;
    let parameters = [];
    priceArray.forEach(el => {
        queryString += '($' + (parameters.length + 1) + ', $' + (parameters.length + 2) + ', $' + (parameters.length + 3) + ', $' + (parameters.length + 4) + '),\n'
        parameters.push(saleId, el.roomId, el.regimeId, el.value);
    })
    queryString = queryString.slice(0, -2);
    let conn = await db.getConnection();
    let res = await conn.query(queryString, parameters);
    await conn.end();
    return res.rows;
}

const deleteRelatedPrice = async saleId => {
    let queryString = `DELETE FROM data.price
                             WHERE rel_sale = $1`
    let conn = await db.getConnection();
    await conn.query(queryString, [saleId]);
    await conn.end();
}


module.exports = {
    getSale: getSale,
    insertSale: insertSale,
    updateSale: updateSale,
    deleteSale: deleteSale
}
#!/bin/bash
echo "started adramar backend service"
if [ "$ENV" = "PROD" ]; then
    nginx
    npm run start --prefix /volume/src
else
    npm --prefix /volume/src install
    nginx
    npm run debug --prefix /volume/src
fi
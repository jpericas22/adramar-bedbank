#!/bin/bash
set -e

#psql "dbname=postgres user=$POSTGRES_USER password=$POSTGRES_PASSWORD" <<-EOSQL
#    CREATE DATABASE adramar;
#    CREATE DATABASE returntransfer;
#EOSQL

psql "dbname=adramar user=$POSTGRES_USER password=$POSTGRES_PASSWORD" -f /db-init-data/adramar.sql
#psql "dbname=returntransfer user=$POSTGRES_USER password=$POSTGRES_PASSWORD" -f /db-init-data/returntransfer.sql
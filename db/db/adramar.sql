--
-- PostgreSQL database dump
--

-- Dumped from database version 12.4
-- Dumped by pg_dump version 12.6

-- Started on 2021-03-17 12:06:34 CET

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 7 (class 2615 OID 18355)
-- Name: access; Type: SCHEMA; Schema: -; Owner: doadmin
--

CREATE SCHEMA access;


ALTER SCHEMA access OWNER TO doadmin;

--
-- TOC entry 10 (class 2615 OID 18356)
-- Name: crossreference; Type: SCHEMA; Schema: -; Owner: doadmin
--

CREATE SCHEMA crossreference;


ALTER SCHEMA crossreference OWNER TO doadmin;

--
-- TOC entry 9 (class 2615 OID 17602)
-- Name: data; Type: SCHEMA; Schema: -; Owner: doadmin
--

CREATE SCHEMA data;


ALTER SCHEMA data OWNER TO doadmin;

--
-- TOC entry 596 (class 1247 OID 18359)
-- Name: child; Type: TYPE; Schema: data; Owner: doadmin
--

CREATE TYPE data.child AS (
	name character varying(256),
	age integer
);


ALTER TYPE data.child OWNER TO doadmin;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 208 (class 1259 OID 18360)
-- Name: user; Type: TABLE; Schema: access; Owner: doadmin
--

CREATE TABLE access."user" (
    id integer NOT NULL,
    username character varying(64) NOT NULL,
    password character varying(64) NOT NULL,
    salt character varying(64) NOT NULL,
    email character varying(256) NOT NULL,
    active boolean NOT NULL
);


ALTER TABLE access."user" OWNER TO doadmin;

--
-- TOC entry 209 (class 1259 OID 18363)
-- Name: user_id_seq; Type: SEQUENCE; Schema: access; Owner: doadmin
--

CREATE SEQUENCE access.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE access.user_id_seq OWNER TO doadmin;

--
-- TOC entry 4239 (class 0 OID 0)
-- Dependencies: 209
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: access; Owner: doadmin
--

ALTER SEQUENCE access.user_id_seq OWNED BY access."user".id;


--
-- TOC entry 210 (class 1259 OID 18365)
-- Name: webbeds_booking; Type: TABLE; Schema: crossreference; Owner: doadmin
--

CREATE TABLE crossreference.webbeds_booking (
    id character varying(32) NOT NULL,
    rel_booking integer NOT NULL
);


ALTER TABLE crossreference.webbeds_booking OWNER TO doadmin;

--
-- TOC entry 211 (class 1259 OID 18368)
-- Name: webbeds_hotel; Type: TABLE; Schema: crossreference; Owner: doadmin
--

CREATE TABLE crossreference.webbeds_hotel (
    id character varying(32) NOT NULL,
    rel_hotel integer NOT NULL
);


ALTER TABLE crossreference.webbeds_hotel OWNER TO doadmin;

--
-- TOC entry 212 (class 1259 OID 18371)
-- Name: webbeds_room; Type: TABLE; Schema: crossreference; Owner: doadmin
--

CREATE TABLE crossreference.webbeds_room (
    id character varying(32) NOT NULL,
    rel_room integer NOT NULL
);


ALTER TABLE crossreference.webbeds_room OWNER TO doadmin;

--
-- TOC entry 213 (class 1259 OID 18374)
-- Name: accomodation_category; Type: TABLE; Schema: data; Owner: doadmin
--

CREATE TABLE data.accomodation_category (
    id smallint NOT NULL,
    name character varying(32) NOT NULL
);


ALTER TABLE data.accomodation_category OWNER TO doadmin;

--
-- TOC entry 214 (class 1259 OID 18377)
-- Name: accomodation_type; Type: TABLE; Schema: data; Owner: doadmin
--

CREATE TABLE data.accomodation_type (
    id smallint NOT NULL,
    name character varying(32) NOT NULL
);


ALTER TABLE data.accomodation_type OWNER TO doadmin;

--
-- TOC entry 215 (class 1259 OID 18380)
-- Name: booking; Type: TABLE; Schema: data; Owner: doadmin
--

CREATE TABLE data.booking (
    id integer NOT NULL,
    rel_client integer NOT NULL,
    rel_hotel integer NOT NULL,
    rel_room integer NOT NULL,
    rel_regime integer NOT NULL,
    adults character varying[] NOT NULL,
    children data.child[] NOT NULL,
    infants integer NOT NULL,
    checkin_date date NOT NULL,
    checkout_date date NOT NULL,
    contact_name character varying(256),
    contact_phone character varying(128),
    contact_email character varying(256),
    observations text,
    status integer NOT NULL,
    booking_date timestamp without time zone NOT NULL,
    cancellation_date timestamp without time zone,
    no_show boolean NOT NULL
);


ALTER TABLE data.booking OWNER TO doadmin;

--
-- TOC entry 216 (class 1259 OID 18386)
-- Name: booking_id_seq1; Type: SEQUENCE; Schema: data; Owner: doadmin
--

CREATE SEQUENCE data.booking_id_seq1
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE data.booking_id_seq1 OWNER TO doadmin;

--
-- TOC entry 4240 (class 0 OID 0)
-- Dependencies: 216
-- Name: booking_id_seq1; Type: SEQUENCE OWNED BY; Schema: data; Owner: doadmin
--

ALTER SEQUENCE data.booking_id_seq1 OWNED BY data.booking.id;


--
-- TOC entry 217 (class 1259 OID 18388)
-- Name: category_id_seq; Type: SEQUENCE; Schema: data; Owner: doadmin
--

CREATE SEQUENCE data.category_id_seq
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE data.category_id_seq OWNER TO doadmin;

--
-- TOC entry 4241 (class 0 OID 0)
-- Dependencies: 217
-- Name: category_id_seq; Type: SEQUENCE OWNED BY; Schema: data; Owner: doadmin
--

ALTER SEQUENCE data.category_id_seq OWNED BY data.accomodation_category.id;


--
-- TOC entry 218 (class 1259 OID 18390)
-- Name: chain; Type: TABLE; Schema: data; Owner: doadmin
--

CREATE TABLE data.chain (
    id integer NOT NULL,
    name character varying(128) NOT NULL
);


ALTER TABLE data.chain OWNER TO doadmin;

--
-- TOC entry 219 (class 1259 OID 18393)
-- Name: chain_id_seq; Type: SEQUENCE; Schema: data; Owner: doadmin
--

CREATE SEQUENCE data.chain_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE data.chain_id_seq OWNER TO doadmin;

--
-- TOC entry 4242 (class 0 OID 0)
-- Dependencies: 219
-- Name: chain_id_seq; Type: SEQUENCE OWNED BY; Schema: data; Owner: doadmin
--

ALTER SEQUENCE data.chain_id_seq OWNED BY data.chain.id;


--
-- TOC entry 220 (class 1259 OID 18395)
-- Name: city; Type: TABLE; Schema: data; Owner: doadmin
--

CREATE TABLE data.city (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    rel_region integer NOT NULL
);


ALTER TABLE data.city OWNER TO doadmin;

--
-- TOC entry 221 (class 1259 OID 18398)
-- Name: city_id_seq; Type: SEQUENCE; Schema: data; Owner: doadmin
--

CREATE SEQUENCE data.city_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE data.city_id_seq OWNER TO doadmin;

--
-- TOC entry 4243 (class 0 OID 0)
-- Dependencies: 221
-- Name: city_id_seq; Type: SEQUENCE OWNED BY; Schema: data; Owner: doadmin
--

ALTER SEQUENCE data.city_id_seq OWNED BY data.city.id;


--
-- TOC entry 222 (class 1259 OID 18400)
-- Name: client; Type: TABLE; Schema: data; Owner: doadmin
--

CREATE TABLE data.client (
    id integer NOT NULL,
    name character varying(128) NOT NULL
);


ALTER TABLE data.client OWNER TO doadmin;

--
-- TOC entry 223 (class 1259 OID 18403)
-- Name: client_id_seq; Type: SEQUENCE; Schema: data; Owner: doadmin
--

CREATE SEQUENCE data.client_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE data.client_id_seq OWNER TO doadmin;

--
-- TOC entry 4244 (class 0 OID 0)
-- Dependencies: 223
-- Name: client_id_seq; Type: SEQUENCE OWNED BY; Schema: data; Owner: doadmin
--

ALTER SEQUENCE data.client_id_seq OWNED BY data.client.id;


--
-- TOC entry 224 (class 1259 OID 18405)
-- Name: contract; Type: TABLE; Schema: data; Owner: doadmin
--

CREATE TABLE data.contract (
    id integer NOT NULL,
    name character varying(128) NOT NULL,
    rel_hotel integer NOT NULL,
    active boolean NOT NULL,
    rel_room integer[] NOT NULL,
    rel_quota integer NOT NULL
);


ALTER TABLE data.contract OWNER TO doadmin;

--
-- TOC entry 225 (class 1259 OID 18411)
-- Name: contract_id_seq; Type: SEQUENCE; Schema: data; Owner: doadmin
--

CREATE SEQUENCE data.contract_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE data.contract_id_seq OWNER TO doadmin;

--
-- TOC entry 4245 (class 0 OID 0)
-- Dependencies: 225
-- Name: contract_id_seq; Type: SEQUENCE OWNED BY; Schema: data; Owner: doadmin
--

ALTER SEQUENCE data.contract_id_seq OWNED BY data.contract.id;


--
-- TOC entry 226 (class 1259 OID 18413)
-- Name: contract_range; Type: TABLE; Schema: data; Owner: doadmin
--

CREATE TABLE data.contract_range (
    id integer NOT NULL,
    rel_contract integer NOT NULL,
    rel_regime integer[] NOT NULL,
    date_start date,
    date_end date
);


ALTER TABLE data.contract_range OWNER TO doadmin;

--
-- TOC entry 227 (class 1259 OID 18419)
-- Name: contract_range_id_seq; Type: SEQUENCE; Schema: data; Owner: doadmin
--

CREATE SEQUENCE data.contract_range_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE data.contract_range_id_seq OWNER TO doadmin;

--
-- TOC entry 4246 (class 0 OID 0)
-- Dependencies: 227
-- Name: contract_range_id_seq; Type: SEQUENCE OWNED BY; Schema: data; Owner: doadmin
--

ALTER SEQUENCE data.contract_range_id_seq OWNED BY data.contract_range.id;


--
-- TOC entry 228 (class 1259 OID 18421)
-- Name: contract_value; Type: TABLE; Schema: data; Owner: doadmin
--

CREATE TABLE data.contract_value (
    id integer NOT NULL,
    rel_room integer NOT NULL,
    rel_regime integer NOT NULL,
    rel_contract_range integer NOT NULL,
    value numeric NOT NULL,
    sale_value numeric
);


ALTER TABLE data.contract_value OWNER TO doadmin;

--
-- TOC entry 229 (class 1259 OID 18427)
-- Name: cost_id_seq; Type: SEQUENCE; Schema: data; Owner: doadmin
--

CREATE SEQUENCE data.cost_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE data.cost_id_seq OWNER TO doadmin;

--
-- TOC entry 4247 (class 0 OID 0)
-- Dependencies: 229
-- Name: cost_id_seq; Type: SEQUENCE OWNED BY; Schema: data; Owner: doadmin
--

ALTER SEQUENCE data.cost_id_seq OWNED BY data.contract_value.id;


--
-- TOC entry 230 (class 1259 OID 18429)
-- Name: country; Type: TABLE; Schema: data; Owner: doadmin
--

CREATE TABLE data.country (
    id smallint NOT NULL,
    name character varying(64) NOT NULL
);


ALTER TABLE data.country OWNER TO doadmin;

--
-- TOC entry 231 (class 1259 OID 18432)
-- Name: country_id_seq; Type: SEQUENCE; Schema: data; Owner: doadmin
--

CREATE SEQUENCE data.country_id_seq
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE data.country_id_seq OWNER TO doadmin;

--
-- TOC entry 4248 (class 0 OID 0)
-- Dependencies: 231
-- Name: country_id_seq; Type: SEQUENCE OWNED BY; Schema: data; Owner: doadmin
--

ALTER SEQUENCE data.country_id_seq OWNED BY data.country.id;


--
-- TOC entry 205 (class 1259 OID 17668)
-- Name: extra_type; Type: TABLE; Schema: data; Owner: doadmin
--

CREATE TABLE data.extra_type (
    id smallint NOT NULL,
    name character varying(64) NOT NULL
);


ALTER TABLE data.extra_type OWNER TO doadmin;

--
-- TOC entry 206 (class 1259 OID 17671)
-- Name: extra_type_id_seq; Type: SEQUENCE; Schema: data; Owner: doadmin
--

CREATE SEQUENCE data.extra_type_id_seq
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE data.extra_type_id_seq OWNER TO doadmin;

--
-- TOC entry 4249 (class 0 OID 0)
-- Dependencies: 206
-- Name: extra_type_id_seq; Type: SEQUENCE OWNED BY; Schema: data; Owner: doadmin
--

ALTER SEQUENCE data.extra_type_id_seq OWNED BY data.extra_type.id;


--
-- TOC entry 232 (class 1259 OID 18434)
-- Name: extras; Type: TABLE; Schema: data; Owner: doadmin
--

CREATE TABLE data.extras (
    id smallint NOT NULL,
    name character varying(128) NOT NULL,
    description character varying(512)
);


ALTER TABLE data.extras OWNER TO doadmin;

--
-- TOC entry 233 (class 1259 OID 18440)
-- Name: extras_id_seq; Type: SEQUENCE; Schema: data; Owner: doadmin
--

CREATE SEQUENCE data.extras_id_seq
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE data.extras_id_seq OWNER TO doadmin;

--
-- TOC entry 4250 (class 0 OID 0)
-- Dependencies: 233
-- Name: extras_id_seq; Type: SEQUENCE OWNED BY; Schema: data; Owner: doadmin
--

ALTER SEQUENCE data.extras_id_seq OWNED BY data.extras.id;


--
-- TOC entry 234 (class 1259 OID 18442)
-- Name: hotel; Type: TABLE; Schema: data; Owner: doadmin
--

CREATE TABLE data.hotel (
    id integer NOT NULL,
    name character varying(128) NOT NULL,
    description character varying(256),
    address character varying(256) NOT NULL,
    postal_code character varying(5) NOT NULL,
    geolocation numeric[],
    email character varying(128),
    email_cc character varying(128),
    phone character varying(32),
    fax character varying(32),
    rel_provider integer NOT NULL,
    rel_city integer NOT NULL,
    rel_accomodation_category integer NOT NULL,
    rel_accomodation_type integer NOT NULL,
    rel_chain integer,
    rel_extras integer[],
    active boolean NOT NULL,
    email_booking character varying(128)
);


ALTER TABLE data.hotel OWNER TO doadmin;

--
-- TOC entry 235 (class 1259 OID 18448)
-- Name: hotel_id_seq1; Type: SEQUENCE; Schema: data; Owner: doadmin
--

CREATE SEQUENCE data.hotel_id_seq1
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE data.hotel_id_seq1 OWNER TO doadmin;

--
-- TOC entry 4251 (class 0 OID 0)
-- Dependencies: 235
-- Name: hotel_id_seq1; Type: SEQUENCE OWNED BY; Schema: data; Owner: doadmin
--

ALTER SEQUENCE data.hotel_id_seq1 OWNED BY data.hotel.id;


--
-- TOC entry 236 (class 1259 OID 18450)
-- Name: pax_range; Type: TABLE; Schema: data; Owner: doadmin
--

CREATE TABLE data.pax_range (
    id integer NOT NULL,
    rel_room_type integer NOT NULL,
    adult_min_pax integer NOT NULL,
    adult_max_pax integer NOT NULL,
    children_min_pax integer NOT NULL,
    children_max_pax integer NOT NULL
);


ALTER TABLE data.pax_range OWNER TO doadmin;

--
-- TOC entry 237 (class 1259 OID 18453)
-- Name: pax_range_id_seq; Type: SEQUENCE; Schema: data; Owner: doadmin
--

CREATE SEQUENCE data.pax_range_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE data.pax_range_id_seq OWNER TO doadmin;

--
-- TOC entry 4252 (class 0 OID 0)
-- Dependencies: 237
-- Name: pax_range_id_seq; Type: SEQUENCE OWNED BY; Schema: data; Owner: doadmin
--

ALTER SEQUENCE data.pax_range_id_seq OWNED BY data.pax_range.id;


--
-- TOC entry 238 (class 1259 OID 18455)
-- Name: provider; Type: TABLE; Schema: data; Owner: doadmin
--

CREATE TABLE data.provider (
    id integer NOT NULL,
    fiscal_name character varying(64) NOT NULL,
    fiscal_code character varying(32) NOT NULL,
    contact_name character varying(256),
    accounting_id character varying(9) NOT NULL,
    address character varying(512) NOT NULL,
    rel_city integer,
    active boolean NOT NULL,
    phone character varying(128),
    fax character varying(128)
);


ALTER TABLE data.provider OWNER TO doadmin;

--
-- TOC entry 239 (class 1259 OID 18461)
-- Name: provider_id_seq; Type: SEQUENCE; Schema: data; Owner: doadmin
--

CREATE SEQUENCE data.provider_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE data.provider_id_seq OWNER TO doadmin;

--
-- TOC entry 4253 (class 0 OID 0)
-- Dependencies: 239
-- Name: provider_id_seq; Type: SEQUENCE OWNED BY; Schema: data; Owner: doadmin
--

ALTER SEQUENCE data.provider_id_seq OWNED BY data.provider.id;


--
-- TOC entry 240 (class 1259 OID 18463)
-- Name: quota; Type: TABLE; Schema: data; Owner: doadmin
--

CREATE TABLE data.quota (
    id integer NOT NULL,
    name character varying(128) NOT NULL,
    rel_hotel integer NOT NULL,
    active boolean NOT NULL,
    date_start date NOT NULL,
    date_end date NOT NULL
);


ALTER TABLE data.quota OWNER TO doadmin;

--
-- TOC entry 241 (class 1259 OID 18466)
-- Name: quota_id_seq; Type: SEQUENCE; Schema: data; Owner: doadmin
--

CREATE SEQUENCE data.quota_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE data.quota_id_seq OWNER TO doadmin;

--
-- TOC entry 4254 (class 0 OID 0)
-- Dependencies: 241
-- Name: quota_id_seq; Type: SEQUENCE OWNED BY; Schema: data; Owner: doadmin
--

ALTER SEQUENCE data.quota_id_seq OWNED BY data.quota.id;


--
-- TOC entry 242 (class 1259 OID 18468)
-- Name: quota_range; Type: TABLE; Schema: data; Owner: doadmin
--

CREATE TABLE data.quota_range (
    id integer NOT NULL,
    rel_quota integer NOT NULL,
    date_start date NOT NULL,
    date_end date NOT NULL,
    priority smallint
);


ALTER TABLE data.quota_range OWNER TO doadmin;

--
-- TOC entry 243 (class 1259 OID 18471)
-- Name: quota_range_id_seq; Type: SEQUENCE; Schema: data; Owner: doadmin
--

CREATE SEQUENCE data.quota_range_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE data.quota_range_id_seq OWNER TO doadmin;

--
-- TOC entry 4255 (class 0 OID 0)
-- Dependencies: 243
-- Name: quota_range_id_seq; Type: SEQUENCE OWNED BY; Schema: data; Owner: doadmin
--

ALTER SEQUENCE data.quota_range_id_seq OWNED BY data.quota_range.id;


--
-- TOC entry 244 (class 1259 OID 18473)
-- Name: quota_value; Type: TABLE; Schema: data; Owner: doadmin
--

CREATE TABLE data.quota_value (
    id integer NOT NULL,
    rel_quota_range integer NOT NULL,
    rel_room integer NOT NULL,
    value integer,
    rel_room_shared_quota integer,
    free_sale boolean NOT NULL,
    on_request boolean NOT NULL
);


ALTER TABLE data.quota_value OWNER TO doadmin;

--
-- TOC entry 245 (class 1259 OID 18476)
-- Name: quota_value_id_seq; Type: SEQUENCE; Schema: data; Owner: doadmin
--

CREATE SEQUENCE data.quota_value_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE data.quota_value_id_seq OWNER TO doadmin;

--
-- TOC entry 4256 (class 0 OID 0)
-- Dependencies: 245
-- Name: quota_value_id_seq; Type: SEQUENCE OWNED BY; Schema: data; Owner: doadmin
--

ALTER SEQUENCE data.quota_value_id_seq OWNED BY data.quota_value.id;


--
-- TOC entry 246 (class 1259 OID 18478)
-- Name: regime; Type: TABLE; Schema: data; Owner: doadmin
--

CREATE TABLE data.regime (
    id integer NOT NULL,
    name character varying(32) NOT NULL,
    description text,
    rel_regime_type integer NOT NULL,
    code character varying(8) NOT NULL,
    rel_regime_service_first integer,
    rel_regime_service_last integer,
    active boolean
);


ALTER TABLE data.regime OWNER TO doadmin;

--
-- TOC entry 247 (class 1259 OID 18484)
-- Name: regime_contract_setting; Type: TABLE; Schema: data; Owner: doadmin
--

CREATE TABLE data.regime_contract_setting (
    id integer NOT NULL,
    rel_contract integer NOT NULL,
    rel_regime integer NOT NULL,
    base boolean NOT NULL,
    modifier numeric
);


ALTER TABLE data.regime_contract_setting OWNER TO doadmin;

--
-- TOC entry 248 (class 1259 OID 18490)
-- Name: regime_contract_setting_id_seq; Type: SEQUENCE; Schema: data; Owner: doadmin
--

CREATE SEQUENCE data.regime_contract_setting_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE data.regime_contract_setting_id_seq OWNER TO doadmin;

--
-- TOC entry 4257 (class 0 OID 0)
-- Dependencies: 248
-- Name: regime_contract_setting_id_seq; Type: SEQUENCE OWNED BY; Schema: data; Owner: doadmin
--

ALTER SEQUENCE data.regime_contract_setting_id_seq OWNED BY data.regime_contract_setting.id;


--
-- TOC entry 249 (class 1259 OID 18492)
-- Name: regime_service; Type: TABLE; Schema: data; Owner: doadmin
--

CREATE TABLE data.regime_service (
    id smallint NOT NULL,
    name character varying(32) NOT NULL
);


ALTER TABLE data.regime_service OWNER TO doadmin;

--
-- TOC entry 250 (class 1259 OID 18495)
-- Name: regime_service_id_seq; Type: SEQUENCE; Schema: data; Owner: doadmin
--

CREATE SEQUENCE data.regime_service_id_seq
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE data.regime_service_id_seq OWNER TO doadmin;

--
-- TOC entry 4258 (class 0 OID 0)
-- Dependencies: 250
-- Name: regime_service_id_seq; Type: SEQUENCE OWNED BY; Schema: data; Owner: doadmin
--

ALTER SEQUENCE data.regime_service_id_seq OWNED BY data.regime_service.id;


--
-- TOC entry 251 (class 1259 OID 18497)
-- Name: regime_type; Type: TABLE; Schema: data; Owner: doadmin
--

CREATE TABLE data.regime_type (
    id integer NOT NULL,
    name character varying(64) NOT NULL
);


ALTER TABLE data.regime_type OWNER TO doadmin;

--
-- TOC entry 252 (class 1259 OID 18500)
-- Name: regime_type_id_seq; Type: SEQUENCE; Schema: data; Owner: doadmin
--

CREATE SEQUENCE data.regime_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE data.regime_type_id_seq OWNER TO doadmin;

--
-- TOC entry 4259 (class 0 OID 0)
-- Dependencies: 252
-- Name: regime_type_id_seq; Type: SEQUENCE OWNED BY; Schema: data; Owner: doadmin
--

ALTER SEQUENCE data.regime_type_id_seq OWNED BY data.regime.id;


--
-- TOC entry 253 (class 1259 OID 18502)
-- Name: regime_type_id_seq1; Type: SEQUENCE; Schema: data; Owner: doadmin
--

CREATE SEQUENCE data.regime_type_id_seq1
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE data.regime_type_id_seq1 OWNER TO doadmin;

--
-- TOC entry 4260 (class 0 OID 0)
-- Dependencies: 253
-- Name: regime_type_id_seq1; Type: SEQUENCE OWNED BY; Schema: data; Owner: doadmin
--

ALTER SEQUENCE data.regime_type_id_seq1 OWNED BY data.regime_type.id;


--
-- TOC entry 254 (class 1259 OID 18504)
-- Name: region; Type: TABLE; Schema: data; Owner: doadmin
--

CREATE TABLE data.region (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    rel_country integer NOT NULL
);


ALTER TABLE data.region OWNER TO doadmin;

--
-- TOC entry 255 (class 1259 OID 18507)
-- Name: region_id_seq; Type: SEQUENCE; Schema: data; Owner: doadmin
--

CREATE SEQUENCE data.region_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE data.region_id_seq OWNER TO doadmin;

--
-- TOC entry 4261 (class 0 OID 0)
-- Dependencies: 255
-- Name: region_id_seq; Type: SEQUENCE OWNED BY; Schema: data; Owner: doadmin
--

ALTER SEQUENCE data.region_id_seq OWNED BY data.region.id;


--
-- TOC entry 256 (class 1259 OID 18509)
-- Name: room; Type: TABLE; Schema: data; Owner: doadmin
--

CREATE TABLE data.room (
    id integer NOT NULL,
    rel_hotel integer NOT NULL,
    name character varying(64) NOT NULL,
    description text,
    rel_room_type integer NOT NULL,
    kids_as_adult boolean NOT NULL,
    infants_as_adult boolean NOT NULL
);


ALTER TABLE data.room OWNER TO doadmin;

--
-- TOC entry 257 (class 1259 OID 18515)
-- Name: room_id_seq; Type: SEQUENCE; Schema: data; Owner: doadmin
--

CREATE SEQUENCE data.room_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE data.room_id_seq OWNER TO doadmin;

--
-- TOC entry 4262 (class 0 OID 0)
-- Dependencies: 257
-- Name: room_id_seq; Type: SEQUENCE OWNED BY; Schema: data; Owner: doadmin
--

ALTER SEQUENCE data.room_id_seq OWNED BY data.room.id;


--
-- TOC entry 258 (class 1259 OID 18517)
-- Name: room_type; Type: TABLE; Schema: data; Owner: doadmin
--

CREATE TABLE data.room_type (
    id smallint NOT NULL,
    name character varying(32) NOT NULL,
    description text,
    min_pax integer NOT NULL,
    max_pax integer NOT NULL,
    active boolean NOT NULL
);


ALTER TABLE data.room_type OWNER TO doadmin;

--
-- TOC entry 259 (class 1259 OID 18523)
-- Name: room_type_id_seq; Type: SEQUENCE; Schema: data; Owner: doadmin
--

CREATE SEQUENCE data.room_type_id_seq
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE data.room_type_id_seq OWNER TO doadmin;

--
-- TOC entry 4263 (class 0 OID 0)
-- Dependencies: 259
-- Name: room_type_id_seq; Type: SEQUENCE OWNED BY; Schema: data; Owner: doadmin
--

ALTER SEQUENCE data.room_type_id_seq OWNED BY data.room_type.id;


--
-- TOC entry 260 (class 1259 OID 18525)
-- Name: sale; Type: TABLE; Schema: data; Owner: doadmin
--

CREATE TABLE data.sale (
    id integer NOT NULL,
    name character varying(128) NOT NULL,
    rel_contract integer NOT NULL,
    active boolean NOT NULL,
    rel_hotel integer NOT NULL
);


ALTER TABLE data.sale OWNER TO doadmin;

--
-- TOC entry 261 (class 1259 OID 18528)
-- Name: sale_id_seq; Type: SEQUENCE; Schema: data; Owner: doadmin
--

CREATE SEQUENCE data.sale_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE data.sale_id_seq OWNER TO doadmin;

--
-- TOC entry 4264 (class 0 OID 0)
-- Dependencies: 261
-- Name: sale_id_seq; Type: SEQUENCE OWNED BY; Schema: data; Owner: doadmin
--

ALTER SEQUENCE data.sale_id_seq OWNED BY data.sale.id;


--
-- TOC entry 262 (class 1259 OID 18530)
-- Name: type_id_seq; Type: SEQUENCE; Schema: data; Owner: doadmin
--

CREATE SEQUENCE data.type_id_seq
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE data.type_id_seq OWNER TO doadmin;

--
-- TOC entry 4265 (class 0 OID 0)
-- Dependencies: 262
-- Name: type_id_seq; Type: SEQUENCE OWNED BY; Schema: data; Owner: doadmin
--

ALTER SEQUENCE data.type_id_seq OWNED BY data.accomodation_type.id;


--
-- TOC entry 3963 (class 2604 OID 18532)
-- Name: user id; Type: DEFAULT; Schema: access; Owner: doadmin
--

ALTER TABLE ONLY access."user" ALTER COLUMN id SET DEFAULT nextval('access.user_id_seq'::regclass);


--
-- TOC entry 3964 (class 2604 OID 18533)
-- Name: accomodation_category id; Type: DEFAULT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.accomodation_category ALTER COLUMN id SET DEFAULT nextval('data.category_id_seq'::regclass);


--
-- TOC entry 3965 (class 2604 OID 18534)
-- Name: accomodation_type id; Type: DEFAULT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.accomodation_type ALTER COLUMN id SET DEFAULT nextval('data.type_id_seq'::regclass);


--
-- TOC entry 3966 (class 2604 OID 18535)
-- Name: booking id; Type: DEFAULT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.booking ALTER COLUMN id SET DEFAULT nextval('data.booking_id_seq1'::regclass);


--
-- TOC entry 3967 (class 2604 OID 18536)
-- Name: chain id; Type: DEFAULT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.chain ALTER COLUMN id SET DEFAULT nextval('data.chain_id_seq'::regclass);


--
-- TOC entry 3968 (class 2604 OID 18537)
-- Name: city id; Type: DEFAULT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.city ALTER COLUMN id SET DEFAULT nextval('data.city_id_seq'::regclass);


--
-- TOC entry 3969 (class 2604 OID 18538)
-- Name: client id; Type: DEFAULT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.client ALTER COLUMN id SET DEFAULT nextval('data.client_id_seq'::regclass);


--
-- TOC entry 3970 (class 2604 OID 18539)
-- Name: contract id; Type: DEFAULT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.contract ALTER COLUMN id SET DEFAULT nextval('data.contract_id_seq'::regclass);


--
-- TOC entry 3971 (class 2604 OID 18540)
-- Name: contract_range id; Type: DEFAULT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.contract_range ALTER COLUMN id SET DEFAULT nextval('data.contract_range_id_seq'::regclass);


--
-- TOC entry 3972 (class 2604 OID 18541)
-- Name: contract_value id; Type: DEFAULT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.contract_value ALTER COLUMN id SET DEFAULT nextval('data.cost_id_seq'::regclass);


--
-- TOC entry 3973 (class 2604 OID 18542)
-- Name: country id; Type: DEFAULT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.country ALTER COLUMN id SET DEFAULT nextval('data.country_id_seq'::regclass);


--
-- TOC entry 3962 (class 2604 OID 17769)
-- Name: extra_type id; Type: DEFAULT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.extra_type ALTER COLUMN id SET DEFAULT nextval('data.extra_type_id_seq'::regclass);


--
-- TOC entry 3974 (class 2604 OID 18543)
-- Name: extras id; Type: DEFAULT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.extras ALTER COLUMN id SET DEFAULT nextval('data.extras_id_seq'::regclass);


--
-- TOC entry 3975 (class 2604 OID 18544)
-- Name: hotel id; Type: DEFAULT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.hotel ALTER COLUMN id SET DEFAULT nextval('data.hotel_id_seq1'::regclass);


--
-- TOC entry 3976 (class 2604 OID 18545)
-- Name: pax_range id; Type: DEFAULT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.pax_range ALTER COLUMN id SET DEFAULT nextval('data.pax_range_id_seq'::regclass);


--
-- TOC entry 3977 (class 2604 OID 18546)
-- Name: provider id; Type: DEFAULT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.provider ALTER COLUMN id SET DEFAULT nextval('data.provider_id_seq'::regclass);


--
-- TOC entry 3978 (class 2604 OID 18547)
-- Name: quota id; Type: DEFAULT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.quota ALTER COLUMN id SET DEFAULT nextval('data.quota_id_seq'::regclass);


--
-- TOC entry 3979 (class 2604 OID 18548)
-- Name: quota_range id; Type: DEFAULT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.quota_range ALTER COLUMN id SET DEFAULT nextval('data.quota_range_id_seq'::regclass);


--
-- TOC entry 3980 (class 2604 OID 18549)
-- Name: quota_value id; Type: DEFAULT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.quota_value ALTER COLUMN id SET DEFAULT nextval('data.quota_value_id_seq'::regclass);


--
-- TOC entry 3981 (class 2604 OID 18550)
-- Name: regime id; Type: DEFAULT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.regime ALTER COLUMN id SET DEFAULT nextval('data.regime_type_id_seq'::regclass);


--
-- TOC entry 3982 (class 2604 OID 18551)
-- Name: regime_contract_setting id; Type: DEFAULT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.regime_contract_setting ALTER COLUMN id SET DEFAULT nextval('data.regime_contract_setting_id_seq'::regclass);


--
-- TOC entry 3983 (class 2604 OID 18552)
-- Name: regime_service id; Type: DEFAULT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.regime_service ALTER COLUMN id SET DEFAULT nextval('data.regime_service_id_seq'::regclass);


--
-- TOC entry 3984 (class 2604 OID 18553)
-- Name: regime_type id; Type: DEFAULT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.regime_type ALTER COLUMN id SET DEFAULT nextval('data.regime_type_id_seq1'::regclass);


--
-- TOC entry 3985 (class 2604 OID 18554)
-- Name: region id; Type: DEFAULT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.region ALTER COLUMN id SET DEFAULT nextval('data.region_id_seq'::regclass);


--
-- TOC entry 3986 (class 2604 OID 18555)
-- Name: room id; Type: DEFAULT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.room ALTER COLUMN id SET DEFAULT nextval('data.room_id_seq'::regclass);


--
-- TOC entry 3987 (class 2604 OID 18556)
-- Name: room_type id; Type: DEFAULT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.room_type ALTER COLUMN id SET DEFAULT nextval('data.room_type_id_seq'::regclass);


--
-- TOC entry 3988 (class 2604 OID 18557)
-- Name: sale id; Type: DEFAULT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.sale ALTER COLUMN id SET DEFAULT nextval('data.sale_id_seq'::regclass);


--
-- TOC entry 4179 (class 0 OID 18360)
-- Dependencies: 208
-- Data for Name: user; Type: TABLE DATA; Schema: access; Owner: doadmin
--

INSERT INTO access."user" (id, username, password, salt, email, active) VALUES (1, 'test', '88cd2108b5347d973cf39cdf9053d7dd42704876d8c9a9bd8e2d168259d3ddf7', 'test', 'test', true);


--
-- TOC entry 4181 (class 0 OID 18365)
-- Dependencies: 210
-- Data for Name: webbeds_booking; Type: TABLE DATA; Schema: crossreference; Owner: doadmin
--

INSERT INTO crossreference.webbeds_booking (id, rel_booking) VALUES ('5324377', 456);
INSERT INTO crossreference.webbeds_booking (id, rel_booking) VALUES ('5332537', 457);
INSERT INTO crossreference.webbeds_booking (id, rel_booking) VALUES ('5318591', 458);
INSERT INTO crossreference.webbeds_booking (id, rel_booking) VALUES ('5335365', 459);


--
-- TOC entry 4182 (class 0 OID 18368)
-- Dependencies: 211
-- Data for Name: webbeds_hotel; Type: TABLE DATA; Schema: crossreference; Owner: doadmin
--



--
-- TOC entry 4183 (class 0 OID 18371)
-- Dependencies: 212
-- Data for Name: webbeds_room; Type: TABLE DATA; Schema: crossreference; Owner: doadmin
--



--
-- TOC entry 4184 (class 0 OID 18374)
-- Dependencies: 213
-- Data for Name: accomodation_category; Type: TABLE DATA; Schema: data; Owner: doadmin
--

INSERT INTO data.accomodation_category (id, name) VALUES (1, '1/2 STAR');
INSERT INTO data.accomodation_category (id, name) VALUES (2, '1 STAR');
INSERT INTO data.accomodation_category (id, name) VALUES (3, '2 STAR');
INSERT INTO data.accomodation_category (id, name) VALUES (4, '3 STAR');
INSERT INTO data.accomodation_category (id, name) VALUES (5, '3 PLUS STAR');
INSERT INTO data.accomodation_category (id, name) VALUES (6, '4 STAR');
INSERT INTO data.accomodation_category (id, name) VALUES (7, '5 STAR');
INSERT INTO data.accomodation_category (id, name) VALUES (8, '6 STAR');


--
-- TOC entry 4185 (class 0 OID 18377)
-- Dependencies: 214
-- Data for Name: accomodation_type; Type: TABLE DATA; Schema: data; Owner: doadmin
--

INSERT INTO data.accomodation_type (id, name) VALUES (1, 'APARTMENTS');
INSERT INTO data.accomodation_type (id, name) VALUES (2, 'RESORT');
INSERT INTO data.accomodation_type (id, name) VALUES (3, 'HOTEL');
INSERT INTO data.accomodation_type (id, name) VALUES (4, 'GENERAL');


--
-- TOC entry 4186 (class 0 OID 18380)
-- Dependencies: 215
-- Data for Name: booking; Type: TABLE DATA; Schema: data; Owner: doadmin
--



--
-- TOC entry 4189 (class 0 OID 18390)
-- Dependencies: 218
-- Data for Name: chain; Type: TABLE DATA; Schema: data; Owner: doadmin
--

INSERT INTO data.chain (id, name) VALUES (2, 'chaint2');


--
-- TOC entry 4191 (class 0 OID 18395)
-- Dependencies: 220
-- Data for Name: city; Type: TABLE DATA; Schema: data; Owner: doadmin
--

INSERT INTO data.city (id, name, rel_region) VALUES (35, 'Coves', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (36, 'Aeroport D''eivissa', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (37, 'Alaior', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (38, 'Alaro', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (39, 'Alcaufar', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (40, 'Alconasser', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (41, 'Alcudia', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (42, 'Algaida', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (43, 'Alqueries', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (44, 'Andratx', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (45, 'Aranjassa (s'')', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (46, 'Arenal (s'')', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (47, 'Argentina, La, Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (48, 'Ariany', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (49, 'Arraco (s'')', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (50, 'Arta', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (51, 'Auto Safari', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (52, 'Badia Blava', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (53, 'Badia Gran', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (54, 'Banyalbufar', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (55, 'Barca Trencada', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (56, 'Barcares (es)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (57, 'Base Aerea Son Sant Joan, Complejo', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (58, 'Base Aeria De PollenÇa', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (59, 'Bellavista', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (60, 'Bendinat', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (61, 'Benestar', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (62, 'Betlem', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (63, 'Biniagual', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (64, 'Biniali', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (65, 'Biniali (menorca)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (66, 'Biniamar', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (67, 'Biniaraix', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (68, 'Binibeca Nou (sant Lluis), Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (69, 'Binibeca, De, Platja', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (70, 'BinibÈquer', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (71, 'Binicalaf', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (72, 'Biniencolla (sant Lluis), Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (73, 'Binifadet', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (74, 'Biniparrell', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (75, 'Binisafuller Platja (sant Lluis), Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (76, 'Binisafuller Roters (sant Lluis), Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (77, 'Binissafuller', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (78, 'Binissalem', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (79, 'Binixica', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (80, 'Blanca Dona (eivissa)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (81, 'Bon Any', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (82, 'Buger', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (83, 'Bunyola', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (84, 'Cabaneta (sa)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (85, 'Cadenes (ses)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (86, 'Caimari', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (87, 'Cala Agulla', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (88, 'Cala Anguila - Cala Mendia', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (89, 'Cala Biniancolla', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (90, 'Cala Blanca (ciutadella De Menorca), Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (91, 'Cala Blanes', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (92, 'Cala Blava, Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (93, 'Cala Bona', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (94, 'Cala Bruch', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (95, 'Cala Carbo', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (96, 'Cala De San Vicente Ibiza', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (97, 'Cala D''or', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (98, 'Cala En Blanes', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (99, 'Cala En Forcat', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (100, 'Cala Ferrera', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (101, 'Cala Figuera (mallorca)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (102, 'Cala Fornells', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (103, 'Cala Galdana', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (104, 'Cala Gat', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (105, 'Cala Gracio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (106, 'Cala Llenya, Platja', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (107, 'Cala Lliteres', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (108, 'Cala Llombards', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (109, 'Cala Llonga, Platja', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (110, 'Cala Magrana', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (111, 'Cala Mandia', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (112, 'Cala Marsal', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (113, 'Cala Mastella', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (114, 'Cala Mesquida', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (115, 'Cala Millor', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (116, 'Cala Mitjana (mallorca)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (117, 'Cala Mondrago', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (118, 'Cala Murada', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (119, 'Cala Pada', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (120, 'Cala Pi', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (121, 'Cala Portinax, Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (122, 'Cala Provensals', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (123, 'Cala Ratjada', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (124, 'Cala Romantica', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (125, 'Cala S''almonia', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (126, 'Cala San Esteban', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (127, 'Cala Sant VicenÇ (pollenÇa)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (128, 'Cala Santanyi', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (129, 'Cala Serena', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (130, 'Cala Tropicana, Platja', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (131, 'Cala Tuent', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (132, 'Cala Vinyes', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (133, 'Cala''n Bosch', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (134, 'Cala''n Busquets', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (135, 'Cala''n Morell', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (136, 'Cala''n Porter', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (137, 'Calas Covas', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (138, 'Calas De Mallorca', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (139, 'Caleta D''en Gorries', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (140, 'Calo Den Busques', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (141, 'Calonge', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (142, 'CalviÀ', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (143, 'Cami Vell De Sant Mateu (eivissa)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (144, 'Camp De La Mar', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (145, 'Camp De Mar (es)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (146, 'Campanet', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (147, 'Campos', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (148, 'Can Bufi (eivissa)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (149, 'Can Cabrit (eivissa)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (150, 'Can Carbonell', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (151, 'Ca''n Cirer', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (152, 'Ca''n Clavo', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (153, 'Can Escandell (eivissa)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (154, 'Can Frigolas, Barriada', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (155, 'Can MarÇa, Barriada', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (156, 'Can Mari, Barriada', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (157, 'Can Pastilla', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (158, 'Ca''n Picafort', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (159, 'Can Sanso, Barriada', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (160, 'Cana Negreta, Barriada', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (161, 'Canteras Las', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (162, 'Canyamel', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (163, 'Cap D''artruix (ciutadella De Menorca)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (164, 'Cap De Barbaria', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (165, 'Cap D''en Font', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (166, 'Cap Des Moro', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (167, 'Cap Martinet', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (168, 'Cap Negret', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (169, 'Capdella, Es', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (170, 'Capdepera', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (171, 'Carritxo (es)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (172, 'Cas Boll', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (173, 'Cas Buc', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (174, 'Ca''s Cana', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (175, 'Cas Capita', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (176, 'Cas CatalÀ', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (177, 'Cas Concos Des Cavaller', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (178, 'Cas Corp', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (179, 'Cas Ferro (eivissa)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (180, 'Cas Miot', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (181, 'Can Negre (eivissa)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (182, 'Castellitx', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (183, 'Castillo De Bendinat', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (184, 'Caubet', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (185, 'Ciudad Jardin (eivissa)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (186, 'Ciutadella De Menorca', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (187, 'Colonia De Sant Jordi', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (188, 'Colonia De Sant Pere', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (189, 'Consell', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (190, 'Costa De La Calma', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (191, 'Costa De Los Pinos', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (192, 'Costa D''en Blanes', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (193, 'Costa, La (alqueria Blanca)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (194, 'Costa, La (santanyi)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (195, 'Costitx', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (196, 'Covas Novas (mercadal)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (197, 'Covetes (ses)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (198, 'Creus Des Magres (eivissa)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (199, 'DeiÀ', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (200, 'Delfines, Los, Urbanizacion', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (201, 'Eivissa', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (202, 'Eivissa-san Juan (carretera), Hasta Km.1,900', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (203, 'Eivissa-sant Antoni De Portmany (carretera), Hasta Km.3,900', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (204, 'Es Calo', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (205, 'Es Cana, Platja', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (206, 'Es Canutells/els Canutells', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (207, 'Es Carregador', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (208, 'Es Castell', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (209, 'Es Caulls, Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (210, 'Es Consell/el Consell (sant Lluis), Caserio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (211, 'Es Fangar', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (212, 'Es Figueral, Platja', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (213, 'Es Figueral (marratxi)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (214, 'Es Garrovers', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (215, 'Es Migjorn Gran/el Migjorn Gran', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (216, 'Es Pelats', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (217, 'Es Pinaret', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (218, 'Es Pitlari', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (219, 'Es Pla De Na Tesa/el Pla De Na Tesa', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (220, 'Es Pont D''inca/el Pont D''inca', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (221, 'Es Pou Nou', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (222, 'Es Vive (eivissa)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (223, 'Escorca', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (224, 'Espinagar', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (225, 'Esporles', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (226, 'Estellencs', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (227, 'Faro De La Mola', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (228, 'Felanitx', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (229, 'Ferreries', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (230, 'Font De Sa Cala', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (231, 'Fontanelles (ses)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (232, 'Formentor', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (233, 'Fornalutx', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (234, 'Fornells', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (235, 'Fortaleza Isabel Ii', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (236, 'Galatzo (santa PonÇa), Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (237, 'Galilea', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (238, 'Garriga, La', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (239, 'Grau (es), Platja', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (240, 'Horta, L'' (pollenÇa)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (241, 'Horta, S'' (felanitx)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (242, 'Illa Plana (eivissa)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (243, 'Illetes (ses)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (244, 'Inca', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (245, 'Jornets', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (246, 'Llimpia', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (247, 'Llombards', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (248, 'Lloret De Vistalegre', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (249, 'Lloseta', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (250, 'Llubi', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (251, 'Lluc', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (252, 'Llucalcari', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (253, 'LlucmaÇanes', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (254, 'Llucmajor', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (255, 'Magalluf', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (256, 'Maioris Decima, Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (257, 'Mal Pas - Bon Aire', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (258, 'Manacor', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (259, 'Mancor De La Vall', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (260, 'Mao (ver Callejero)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (261, 'Maravillas Las', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (262, 'Maria De La Salut', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (263, 'Marina De Son Ganxo', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (264, 'Marina-manresa', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (265, 'Marratxi', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (266, 'Marratxinet (marratxi)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (267, 'Mercadal, Es', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (268, 'Mesquida (sa), Platja', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (269, 'Miramar', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (270, 'Mitjorn, Platja', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (271, 'MontaÑas Verdes, Barriada', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (272, 'Monte Toro, Santuario', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (273, 'Montecristo (eivissa)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (274, 'Montuiri', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (275, 'Moscari', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (276, 'Muro', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (277, 'Murta, Es', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (278, 'Na Macaret, Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (279, 'Na Taconera', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (280, 'Niu Blau', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (281, 'Noria Riera', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (282, 'Nova Valldemosa', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (283, 'Nuestra SeÑora De Jesus', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (284, 'Nuestra SeÑora Del Pilar', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (285, 'Olleries', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (286, 'Orient', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (287, 'Palma De Mallorca (ver Callejero)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (288, 'Palmanova', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (289, 'Palmanyola', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (290, 'Palmer, El', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (291, 'Palmeres, Les', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (292, 'Parc Bit, Urbanizacion', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (293, 'Pedruscada', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (294, 'Peguera', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (295, 'Petra', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (296, 'Pilar De La Mola', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (297, 'Pina', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (298, 'Pla De Sant Jordi', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (299, 'Planera, Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (300, 'Platja De Alcudia, Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (301, 'Platja De Muro, Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (302, 'Playa De Fornells, Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (303, 'Pobla (sa)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (304, 'Poligon Industrial Son Rossinyol', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (305, 'Poligono Industrial De Marratxi', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (306, 'PollenÇa', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (307, 'Pont De S''argentera', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (308, 'Pont D''inca Nou', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (309, 'Pont D''inca Park', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (310, 'Porreres', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (311, 'Port D''addaia', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (312, 'Port D''alcudia', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (313, 'Port D''andratx', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (314, 'Port De PollenÇa', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (315, 'Port De Sa Calobra', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (316, 'Port De Sant Miquel, Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (317, 'Port De Soller', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (318, 'Port Des Canonge/port Del Canonge', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (319, 'Port Verd', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (320, 'Portals Nous', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (321, 'Portals Vells', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (322, 'Porto Cristo Novo', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (323, 'Portocolom', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (324, 'Portocristo/port De Manacor', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (325, 'PÒrtol', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (326, 'Portopetro', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (327, 'Prat De Jesus', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (328, 'Puig De Ros, Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (329, 'Puig Den Valls (eivissa)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (330, 'Puigpunyent', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (331, 'Pujols', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (332, 'Punta Grossa', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (333, 'Punta Negra', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (334, 'Punta Prima, Platja', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (335, 'Randa', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (336, 'Rapita (sa)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (337, 'Ribera, La', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (338, 'Roca Llisa, Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (339, 'Rotes Velles', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (340, 'Ruberts', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (341, 'Sa Cabana', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (342, 'Sa Calobra/la Calobra', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (343, 'Sa Coma (cala Millor)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (344, 'Sa Creu Vermella/la Creu Vermella', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (345, 'Sa Font Seca/la Font Seca', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (346, 'Sa Furana', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (347, 'Sa Garriga', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (348, 'Sa Porrassa/la Porrassa', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (349, 'Sa Roca', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (350, 'Sa Tanca De Cas Buc', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (351, 'Sa Tanqueta (eivissa)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (352, 'Sa Taulera', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (353, 'Sa Torre Nova', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (354, 'Sa Vinya De Son Llebre', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (355, 'S''algar', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (356, 'Salines (ses)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (357, 'Salines, Ses (mercadal-menorca), Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (358, 'S''alqueria Blanca/l''alqueria Blanca', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (359, 'San Antonio (mahon)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (360, 'San Antonio Abad', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (361, 'San Jaime Mediterraneo, Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (362, 'Sant Antoni De Portmany', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (363, 'Sant Carles (desde Km.7400 Hasta Final), Carretera', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (364, 'Sant Carles De Peralta', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (365, 'Sant Climent', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (366, 'Sant Elm', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (367, 'Sant Felip', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (368, 'Sant Ferran De Ses Roques', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (369, 'Sant Francesc De Formentera', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (370, 'Sant Joan', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (371, 'Sant Joan De Labritja', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (372, 'Sant Jordi', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (373, 'Sant Jordi De Ses Salines', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (374, 'Sant Josep De Sa Talaia (benimussa)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (375, 'Sant Josep De Sa Talaia (cala Bassa)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (376, 'Sant Josep De Sa Talaia (cala Bou)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (377, 'Sant Josep De Sa Talaia (cala Comte)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (378, 'Sant Josep De Sa Talaia (cala Corral)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (379, 'Sant Josep De Sa Talaia (cala Tarida)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (380, 'Sant Josep De Sa Talaia (cala Vedella)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (381, 'Sant Josep De Sa Talaia (can Bellotera)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (382, 'Sant Josep De Sa Talaia (can Cifre), Barriada', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (383, 'Sant Josep De Sa Talaia (can Fita), Barriada', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (384, 'Sant Josep De Sa Talaia (can Guerxo), Barriada', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (385, 'Sant Josep De Sa Talaia (can MaÑa)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (386, 'Sant Josep De Sa Talaia (can Noguera), Barriada', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (387, 'Sant Josep De Sa Talaia (la Caleta)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (388, 'Sant Josep De Sa Talaia (les Salines)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (389, 'Sant Josep De Sa Talaia (nucleo)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (390, 'Sant Josep De Sa Talaia (plana Den Fita), Barriada', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (391, 'Sant Josep De Sa Talaia (port Des Torrent)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (392, 'Sant Josep De Sa Talaia (sa Carroca)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (393, 'Sant Josep De Sa Talaia (sant Agusti Des VedrÀ), Nucleo', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (394, 'Sant Josep De Sa Talaia (sant Francesc De S''estany)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (395, 'Sant Josep De Sa Talaia (urbanitzacio Cala Carbo)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (396, 'Sant Josep De Sa Talaia (urbanitzacio Cala Codola)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (397, 'Sant Josep De Sa Talaia (urbanitzacio Cala D''hort)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (398, 'Sant Josep De Sa Talaia (urbanitzacio Cala Moli)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (399, 'Sant Josep De Sa Talaia (urbanitzacio Calo Den Reial)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (400, 'Sant Josep De Sa Talaia (urbanitzacio Club Delfin)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (401, 'Sant Josep De Sa Talaia (urbanitzacio El Paraiso)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (402, 'Sant Josep De Sa Talaia (urbanitzacio Es Cubells)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (403, 'Sant Josep De Sa Talaia (urbanitzacio Pinos Y Mar)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (404, 'Sant Josep De Sa Talaia (urbanitzacio Platja Den Bossa)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (405, 'Sant Josep De Sa Talaia (urbanitzacio Port Roig)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (406, 'Sant Josep De Sa Talaia (urbanitzacio Vista Alegre-sa Caixota)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (407, 'Sant Llatzet', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (408, 'Sant LlorenÇ De BalÀfia', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (409, 'Sant LlorenÇ Des Cardassar', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (410, 'Sant Lluis', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (412, 'Sant Mateu D''albarca', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (413, 'Sant Miquel De Balasant', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (414, 'Sant Rafel De Sa Creu/sant Rafel De La Creu', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (415, 'Sant TomÀs, Platja, Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (416, 'Sant Vicent De Sa Cala', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (417, 'Santa Agnes De Corona', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (418, 'Santa Eugenia', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (419, 'Santa Eularia Des Riu', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (420, 'Santa Gertrudis De Fruitera', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (421, 'Santa Madrona', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (422, 'Santa Margalida', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (423, 'Santa Maria Del Cami (isla De Mallorca)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (424, 'Santa PonÇa', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (425, 'Santandria, Platja', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (426, 'Santanyi', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (427, 'S''arenal D''en Castell (menorca), Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (428, 'S''argamassa, Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (429, 'Savina, La', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (430, 'Se Reconada, Barriada', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (431, 'Selva', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (432, 'Sencelles', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (433, 'Serra Morena', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (434, 'Serra Nova', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (435, 'Ses Barraques', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (436, 'Ses Cadenas', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (437, 'Ses Cases Noves', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (438, 'Ses Figueres (eivissa)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (439, 'Ses Rotgetes', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (440, 'S''esglaieta', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (441, 'S''estanyol', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (442, 'S''horta (soller)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (443, 'S''hostalot, Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (444, 'Siesta, La, Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (445, 'S''illot-cala Morlanda', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (446, 'Sineu', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (447, 'Sol De Mallorca', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (448, 'Sol Del Este, Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (449, 'Soller', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (450, 'Sometines', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (451, 'Son Amar', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (452, 'Son Amtller, Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (453, 'Son Arosa', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (454, 'Son Baulo', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (455, 'Son Blanc (ciutadella De Menorca)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (456, 'Son Bonet', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (457, 'Son Bou, Platja', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (458, 'Son Bugadelles, Poligono', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (459, 'Son Caldero', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (460, 'Son Caliu', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (461, 'Son Carrio (mallorca)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (462, 'Son Daviu, Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (463, 'Son Duri', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (464, 'Son Espanyol', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (465, 'Son Fangos', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (466, 'Son Fe', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (467, 'Son Ferrer, Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (468, 'Son Ferriol', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (469, 'Son Macia', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (470, 'Son MaciÀ', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (471, 'Son Matias', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (472, 'Son Maxella', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (473, 'Son Mayol', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (474, 'Son Mesquida', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (475, 'Son Moger', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (476, 'Son Nebot', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (477, 'Son Negre', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (478, 'Son Parc, Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (479, 'Son ProenÇ', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (480, 'Son Ramonell, Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (481, 'Son Real', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (482, 'Son Sardina', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (483, 'Son Serra De Marina', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (484, 'Son Servera', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (485, 'Son Sunyer (arenal)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (486, 'Son Sunyer (s'' Aranjassa)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (487, 'Son Termes', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (488, 'Son Trias', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (489, 'Son Valls', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (490, 'Son Veri Nou, Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (491, 'Son Veri Vell', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (492, 'Son Vilar (es Castell De Menorca)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (493, 'S''ullastrar', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (494, 'Talamanca (eivissa)', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (495, 'Tollerich', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (496, 'Toro, El', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (497, 'Torre Del Ram', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (498, 'Torre Soli Nou, Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (499, 'Torrenova', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (500, 'Torret, Caserio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (501, 'Trebeluger', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (502, 'Trepuco', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (503, 'Uestra, S'' (sant Lluis), Urbanitzacio', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (504, 'Ullaro (campanet), Lloc', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (505, 'Ullastrar, L''', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (506, 'Universitat Illes Balears', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (507, 'Urbanizacion Puntiro', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (508, 'Urbanizacion Sa Torre', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (509, 'Valldemossa', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (510, 'Vallgornera', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (511, 'Vergers De Sant Joan', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (512, 'Vilafranca De Bonany', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (513, 'Palma De Mallorca', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (514, 'Maó', 1);
INSERT INTO data.city (id, name, rel_region) VALUES (411, 'Sant Marçal', 1);


--
-- TOC entry 4193 (class 0 OID 18400)
-- Dependencies: 222
-- Data for Name: client; Type: TABLE DATA; Schema: data; Owner: doadmin
--

INSERT INTO data.client (id, name) VALUES (1, 'WEBBEDS');


--
-- TOC entry 4195 (class 0 OID 18405)
-- Dependencies: 224
-- Data for Name: contract; Type: TABLE DATA; Schema: data; Owner: doadmin
--



--
-- TOC entry 4197 (class 0 OID 18413)
-- Dependencies: 226
-- Data for Name: contract_range; Type: TABLE DATA; Schema: data; Owner: doadmin
--



--
-- TOC entry 4199 (class 0 OID 18421)
-- Dependencies: 228
-- Data for Name: contract_value; Type: TABLE DATA; Schema: data; Owner: doadmin
--



--
-- TOC entry 4201 (class 0 OID 18429)
-- Dependencies: 230
-- Data for Name: country; Type: TABLE DATA; Schema: data; Owner: doadmin
--

INSERT INTO data.country (id, name) VALUES (1, 'Spain');


--
-- TOC entry 4177 (class 0 OID 17668)
-- Dependencies: 205
-- Data for Name: extra_type; Type: TABLE DATA; Schema: data; Owner: doadmin
--

INSERT INTO data.extra_type (id, name) VALUES (1, 'test_extra_type');


--
-- TOC entry 4203 (class 0 OID 18434)
-- Dependencies: 232
-- Data for Name: extras; Type: TABLE DATA; Schema: data; Owner: doadmin
--

INSERT INTO data.extras (id, name, description) VALUES (1, 'test_extra', NULL);
INSERT INTO data.extras (id, name, description) VALUES (2, 'ex2', NULL);


--
-- TOC entry 4205 (class 0 OID 18442)
-- Dependencies: 234
-- Data for Name: hotel; Type: TABLE DATA; Schema: data; Owner: doadmin
--

INSERT INTO data.hotel (id, name, description, address, postal_code, geolocation, email, email_cc, phone, fax, rel_provider, rel_city, rel_accomodation_category, rel_accomodation_type, rel_chain, rel_extras, active, email_booking) VALUES (14, 't1', '123', '123', '123', NULL, NULL, NULL, NULL, NULL, 1, 35, 1, 1, 2, '{}', true, NULL);


--
-- TOC entry 4207 (class 0 OID 18450)
-- Dependencies: 236
-- Data for Name: pax_range; Type: TABLE DATA; Schema: data; Owner: doadmin
--

INSERT INTO data.pax_range (id, rel_room_type, adult_min_pax, adult_max_pax, children_min_pax, children_max_pax) VALUES (3, 9, 1, 2, 3, 4);
INSERT INTO data.pax_range (id, rel_room_type, adult_min_pax, adult_max_pax, children_min_pax, children_max_pax) VALUES (4, 9, 5, 6, 7, 8);
INSERT INTO data.pax_range (id, rel_room_type, adult_min_pax, adult_max_pax, children_min_pax, children_max_pax) VALUES (5, 10, 1, 2, 3, 4);
INSERT INTO data.pax_range (id, rel_room_type, adult_min_pax, adult_max_pax, children_min_pax, children_max_pax) VALUES (6, 10, 5, 6, 7, 8);
INSERT INTO data.pax_range (id, rel_room_type, adult_min_pax, adult_max_pax, children_min_pax, children_max_pax) VALUES (38, 8, 5, 6, 7, 8);
INSERT INTO data.pax_range (id, rel_room_type, adult_min_pax, adult_max_pax, children_min_pax, children_max_pax) VALUES (73, 43, 1, 2, 0, 0);
INSERT INTO data.pax_range (id, rel_room_type, adult_min_pax, adult_max_pax, children_min_pax, children_max_pax) VALUES (74, 43, 2, 2, 1, 2);
INSERT INTO data.pax_range (id, rel_room_type, adult_min_pax, adult_max_pax, children_min_pax, children_max_pax) VALUES (75, 42, 1, 2, 0, 1);
INSERT INTO data.pax_range (id, rel_room_type, adult_min_pax, adult_max_pax, children_min_pax, children_max_pax) VALUES (76, 42, 2, 3, 4, 5);


--
-- TOC entry 4209 (class 0 OID 18455)
-- Dependencies: 238
-- Data for Name: provider; Type: TABLE DATA; Schema: data; Owner: doadmin
--

INSERT INTO data.provider (id, fiscal_name, fiscal_code, contact_name, accounting_id, address, rel_city, active, phone, fax) VALUES (1, 'testprovider', 'asd', 'asd2', '123', 'asd', 35, true, NULL, '23');


--
-- TOC entry 4211 (class 0 OID 18463)
-- Dependencies: 240
-- Data for Name: quota; Type: TABLE DATA; Schema: data; Owner: doadmin
--

INSERT INTO data.quota (id, name, rel_hotel, active, date_start, date_end) VALUES (43, 'test ', 14, true, '2021-05-01', '2021-10-31');


--
-- TOC entry 4213 (class 0 OID 18468)
-- Dependencies: 242
-- Data for Name: quota_range; Type: TABLE DATA; Schema: data; Owner: doadmin
--

INSERT INTO data.quota_range (id, rel_quota, date_start, date_end, priority) VALUES (42, 43, '2021-05-01', '2021-10-31', 9);
INSERT INTO data.quota_range (id, rel_quota, date_start, date_end, priority) VALUES (43, 43, '2021-07-01', '2021-07-31', 8);


--
-- TOC entry 4215 (class 0 OID 18473)
-- Dependencies: 244
-- Data for Name: quota_value; Type: TABLE DATA; Schema: data; Owner: doadmin
--

INSERT INTO data.quota_value (id, rel_quota_range, rel_room, value, rel_room_shared_quota, free_sale, on_request) VALUES (76, 42, 61, 50, NULL, false, false);
INSERT INTO data.quota_value (id, rel_quota_range, rel_room, value, rel_room_shared_quota, free_sale, on_request) VALUES (77, 43, 61, 10, NULL, false, false);


--
-- TOC entry 4217 (class 0 OID 18478)
-- Dependencies: 246
-- Data for Name: regime; Type: TABLE DATA; Schema: data; Owner: doadmin
--

INSERT INTO data.regime (id, name, description, rel_regime_type, code, rel_regime_service_first, rel_regime_service_last, active) VALUES (5, '22', 'r12', 1, '1', 2, 2, true);
INSERT INTO data.regime (id, name, description, rel_regime_type, code, rel_regime_service_first, rel_regime_service_last, active) VALUES (6, '33', '33', 1, '123', 1, NULL, true);


--
-- TOC entry 4218 (class 0 OID 18484)
-- Dependencies: 247
-- Data for Name: regime_contract_setting; Type: TABLE DATA; Schema: data; Owner: doadmin
--



--
-- TOC entry 4220 (class 0 OID 18492)
-- Dependencies: 249
-- Data for Name: regime_service; Type: TABLE DATA; Schema: data; Owner: doadmin
--

INSERT INTO data.regime_service (id, name) VALUES (1, 'Room Only');
INSERT INTO data.regime_service (id, name) VALUES (2, 'Breakfast');
INSERT INTO data.regime_service (id, name) VALUES (3, 'Lunch');
INSERT INTO data.regime_service (id, name) VALUES (4, 'Dinner');


--
-- TOC entry 4222 (class 0 OID 18497)
-- Dependencies: 251
-- Data for Name: regime_type; Type: TABLE DATA; Schema: data; Owner: doadmin
--

INSERT INTO data.regime_type (id, name) VALUES (1, 'Full Board');


--
-- TOC entry 4225 (class 0 OID 18504)
-- Dependencies: 254
-- Data for Name: region; Type: TABLE DATA; Schema: data; Owner: doadmin
--

INSERT INTO data.region (id, name, rel_country) VALUES (1, 'Illes Balears', 1);


--
-- TOC entry 4227 (class 0 OID 18509)
-- Dependencies: 256
-- Data for Name: room; Type: TABLE DATA; Schema: data; Owner: doadmin
--

INSERT INTO data.room (id, rel_hotel, name, description, rel_room_type, kids_as_adult, infants_as_adult) VALUES (61, 14, '123', '123', 43, false, false);
INSERT INTO data.room (id, rel_hotel, name, description, rel_room_type, kids_as_adult, infants_as_adult) VALUES (62, 14, '123 vista mar', NULL, 43, false, false);


--
-- TOC entry 4229 (class 0 OID 18517)
-- Dependencies: 258
-- Data for Name: room_type; Type: TABLE DATA; Schema: data; Owner: doadmin
--

INSERT INTO data.room_type (id, name, description, min_pax, max_pax, active) VALUES (43, 'tipo2', NULL, 1, 4, true);
INSERT INTO data.room_type (id, name, description, min_pax, max_pax, active) VALUES (42, 'room_type1', 'rt1', 2, 3, true);


--
-- TOC entry 4231 (class 0 OID 18525)
-- Dependencies: 260
-- Data for Name: sale; Type: TABLE DATA; Schema: data; Owner: doadmin
--

INSERT INTO data.sale (id, name, rel_contract, active, rel_hotel) VALUES (53, 's1', 80, true, 13);


--
-- TOC entry 4266 (class 0 OID 0)
-- Dependencies: 209
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: access; Owner: doadmin
--

SELECT pg_catalog.setval('access.user_id_seq', 1, true);


--
-- TOC entry 4267 (class 0 OID 0)
-- Dependencies: 216
-- Name: booking_id_seq1; Type: SEQUENCE SET; Schema: data; Owner: doadmin
--

SELECT pg_catalog.setval('data.booking_id_seq1', 502, true);


--
-- TOC entry 4268 (class 0 OID 0)
-- Dependencies: 217
-- Name: category_id_seq; Type: SEQUENCE SET; Schema: data; Owner: doadmin
--

SELECT pg_catalog.setval('data.category_id_seq', 8, true);


--
-- TOC entry 4269 (class 0 OID 0)
-- Dependencies: 219
-- Name: chain_id_seq; Type: SEQUENCE SET; Schema: data; Owner: doadmin
--

SELECT pg_catalog.setval('data.chain_id_seq', 2, true);


--
-- TOC entry 4270 (class 0 OID 0)
-- Dependencies: 221
-- Name: city_id_seq; Type: SEQUENCE SET; Schema: data; Owner: doadmin
--

SELECT pg_catalog.setval('data.city_id_seq', 514, true);


--
-- TOC entry 4271 (class 0 OID 0)
-- Dependencies: 223
-- Name: client_id_seq; Type: SEQUENCE SET; Schema: data; Owner: doadmin
--

SELECT pg_catalog.setval('data.client_id_seq', 1, false);


--
-- TOC entry 4272 (class 0 OID 0)
-- Dependencies: 225
-- Name: contract_id_seq; Type: SEQUENCE SET; Schema: data; Owner: doadmin
--

SELECT pg_catalog.setval('data.contract_id_seq', 80, true);


--
-- TOC entry 4273 (class 0 OID 0)
-- Dependencies: 227
-- Name: contract_range_id_seq; Type: SEQUENCE SET; Schema: data; Owner: doadmin
--

SELECT pg_catalog.setval('data.contract_range_id_seq', 86, true);


--
-- TOC entry 4274 (class 0 OID 0)
-- Dependencies: 229
-- Name: cost_id_seq; Type: SEQUENCE SET; Schema: data; Owner: doadmin
--

SELECT pg_catalog.setval('data.cost_id_seq', 393, true);


--
-- TOC entry 4275 (class 0 OID 0)
-- Dependencies: 231
-- Name: country_id_seq; Type: SEQUENCE SET; Schema: data; Owner: doadmin
--

SELECT pg_catalog.setval('data.country_id_seq', 1, true);


--
-- TOC entry 4276 (class 0 OID 0)
-- Dependencies: 206
-- Name: extra_type_id_seq; Type: SEQUENCE SET; Schema: data; Owner: doadmin
--

SELECT pg_catalog.setval('data.extra_type_id_seq', 1, true);


--
-- TOC entry 4277 (class 0 OID 0)
-- Dependencies: 233
-- Name: extras_id_seq; Type: SEQUENCE SET; Schema: data; Owner: doadmin
--

SELECT pg_catalog.setval('data.extras_id_seq', 35, true);


--
-- TOC entry 4278 (class 0 OID 0)
-- Dependencies: 235
-- Name: hotel_id_seq1; Type: SEQUENCE SET; Schema: data; Owner: doadmin
--

SELECT pg_catalog.setval('data.hotel_id_seq1', 14, true);


--
-- TOC entry 4279 (class 0 OID 0)
-- Dependencies: 237
-- Name: pax_range_id_seq; Type: SEQUENCE SET; Schema: data; Owner: doadmin
--

SELECT pg_catalog.setval('data.pax_range_id_seq', 76, true);


--
-- TOC entry 4280 (class 0 OID 0)
-- Dependencies: 239
-- Name: provider_id_seq; Type: SEQUENCE SET; Schema: data; Owner: doadmin
--

SELECT pg_catalog.setval('data.provider_id_seq', 4, true);


--
-- TOC entry 4281 (class 0 OID 0)
-- Dependencies: 241
-- Name: quota_id_seq; Type: SEQUENCE SET; Schema: data; Owner: doadmin
--

SELECT pg_catalog.setval('data.quota_id_seq', 43, true);


--
-- TOC entry 4282 (class 0 OID 0)
-- Dependencies: 243
-- Name: quota_range_id_seq; Type: SEQUENCE SET; Schema: data; Owner: doadmin
--

SELECT pg_catalog.setval('data.quota_range_id_seq', 43, true);


--
-- TOC entry 4283 (class 0 OID 0)
-- Dependencies: 245
-- Name: quota_value_id_seq; Type: SEQUENCE SET; Schema: data; Owner: doadmin
--

SELECT pg_catalog.setval('data.quota_value_id_seq', 77, true);


--
-- TOC entry 4284 (class 0 OID 0)
-- Dependencies: 248
-- Name: regime_contract_setting_id_seq; Type: SEQUENCE SET; Schema: data; Owner: doadmin
--

SELECT pg_catalog.setval('data.regime_contract_setting_id_seq', 1, false);


--
-- TOC entry 4285 (class 0 OID 0)
-- Dependencies: 250
-- Name: regime_service_id_seq; Type: SEQUENCE SET; Schema: data; Owner: doadmin
--

SELECT pg_catalog.setval('data.regime_service_id_seq', 4, true);


--
-- TOC entry 4286 (class 0 OID 0)
-- Dependencies: 252
-- Name: regime_type_id_seq; Type: SEQUENCE SET; Schema: data; Owner: doadmin
--

SELECT pg_catalog.setval('data.regime_type_id_seq', 6, true);


--
-- TOC entry 4287 (class 0 OID 0)
-- Dependencies: 253
-- Name: regime_type_id_seq1; Type: SEQUENCE SET; Schema: data; Owner: doadmin
--

SELECT pg_catalog.setval('data.regime_type_id_seq1', 1, true);


--
-- TOC entry 4288 (class 0 OID 0)
-- Dependencies: 255
-- Name: region_id_seq; Type: SEQUENCE SET; Schema: data; Owner: doadmin
--

SELECT pg_catalog.setval('data.region_id_seq', 1, true);


--
-- TOC entry 4289 (class 0 OID 0)
-- Dependencies: 257
-- Name: room_id_seq; Type: SEQUENCE SET; Schema: data; Owner: doadmin
--

SELECT pg_catalog.setval('data.room_id_seq', 62, true);


--
-- TOC entry 4290 (class 0 OID 0)
-- Dependencies: 259
-- Name: room_type_id_seq; Type: SEQUENCE SET; Schema: data; Owner: doadmin
--

SELECT pg_catalog.setval('data.room_type_id_seq', 44, true);


--
-- TOC entry 4291 (class 0 OID 0)
-- Dependencies: 261
-- Name: sale_id_seq; Type: SEQUENCE SET; Schema: data; Owner: doadmin
--

SELECT pg_catalog.setval('data.sale_id_seq', 53, true);


--
-- TOC entry 4292 (class 0 OID 0)
-- Dependencies: 262
-- Name: type_id_seq; Type: SEQUENCE SET; Schema: data; Owner: doadmin
--

SELECT pg_catalog.setval('data.type_id_seq', 4, true);


--
-- TOC entry 3992 (class 2606 OID 18559)
-- Name: user user_pkey; Type: CONSTRAINT; Schema: access; Owner: doadmin
--

ALTER TABLE ONLY access."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id, username);


--
-- TOC entry 3994 (class 2606 OID 18561)
-- Name: webbeds_booking webbeds_booking_pkey; Type: CONSTRAINT; Schema: crossreference; Owner: doadmin
--

ALTER TABLE ONLY crossreference.webbeds_booking
    ADD CONSTRAINT webbeds_booking_pkey PRIMARY KEY (id, rel_booking);


--
-- TOC entry 3996 (class 2606 OID 18563)
-- Name: webbeds_hotel webbeds_hotel_pkey; Type: CONSTRAINT; Schema: crossreference; Owner: doadmin
--

ALTER TABLE ONLY crossreference.webbeds_hotel
    ADD CONSTRAINT webbeds_hotel_pkey PRIMARY KEY (id, rel_hotel);


--
-- TOC entry 3998 (class 2606 OID 18565)
-- Name: webbeds_room webbeds_room_pkey; Type: CONSTRAINT; Schema: crossreference; Owner: doadmin
--

ALTER TABLE ONLY crossreference.webbeds_room
    ADD CONSTRAINT webbeds_room_pkey PRIMARY KEY (id, rel_room);


--
-- TOC entry 4004 (class 2606 OID 18567)
-- Name: booking booking_pkey1; Type: CONSTRAINT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.booking
    ADD CONSTRAINT booking_pkey1 PRIMARY KEY (id);


--
-- TOC entry 4000 (class 2606 OID 18569)
-- Name: accomodation_category category_pkey; Type: CONSTRAINT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.accomodation_category
    ADD CONSTRAINT category_pkey PRIMARY KEY (id);


--
-- TOC entry 4006 (class 2606 OID 18571)
-- Name: chain chain_pkey; Type: CONSTRAINT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.chain
    ADD CONSTRAINT chain_pkey PRIMARY KEY (id);


--
-- TOC entry 4008 (class 2606 OID 18573)
-- Name: city city_pkey; Type: CONSTRAINT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.city
    ADD CONSTRAINT city_pkey PRIMARY KEY (id);


--
-- TOC entry 4010 (class 2606 OID 18575)
-- Name: client client_pkey; Type: CONSTRAINT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.client
    ADD CONSTRAINT client_pkey PRIMARY KEY (id);


--
-- TOC entry 4012 (class 2606 OID 18577)
-- Name: contract contract_pkey; Type: CONSTRAINT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.contract
    ADD CONSTRAINT contract_pkey PRIMARY KEY (id);


--
-- TOC entry 4014 (class 2606 OID 18579)
-- Name: contract_range contract_range_pkey; Type: CONSTRAINT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.contract_range
    ADD CONSTRAINT contract_range_pkey PRIMARY KEY (id);


--
-- TOC entry 4016 (class 2606 OID 18581)
-- Name: contract_value cost_pkey; Type: CONSTRAINT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.contract_value
    ADD CONSTRAINT cost_pkey PRIMARY KEY (id);


--
-- TOC entry 4018 (class 2606 OID 18583)
-- Name: country country_pkey; Type: CONSTRAINT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.country
    ADD CONSTRAINT country_pkey PRIMARY KEY (id);


--
-- TOC entry 3990 (class 2606 OID 17804)
-- Name: extra_type extra_type_pkey; Type: CONSTRAINT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.extra_type
    ADD CONSTRAINT extra_type_pkey PRIMARY KEY (id);


--
-- TOC entry 4020 (class 2606 OID 18585)
-- Name: extras extras_pkey; Type: CONSTRAINT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.extras
    ADD CONSTRAINT extras_pkey PRIMARY KEY (id);


--
-- TOC entry 4022 (class 2606 OID 18587)
-- Name: hotel hotel_pkey1; Type: CONSTRAINT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.hotel
    ADD CONSTRAINT hotel_pkey1 PRIMARY KEY (id);


--
-- TOC entry 4024 (class 2606 OID 18589)
-- Name: pax_range pax_range_pkey; Type: CONSTRAINT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.pax_range
    ADD CONSTRAINT pax_range_pkey PRIMARY KEY (id);


--
-- TOC entry 4030 (class 2606 OID 18591)
-- Name: quota_range priority_constrain; Type: CONSTRAINT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.quota_range
    ADD CONSTRAINT priority_constrain UNIQUE (rel_quota, priority);


--
-- TOC entry 4026 (class 2606 OID 18593)
-- Name: provider provider_pkey; Type: CONSTRAINT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.provider
    ADD CONSTRAINT provider_pkey PRIMARY KEY (id);


--
-- TOC entry 4028 (class 2606 OID 18595)
-- Name: quota quota_pkey; Type: CONSTRAINT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.quota
    ADD CONSTRAINT quota_pkey PRIMARY KEY (id);


--
-- TOC entry 4032 (class 2606 OID 18597)
-- Name: quota_range quota_range_pkey; Type: CONSTRAINT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.quota_range
    ADD CONSTRAINT quota_range_pkey PRIMARY KEY (id);


--
-- TOC entry 4034 (class 2606 OID 18599)
-- Name: quota_value quota_value_pkey; Type: CONSTRAINT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.quota_value
    ADD CONSTRAINT quota_value_pkey PRIMARY KEY (id);


--
-- TOC entry 4038 (class 2606 OID 18601)
-- Name: regime_contract_setting regime_contract_setting_pkey; Type: CONSTRAINT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.regime_contract_setting
    ADD CONSTRAINT regime_contract_setting_pkey PRIMARY KEY (id);


--
-- TOC entry 4040 (class 2606 OID 18603)
-- Name: regime_service regime_service_pkey; Type: CONSTRAINT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.regime_service
    ADD CONSTRAINT regime_service_pkey PRIMARY KEY (id);


--
-- TOC entry 4036 (class 2606 OID 18605)
-- Name: regime regime_type_pkey; Type: CONSTRAINT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.regime
    ADD CONSTRAINT regime_type_pkey PRIMARY KEY (id);


--
-- TOC entry 4042 (class 2606 OID 18607)
-- Name: regime_type regime_type_pkey1; Type: CONSTRAINT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.regime_type
    ADD CONSTRAINT regime_type_pkey1 PRIMARY KEY (id);


--
-- TOC entry 4044 (class 2606 OID 18609)
-- Name: region region_pkey; Type: CONSTRAINT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.region
    ADD CONSTRAINT region_pkey PRIMARY KEY (id);


--
-- TOC entry 4046 (class 2606 OID 18611)
-- Name: room room_pkey; Type: CONSTRAINT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.room
    ADD CONSTRAINT room_pkey PRIMARY KEY (id);


--
-- TOC entry 4048 (class 2606 OID 18613)
-- Name: room_type room_type_pkey; Type: CONSTRAINT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.room_type
    ADD CONSTRAINT room_type_pkey PRIMARY KEY (id);


--
-- TOC entry 4050 (class 2606 OID 18615)
-- Name: sale sale_pkey; Type: CONSTRAINT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.sale
    ADD CONSTRAINT sale_pkey PRIMARY KEY (id);


--
-- TOC entry 4002 (class 2606 OID 18617)
-- Name: accomodation_type type_pkey; Type: CONSTRAINT; Schema: data; Owner: doadmin
--

ALTER TABLE ONLY data.accomodation_type
    ADD CONSTRAINT type_pkey PRIMARY KEY (id);


-- Completed on 2021-03-17 12:06:58 CET

--
-- PostgreSQL database dump complete
--


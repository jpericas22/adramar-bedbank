#!/bin/bash
echo "started adramar sync service"

#until PGPA1SSWORD=$POSTGRES_PASSWORD psql -h "$PGHOST" -U "$PGUSER" -c '\q'; do
#  echo "Waiting for postgres"
#  sleep 2
#done

until nc -z $DB_HOST $DB_PORT; do
  echo "Waiting for database"
  sleep 2
done

until nc -z $RABBITMQ_HOST $RABBITMQ_PORT; do
  echo "Waiting for rabbitmq"
  sleep 2
done

python3 -u /volume/src/main.py

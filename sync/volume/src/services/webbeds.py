import sys
import os
from lib import http_req as http, soap
from datetime import datetime, date
from services import backend


WEBBEDS_USERNAME = os.environ['WEBBEDS_USERNAME']
WEBBEDS_PASSWORD = os.environ['WEBBEDS_PASSWORD']
HOSTNAME = os.environ['WEBBEDS_HOSTNAME']
PORT = os.environ['WEBBEDS_PORT']
BASE_PATH = os.environ['WEBBEDS_BASE_PATH']
WEBBEDS_API_NAMESPACE = os.environ['WEBBEDS_API_NAMESPACE']
DEFAULT_CURRENCY = 'EUR'


class Error(Exception):
    pass


class ApiError(Error):
    def __init__(self, status_code, error):
        self.status_code = status_code
        self.error = error


def request(action, data):
    for item in data:
        if data[item] is None:
            data[item] = ''
        elif type(data[item]) is date:
            data[item] = data[item].strftime('%Y-%m-%d')
        elif type(data[item]) is datetime:
            data[item] = data[item].strftime('%Y-%m-%d %H:%M')
        elif type(data[item]) is bool:
            data[item] = 'true' if data[item] else 'false'

    data = {
        'username': WEBBEDS_USERNAME,
        'password': WEBBEDS_PASSWORD,
        **data
    }
    res = http.request(HOSTNAME, int(PORT), BASE_PATH + '/' +
                       action, 'POST', post_params=data)
    res = soap.xml_to_dict(res, namespace=WEBBEDS_API_NAMESPACE)

    if res['StatusCode'] != '0':
        raise ApiError(res['StatusCode'], res['Error'])

    return res


def getRoomTypes(shHotelId=''):
    data = {
        'shHotelId': shHotelId
    }
    return request('GetRoomTypes', data)


def getRemoteHotels(shHotelId=''):
    client_data = getRoomTypes()
       # TODO check error codes
    status_code = int(client_data['StatusCode'])
    if status_code != 0:
        raise Exception('Status code ' + str(status_code))

    if 'Hotels' not in client_data:
        raise Exception('"Hotels" key not found in response')

    hotel_data = client_data['Hotels']
    if 'Hotel' not in hotel_data:
        raise Exception('"Hotel" key not found in response')

    return hotel_data['Hotel']


def sync_hotels():
    hotel_data = getRemoteHotels()
    hotel_db_sync_data = list(
        map(lambda x: (x['HotelId'], x['HotelName']), hotel_data))
    room_db_sync_data = []
    for hotel in hotel_data:
        if 'RoomTypes' not in hotel:
            raise Exception('RoomTypes key not found')
        room_data = hotel['RoomTypes']
        if 'RoomType' not in room_data:
            raise Exception('RoomType key not found')
        room_data = room_data['RoomType']
        if not isinstance(room_data, list):
            room_data = [room_data]
        room_db_sync_data.extend(list(map(lambda x: (x['RoomId'], x['Description'], hotel['HotelId']), room_data)))
    backend.insert_hotel_crossreference(hotel_db_sync_data, 'webbeds')
    backend.insert_room_crossreference(room_db_sync_data, 'webbeds')


def updateCalendarInformation(RoomId, StartDate, EndDate, Price='', Currency='',  Availability='', MinimumStay='', MaximumStay='', ReleaseDays='', ReleaseHour='', xBedChildRate='', IncudedBoard='None', BreakfastAdultPrice='', BreakfastChildPrice='', HalfBoardAdultPrice='', HalfBoardChildPrice='', FullBoardAdultPrice='', FullBoardChildPrice='', AllIncAdultPrice='', AllIncChildPrice=''):
    if not (RoomId or StartDate or EndDate):
        raise ValueError(
            'Please input fields RoomId, start_date and end_date')
    if Price ^ Currency:
        raise ValueError('Pease input price AND currency')
    board_whitelist=['None', 'Breakfast', 'Half_Board', 'Full_Board', 'All']
    if IncudedBoard and IncudedBoard not in board_whitelist:
        raise ValueError(
            'invalid IncudedBoard value, accepts ' + str(','.join(IncudedBoard)))

    data={
        'RoomId': RoomId,
        'StartDate': StartDate,
        'EndDate': EndDate,
        'Price': Price,
        'Currency': Currency,
        'Availability': Availability,
        'MinimumStay': MinimumStay,
        'MaximumStay': MaximumStay,
        'ReleaseDays': ReleaseDays
    }
    return request('UpdateCalendarInformation', data)


def getBookings(shHotelId=None, arrivalStartDate: date=None, arrivalEndDate: date=None, bookingStartDate: datetime=None, bookingEndDate: datetime=None, cancelStartDate: datetime=None, cancelEndDate: datetime=None, includeVccData: bool=True):
    if not (arrivalStartDate or bookingStartDate or cancelStartDate):
        raise ValueError(
            'Please input at least one: arrivalStartDate, bookingStartDate, cancelStartDate')

    data={
        'shHotelId': shHotelId,
        'arrivalStartDate': arrivalStartDate,
        'arrivalEndDate': arrivalEndDate,
        'bookingStartDate': bookingStartDate,
        'bookingEndDate': bookingEndDate,
        'cancelStartDate': cancelStartDate,
        'cancelEndDate': cancelEndDate,
        'includeVccData': includeVccData
    }

    res = request('GetBookings', data)
    bookings = res['Bookings']['Booking']

    if bookings:
        return bookings
    else:
        return []


def parse_room_sale_value(sale_value):
    sale_value


"""
def sync_rooms():
    backend_room_res = backend.get_rooms()
    for room in backend_room_res:
        updateCalendarInformation(
            room['webbeds_room_id'],
            room['sale_date_start'],
            room['sale_date_end'],
            Price=room['sale_value'],
            Currency=DEFAULT_CURRENCY,
            Availability=0 if not room['contract_active'] or not room['quota_active'] else room['quota_value'],
            MinimumStay=room['sale_min_nights'],
            ReleaseDays=room['sale_release'],
            BreakfastAdultPrice=room['sale_value']['3'] or '',
            BreakfastChildPrice=room['sale_value']['3'] or '',
            HalfBoardAdultPrice=room['sale_value']['2'] or '',
            HalfBoardChildPrice=room['sale_value']['2'] or '',
            FullBoardAdultPrice=room['sale_value']['1'] or '',
            FullBoardChildPrice=room['sale_value']['1'] or '',
            AllIncAdultPrice=room['sale_value']['4'] or '',
            AllIncChildPrice=room['sale_value']['4'] or ''
        )
"""

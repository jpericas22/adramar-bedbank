from lib import db
from psycopg2.extensions import AsIs
from psycopg2.extras import execute_values
from functools import reduce


def get_hotels():
    cur = db.get_cursor()
    # base_query = 'SELECT * FROM %s'
    # cur.execute(base_query, ('asd',))
    base_query = 'SELECT * FROM data.hotel'
    cur.execute(base_query, ())
    return cur.fetchall()


def insert_booking(data, crossreference, con=None, commit=True, close=True):
    # try to implement paralel insert
    data = {
        **data,
        'crossreference': AsIs(crossreference)
    }
    if not con:
        con = db.get_connection()
    cur = con.cursor()
    # TODO crossreference de todo
    query = """INSERT INTO data.booking(rel_client,
                                        rel_hotel,
                                        rel_room,
                                        rel_regime,
                                        adults,
                                        children,
                                        infants,
                                        checkin_date,
                                        checkout_date,
                                        contact_name,
                                        contact_phone,
                                        contact_email,
                                        observations,
                                        status,
                                        booking_date,
                                        cancellation_date,
                                        no_show)
	VALUES (%(client_id)s,
            (SELECT rel_hotel
               FROM crossreference.%(crossreference)s_hotel
              WHERE id = %(hotel_id)s),
            %(room_id)s,
            %(regime_id)s,
            %(adults)s,
            (%(children)s)::"data".child[],
            %(infants)s,
            %(checkin_date)s,
            %(checkout_date)s,
            %(contact_name)s,
            %(contact_phone)s,
            %(contact_email)s,
            %(observations)s,
            %(status)s,
            %(booking_date)s,
            null,
            false)
    RETURNING id
    """
    cur.execute(query, data)
    res_id = cur.fetchone()
    cur.close()
    if commit:
        con.commit()
    if close:
        con.close()
    return (res_id[0], data['client_booking_id'])


def check_db_bookings(bookings, id_key, crossreference, con=None):
    def extract_sunhotels_id(item):
        # TODO comprobar si es mejor guardar el ID como int
        return item[id_key]
    ids = list(map(extract_sunhotels_id, bookings))

    if not con:
        con = db.get_connection()
    cur = con.cursor()

    query = """SELECT array_agg(id)
                 FROM crossreference.%(crossreference)s_booking
             WHERE id = ANY(%(ids)s)
             """
    cur.execute(query, ({
        'ids': ids,
        'crossreference': AsIs(crossreference)
    }))
    res_id = cur.fetchone()
    cur.close()
    found_ids = res_id[0] or []
    return {
        'missing': list(filter(lambda x: (x not in ids), found_ids)),
        'found': list(filter(lambda x: (x in ids), found_ids))
    }


def insert_booking_crossreference(id_pair, crossreference, con=None, commit=True, close=True):
    if not con:
        con = db.get_connection()
    cur = con.cursor()
    query = """INSERT INTO crossreference.%(crossreference)s_booking(id,
                                                                     rel_booking)
                    VALUES (%(client_booking_id)s, %(db_booking_id)s)"""
    cur.execute(query, ({
        'crossreference': AsIs(crossreference),
        'db_booking_id': id_pair[0],
        'client_booking_id': id_pair[1],
    }))
    cur.close()
    if commit:
        con.commit()
    if close:
        con.close()


def insert_hotel_crossreference(data, crossreference, con=None, commit=True, close=True):
    if not con:
        con = db.get_connection()
    cur = con.cursor()
    data = map(lambda x: x + (None, 'NOW()',), data)
    query = """INSERT INTO crossreference.""" + crossreference + """_hotel(client_id,
                                                   client_name,
                                                   rel_hotel,
                                                   timestamp)
                                            VALUES %s
                                       ON CONFLICT (client_id)
                                        DO NOTHING"""
    execute_values(cur, query, list(data))
    cur.close()
    if commit:
        con.commit()
    if close:
        con.close()

def insert_room_crossreference(data, crossreference, con=None, commit=True, close=True):
    if not con:
        con = db.get_connection()
    cur = con.cursor()
    data = map(lambda x: x + (None, 'NOW()',), data)
    query = """INSERT INTO crossreference.""" + crossreference + """_room(client_id,
                                                   client_name,
                                                   client_hotel_id,
                                                   rel_room,
                                                   timestamp)
                                            VALUES %s
                                       ON CONFLICT (client_id)
                                        DO NOTHING"""
    execute_values(cur, query, list(data))
    cur.close()
    if commit:
        con.commit()
    if close:
        con.close()


def get_latest_client_booking_date(client_id, con=None, close=True):
    if not con:
        con = db.get_connection()
    cur = con.cursor()
    query = """SELECT booking_date
	             FROM data.booking
	            WHERE rel_client = %s
	         ORDER BY booking_date DESC
	            LIMIT 1"""
    cur.execute(query, (client_id,))
    res_date = cur.fetchone()
    cur.close()
    if close:
        con.close()
    return res_date[0]


def get_latest_client_cancellation_date(client_id, con=None, close=True):
    if not con:
        con = db.get_connection()
    cur = con.cursor()
    query = """SELECT booking_date
	             FROM data.booking
	            WHERE rel_client = %s
	         ORDER BY cancellation_date DESC
	            LIMIT 1"""
    cur.execute(query, (client_id,))
    res_date = cur.fetchone()
    cur.close()
    if close:
        con.close()
    return res_date[0]


def get_rooms(con=None, close=True):
    if not con:
        con = db.get_connection()
    cur = con.cursor()
    query = """SELECT wb_r.id as webbeds_room_id,
                      h.id AS hotel_id,
                      h.name AS hotel_name,
                      r.id AS room_id,
                      r.name AS room_name,
                      c.active AS contract_active,
                      cr.date_start AS sale_date_start,
                      cr.date_end AS sale_date_end,
                      cr.min_nights AS sale_min_nights,
                      cr.release AS sale_release,
					  jsonb_agg(json_build_object(ret.id, cv.sale_value)) AS sale_value,
                      q.active AS quota_active,
                      qr.date_start AS quota_date_start,
                      qr.date_end AS quota_date_end,
                      COALESCE(qv_r.value, qv.value) AS quota_value,
                      COALESCE(qv_r.id, qv.id) AS quota_value_id
                 FROM crossreference.webbeds_room AS wb_r
                 JOIN data.room AS r ON r.id = wb_r.rel_room
                 JOIN data.hotel AS h ON h.id = r.rel_hotel
                 JOIN data.contract AS c ON c.rel_hotel = h.id AND r.id = ANY(c.rel_room)
                 JOIN data.contract_range AS cr ON cr.rel_contract = c.id
                 JOIN data.contract_value AS cv ON cv.rel_contract_range = cr.id
                 JOIN data.quota AS q ON q.id = c.rel_quota
                 JOIN data.quota_range AS qr ON qr.rel_quota = q.id AND cr.date_start >= qr.date_start AND cr.date_end <= qr.date_end
                 JOIN data.quota_value AS qv ON qv.rel_quota_range = qr.id AND qv.rel_room = r.id
				 JOIN data.regime AS re ON re.id = cv.rel_regime
				 JOIN data.regime_type AS ret ON ret.id = re.rel_regime_type
            LEFT JOIN data.quota_value AS qv_r ON qv.rel_room_shared_quota = qv_r.rel_room AND qv.rel_quota_range = qv_r.rel_quota_range
			    WHERE cr.date_end > NOW() AND qr.date_end > NOW()
                GROUP BY wb_r.id, h.id, r.id, c.active, cr.date_start, cr.date_end, cr.min_nights, cr.release, cv.rel_room, q.active, qr.date_start, qr.date_end, qv.value, qv.id, qv_r.value, qv_r.id"""

    cur.execute(query, ())
    res_rooms = cur.fetchall()
    cur.close()
    if close:
        con.close()
    return res_rooms

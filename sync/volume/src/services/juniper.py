import os
from lib import http_req as http, soap
from services import backend

JUNIPER_API_NAMESPACE = os.environ['JUNIPER_API_NAMESPACE']
JUNIPER_USERNAME = os.environ['JUNIPER_USERNAME']
JUNIPER_PASSWORD = os.environ['JUNIPER_PASSWORD']
HOSTNAME = os.environ['JUNIPER_HOSTNAME']
PORT = os.environ['JUNIPER_PORT']
BASE_PATH = os.environ['JUNIPER_BASE_PATH']


class Error(Exception):
    pass


class ApiError(Error):
    def __init__(self, status_code, error):
        self.status_code = status_code
        self.error = error


def request(action, data):
    for key in data:
        item = data[key]
        args = item['args'] if 'args' in item else {}
        data[key] = {
            'args': {
                **args,
                'Version': '1',
                'Language': 'es'
            },
            'Login': {
                'args': {
                    'Email': JUNIPER_USERNAME,
                    'Password': JUNIPER_PASSWORD
                }
            },
            **item,
        }
    data = soap.build_request({action: data}, JUNIPER_API_NAMESPACE)
    res = http.request(HOSTNAME, int(PORT), BASE_PATH + action, 'POST', post_data=data, content_type='soap')
    res = soap.xml_to_dom(res, namespace={
        'soap': 'http://schemas.xmlsoap.org/soap/envelope/',
        'juniper': JUNIPER_API_NAMESPACE
    }, default_namespace='juniper')

    if res.get('Error'):
        # pylint: disable=no-member
        raise ApiError(res.get('./Error/@Code').text,
                       res.get('./Error/Text').text)

    return res

def test():
    req_obj = {
        'HotelOccupancyListRQ': {}
    }
    print(request('HotelOccupancyList', req_obj))
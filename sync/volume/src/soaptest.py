from functools import reduce
from lib import soap
TEST = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://www.juniper.es/webservice/2007/">
  <soapenv:Header/>
  <soapenv:Body>
    <HotelExtranetContractList>
      <HotelExtranetContractListRQ Version="1" Language="es">
        <Login Email="user@mydomain.com" Password="pass" />
        <HotelExtranetContractListRequest IncludeExpired="false">
          <Hotels>
            <Hotel Code="1176"/>
          </Hotels>
        </HotelExtranetContractListRequest>
      </HotelExtranetContractListRQ>
    </HotelExtranetContractList>
  </soapenv:Body>
</soapenv:Envelope>"""





testobj = {
    'request': {
        'args': {
            'asd': 'asd'
        },
        'nano': {
            'args': {
                'nanoarg': 'baba'
            }
        },
        'nano2': {
            'args': {
                'nanoarg2': 'baba2'
            },
            'nano3': {
                'args': {
                    'nanoarg3': 'baba3'
                }
            }
        }
    }
}

print(soap.build_request(testobj, 'nmtest'))

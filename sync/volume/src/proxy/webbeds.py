from services import webbeds, backend
from datetime import datetime
from lib import db

WEBBEDS_DATE_FORMAT = '%Y-%m-%dT%H:%M:%S'
WEBBEDS_DATETIME_FORMAT = '%Y-%m-%dT%H:%M:%S.%f'


def build_children_obj(obj):
    name = obj['Name']
    age = obj['Age']
    if name and age:
        return (obj['Name'], int(obj['Age']))


def build_webbeds_booking_filter(missing_ids):
    def filter_func(input):
        return int(input['BookingId']) not in missing_ids
    return filter_func


def build_webbeds_db_booking_obj(obj):
    adults = list(filter(None, obj['AdultGuests']['string']))
    children = list(
        filter(None, map(build_children_obj, obj['ChildGuests']['Child'])))
    checkin_date = datetime.strptime(obj['CheckInDate'], WEBBEDS_DATE_FORMAT)
    checkout_date = datetime.strptime(obj['CheckOutDate'], WEBBEDS_DATE_FORMAT)
    booking_date = datetime.strptime(
        obj['BookingDate'], WEBBEDS_DATETIME_FORMAT)

    return {
        'client_id': 1,
        'hotel_id': obj['HotelId'],
        'room_id': obj['RoomId'],
        'regime_id': '1',
        'adults': adults,
        'children': children,
        'infants': int(obj['Infants']),
        'checkin_date': checkin_date,
        'checkout_date':  checkout_date,
        'contact_name': None,
        'contact_phone': None,
        'contact_email': None,
        'observations': None,
        'status': 1,
        'booking_date': booking_date,
        'client_booking_id': obj['BookingId']
    }


def import_bookings():
    con = db.get_connection()

    latest_import_date = backend.get_latest_client_booking_date(
        1, con=con, close=False)
    webbeds_bookings = webbeds.get_bookings(
        bookingStartDate=latest_import_date,
        bookingEndDate=datetime.now())

    db_results = backend.check_db_bookings(
        webbeds_bookings, 'BookingId', 'webbeds')
    booking_db_objs = map(build_webbeds_db_booking_obj,
                          filter(lambda x: int(x['BookingId']) not in db_results['missing'],
                                 webbeds_bookings))

    inserted_ids = list(map(lambda x: backend.insert_booking(
        x, 'webbeds', con=con, commit=False, close=False), booking_db_objs))
    [backend.insert_booking_crossreference(
        id_pair, 'webbeds', con=con, commit=False, close=False) for id_pair in inserted_ids]
    con.commit()
    con.close()
    return inserted_ids

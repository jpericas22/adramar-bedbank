import os
import pika
import json
from datetime import datetime, date
from services import webbeds, backend, juniper


def parse_input(data, date_keys=[], datetime_keys=[], required_keys=[]):
    for key in data:
        if data[key] is None:
            data[key] = ''
        elif key in date_keys:
            data[key] = date.fromtimestamp(data[key])
        elif key in datetime_keys:
            data[key] = datetime.fromtimestamp(data[key])
    for key in required_keys:
        if key not in data:
            raise Exception('missing key '+key)


def webbeds_get_bookings_handler(args):  # TODO implement missing key handler
    parse_input(args, date_keys=['arrivalStartDate',
                                 'arrivalEndDate'], datetime_keys=['bookingStartDate', 'bookingEndDate'], required_keys=['shHotelId',
                                                                                                                         'arrivalStartDate',
                                                                                                                         'arrivalEndDate',
                                                                                                                         'bookingStartDate',
                                                                                                                         'bookingEndDate',
                                                                                                                         'cancelStartDate',
                                                                                                                         'cancelEndDate',
                                                                                                                         'includeVccData'])
    return webbeds.getBookings(**args)


def webbeds_get_room_types_handler(args):
    parse_input(args, required_keys=['shHotelId'])
    return webbeds.getRoomTypes(**args)


def router(ch, method, properties, body):
    print('asd')
    print(" [x] Received %r" % body)
    body = json.loads(body)
    methods = {
        'webbeds_get_bookings': webbeds_get_bookings_handler,
        'webbeds_get_room_types': webbeds_get_room_types_handler
    }
    print(methods[body['method']](body['arguments']))

"""
# TODO IMPLEMENT NONBLOCKING
connection = pika.BlockingConnection(pika.ConnectionParameters('broker'))
channel = connection.channel()
channel.queue_declare(queue=os.environ['RABBITMQ_QUEUE'])
channel.basic_consume(os.environ['RABBITMQ_QUEUE'],
                      auto_ack=True,
                      on_message_callback=router)
print(backend.get_rooms())
print('Sync service waiting for messages')
channel.start_consuming()
"""

#webbeds.sync_hotels()
print(juniper.test())


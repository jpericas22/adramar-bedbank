import xml.etree.ElementTree as ET
from functools import reduce


class DOM(object):
    def __init__(self, data, namespace, default_namespace='a'):
        self.data = data
        self.namespace = namespace
        self.default_namespace = default_namespace

    def get_all(self, attr):
        return self.data.findall(f'.//{self.default_namespace}:{attr}', self.namespace)

    def get_all_dom(self, attr):
        items = self.get_all(attr)
        return list(map(lambda x: DOM(x, self.namespace, self.default_namespace), items))

    def get_all_dict(self, attr):
        items = self.get_all(attr)
        return explode_xml(items, namespace=self.namespace.values())[attr]

    def get(self, attr):
        return self.data.find(f'.//{self.default_namespace}:{attr}', self.namespace)

    def get_dict(self, attr):
        item = self.get(attr)
        return explode_xml(item, namespace=self.namespace.values())

    def get_dom(self, attr):
        item = self.get(attr)
        return DOM(item, self.namespace, self.default_namespace)

    # def build_dict(self):
    #    return explode_xml(self.data, namespace=self.namespace.values())

    def build_dict(self):
        return explode_xml(self.data, namespace=self.namespace.values())


class Error(Exception):
    pass


class XMLError(Error):
    def __init__(self, data):
        self.data = data


def xml_to_dict(xml, namespace=[]):
    xml = ET.fromstring(xml)
    return explode_xml(xml, namespace=namespace)


def xml_to_dom(xml, namespace={}, default_namespace='a'):
    data = ET.fromstring(xml)
    return DOM(data, namespace, default_namespace)


def build_child(parent_dom, element):
    return DOM(element, parent_dom.namespace, parent_dom.default_namespace)


def explode_xml(xml, namespace=[]):
    data = {}
    for child in xml:
        key = child.tag
        if len(namespace) > 0:
            for item in namespace:
                item = r'%s' % '{' + item + '}'
                key = key.replace(item, '')
        parsed = None
        if len(child) > 0:
            parsed = {key: explode_xml(child, namespace=namespace)}
        else:
            parsed = {key: child.text}
        if key in data:
            if type(data[key]) != list:
                data[key] = [data[key]]
            data[key].append(parsed[key])
        else:
            data = {
                **data,
                **parsed
            }
    return data


def build_from_dict(input):
    buffer = ''
    for item in input:
        data = input[item]
        args = reduce(lambda prev, curr: prev +
                      f'{curr[0]}="{curr[1]}" ', data['args'].items(), ' ')[:-1] if 'args' in data else ''
        buffer += f'<{item}{args}'
        if 'args' in data:
            del data['args']
        if len(data) > 0:
            buffer += f'>\n{build_from_dict(data)}</{item}>\n'
        else:
            buffer += '/>\n'
    return buffer


def build_request(body, namespace, header={}):

    req_object = {
        'soapenv:Envelope': {
            'args': {
                'xmlns:soapenv': 'http://schemas.xmlsoap.org/soap/envelope/',
                'xmlns:ns': namespace
            },
            'soapenv:Header': header,
            'soapenv:Body': body
        }
    }
    
    return '<?xml version="1.0" encoding="utf-8"?>\n' + build_from_dict(req_object)

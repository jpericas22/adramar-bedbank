import http.client
from urllib.parse import urlencode
import json

ENCODING = 'utf-8'


class Error(Exception):
    pass


class HttpError(Error):
    def __init__(self, status_code, message):
        self.status_code = status_code
        self.message = message


def request(hostname, port, path, method, get_params={}, post_data=None, content_type='urlencoded', headers={}):
    if method == 'POST' and post_data:
        if content_type == 'json':
            content_type_header_value = 'application/json'
            req = json.dumps(post_data)
        elif content_type == 'soap':
            content_type_header_value = 'application/soap+xml'
            req = post_data
        else:
            content_type_header_value = 'application/x-www-form-urlencoded'
            req = urlencode(post_data)

        headers = {
            **headers,
            'Content-type': content_type_header_value,
            'Content-Length': len(req)
        }
    
    if get_params:
        path += '?' + urlencode(get_params)

    if port == 443:
        conn = http.client.HTTPSConnection(hostname, port)
    else:
        conn = http.client.HTTPConnection(hostname, port)
    
    conn.request(method, path, req, headers)
    res = conn.getresponse()

    if res.status != 200:
        response_message = res.read()
        raise HttpError(res.status, response_message)

    buffer = ''
    while chunk := res.read(200):
        buffer += str(chunk, ENCODING)

    return buffer

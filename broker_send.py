import pika
import json
from datetime import datetime, date

connection = pika.BlockingConnection(
    pika.ConnectionParameters('localhost', port=6672))
channel = connection.channel()
channel.queue_declare(queue='sync')

message_body = {
    'command': 'get_bookings',
    'arguments': {
        'bookingStartDate': {
            'value': date(2020, 1, 1).isoformat(),
            'format': 'date'
        },
        'bookingEndDate': {
            'value': datetime(2020, 1, 1, 1, 1, 1).isoformat(),
            'format': 'datetime'
        },
    }
}


channel.basic_publish(exchange='',
                      routing_key='sync',
                      body=json.dumps(message_body))
print(" [x] Sent 'Hello World!'")
connection.close()
